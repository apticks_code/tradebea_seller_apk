package com.tradebeaseller.adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tradebeaseller.ApiCalls.OrderAcceptApiCall;
import com.tradebeaseller.ApiCalls.OrderRejectApiCall;
import com.tradebeaseller.R;
import com.tradebeaseller.activities.SelectedOrderDetailsActivity;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.models.responseModels.orderDetailsResponse.OrderData;
import com.tradebeaseller.models.responseModels.orderDetailsResponse.OrderStatus;
import com.tradebeaseller.utils.Constants;
import com.tradebeaseller.utils.DialogUtils;
import com.tradebeaseller.utils.PreferenceConnector;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

public class OrdersFragmentAdapter extends RecyclerView.Adapter<OrdersFragmentAdapter.ViewHolder> implements HttpReqResCallBack {

    private Context context;
    private Dialog progressDialog;
    private EditText etRejectMessage;
    int position;

    private LinkedList<OrderData> listOfOrderData;

    private String rejectOrderMessage = "";

    public OrdersFragmentAdapter(Context context, LinkedList<OrderData> listOfOrderData) {
        this.context = context;
        this.listOfOrderData = listOfOrderData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_order_details_items, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.llAcceptRejectStatus.setVisibility(View.GONE);
        OrderData orderData = listOfOrderData.get(position);
        String orderId = orderData.getTrackId();
        OrderStatus orderStatus = orderData.getOrderStatus();
        String orderStatusName = orderStatus.getCurrentStatus();
        int orderStatusId = orderStatus.getId();
        String orderDate = orderData.getDate();
        String otp = orderData.getOrderPickupOtp();
        String customerId = orderData.getCustomer();
        String time = orderData.getTime();
        String deliveryBoyStatus = orderData.getDeliveryBoyStatus();

        holder.tvOrderId.setText(orderId);
        holder.tvOrderStatus.setText(orderStatusName);
        holder.tvDate.setText(orderDate);
        holder.tvTime.setText(time);
        holder.tvCustomerId.setText(customerId);
        holder.tvOutForDelivery.setText("Status : " + " " + orderStatusName);
        if (otp != null) {
            if (!otp.isEmpty()) {
                holder.tvOTP.setText("Pickup OTP" + " " + ":" + " " + otp);
            } else {
                holder.tvOTP.setText("Pickup OTP" + " " + ":" + " " + "N/A");
            }
        } else {
            holder.tvOTP.setText("Pickup OTP" + " " + ":" + " " + "N/A");
        }

        if (orderStatusId == 0) {
            holder.llAcceptRejectStatus.setVisibility(View.GONE);
        } else if (orderStatusId == 1) {
            holder.tvAccepted.setVisibility(View.VISIBLE);
            holder.tvRejected.setVisibility(View.VISIBLE);
            holder.tvAccepted.setText(context.getString(R.string.accept));
            holder.tvRejected.setText(context.getString(R.string.reject));
        } else if (orderStatusId == 2) {
            holder.tvAccepted.setVisibility(View.VISIBLE);
            holder.tvRejected.setVisibility(View.VISIBLE);
            holder.tvAccepted.setText(context.getString(R.string.accepted));
            holder.tvRejected.setText(context.getString(R.string.reject));
        } else if (orderStatusId == 3) {
            holder.tvAccepted.setVisibility(View.VISIBLE);
            holder.tvRejected.setVisibility(View.VISIBLE);
            holder.tvAccepted.setText(context.getString(R.string.accept));
            holder.tvRejected.setText(context.getString(R.string.rejected));
        }
    }

    @Override
    public int getItemCount() {
        return listOfOrderData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private LinearLayout llAcceptRejectStatus;
        private TextView tvOrderId, tvOrderStatus, tvDate, tvOTP, tvCustomerId, tvTime, tvAccepted, tvRejected, tvOutForDelivery;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvOTP = itemView.findViewById(R.id.tvOTP);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvOrderId = itemView.findViewById(R.id.tvOrderId);
            tvAccepted = itemView.findViewById(R.id.tvAccepted);
            tvRejected = itemView.findViewById(R.id.tvRejected);
            tvCustomerId = itemView.findViewById(R.id.tvCustomerId);
            tvOrderStatus = itemView.findViewById(R.id.tvOrderStatus);
            tvOutForDelivery = itemView.findViewById(R.id.tvOutForDelivery);
            llAcceptRejectStatus = itemView.findViewById(R.id.llAcceptRejectStatus);

            tvAccepted.setOnClickListener(this);
            tvRejected.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            OrderData orderData = listOfOrderData.get(getLayoutPosition());
            int orderId = orderData.getId();
            if (id == R.id.tvAccepted) {
                position = getLayoutPosition();
                Log.d("Adapter position", String.valueOf(position));
                prepareAcceptedDetails(orderId);
            } else if (id == R.id.tvRejected) {
                position = getLayoutPosition();
                Log.d("Adapter position", String.valueOf(position));
                showRejectAlertDialog(orderId);
            } else {
                position = getLayoutPosition();
                Log.d("Adapter position", String.valueOf(position));
                Intent selectedOrderDetailsIntent = new Intent(context, SelectedOrderDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(context.getString(R.string.order_id), String.valueOf(orderId));
                selectedOrderDetailsIntent.putExtras(bundle);
                context.startActivity(selectedOrderDetailsIntent);
            }

        }
    }

    private void showRejectAlertDialog(int orderId) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        alertDialogBuilder.setTitle(context.getString(R.string.order_rejected_message));
        alertDialogBuilder.setCancelable(false);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.layout_order_reject_dialog, null);
        alertDialogBuilder.setView(view);
        etRejectMessage = view.findViewById(R.id.etRejectMessage);
        alertDialogBuilder.setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                prepareRejectedDetails(orderId);
                dialog.cancel();
            }
        });
        alertDialogBuilder.setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void prepareAcceptedDetails(int orderId) {
        showProgressBar(context);
        String token = PreferenceConnector.readString(context, context.getString(R.string.user_token), "");
        OrderAcceptApiCall.serviceCallForOrderAccept(context, null, OrdersFragmentAdapter.this, orderId, token, "");
    }

    private void prepareRejectedDetails(int orderId) {
        rejectOrderMessage = etRejectMessage.getText().toString();
        if (!rejectOrderMessage.isEmpty()) {
            showProgressBar(context);
            String token = PreferenceConnector.readString(context, context.getString(R.string.user_token), "");
            OrderRejectApiCall.serviceCallForOrderReject(context, null, OrdersFragmentAdapter.this, orderId, token, rejectOrderMessage);
        } else {
            Toast.makeText(context, context.getString(R.string.please_enter_reject_message), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_FOR_ORDER_ACCEPT_DETAILS:
                if (jsonResponse != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(jsonResponse);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            listOfOrderData.remove(position);
                            notifyDataSetChanged();
                            Toast.makeText(context, "Order Accepted", Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_FOR_ORDER_REJECT_DETAILS:
                if (jsonResponse != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(jsonResponse);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {

                            listOfOrderData.remove(position);
                            notifyDataSetChanged();
                            Toast.makeText(context, "Order Rejected", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    public void showProgressBar(Context context) {
        try {
            if (progressDialog != null && !progressDialog.isShowing()) {
                progressDialog = DialogUtils.getDialogUtilsInstance().progressDialog(context);
            } else {
                closeProgressbar();
                progressDialog = null;
                progressDialog = DialogUtils.getDialogUtilsInstance().progressDialog(context);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void closeProgressbar() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
