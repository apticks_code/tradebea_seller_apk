package com.tradebeaseller.models.responseModels.subCategoryDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class Data {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("ext")
    @Expose
    private String ext;
    @SerializedName("sub_categories")
    @Expose
    private LinkedList<SubCategoryDetails> listOfSubCategoryDetails;
    @SerializedName("brands")
    @Expose
    private LinkedList<BrandDetails> listOfBrandDetails;
    @SerializedName("image")
    @Expose
    private String imageUrl = "";


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public LinkedList<SubCategoryDetails> getListOfSubCategoryDetails() {
        return listOfSubCategoryDetails;
    }

    public void setListOfSubCategoryDetails(LinkedList<SubCategoryDetails> listOfSubCategoryDetails) {
        this.listOfSubCategoryDetails = listOfSubCategoryDetails;
    }

    public LinkedList<BrandDetails> getListOfBrandDetails() {
        return listOfBrandDetails;
    }

    public void setListOfBrandDetails(LinkedList<BrandDetails> listOfBrandDetails) {
        this.listOfBrandDetails = listOfBrandDetails;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
