package com.tradebeaseller.models.requestModels.returnAcceptOrderRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReturnAcceptOrderRequest {

    @SerializedName("return_request_id")
    @Expose
    private Integer returnRequestId;
    @SerializedName("status")
    @Expose
    private Integer status;


    public Integer getReturnRequestId() {
        return returnRequestId;
    }

    public void setReturnRequestId(Integer returnRequestId) {
        this.returnRequestId = returnRequestId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
