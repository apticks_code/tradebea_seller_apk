package com.tradebeaseller.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.tradebeaseller.ApiCalls.SubSubCategoryDetailsApiCall;
import com.tradebeaseller.R;
import com.tradebeaseller.adapters.SubSubCategoryDetailsAdapter;
import com.tradebeaseller.interfaces.CloseCallBack;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.models.responseModels.subSubCategoryDetailsResponse.Data;
import com.tradebeaseller.models.responseModels.subSubCategoryDetailsResponse.SubSubCategoryDetails;
import com.tradebeaseller.models.responseModels.subSubCategoryDetailsResponse.SubSubCategoryDetailsResponse;
import com.tradebeaseller.utils.Constants;

import java.util.LinkedList;

public class SubSubCategoryDetailsActivity extends BaseActivity implements HttpReqResCallBack, View.OnClickListener, CloseCallBack {

    private ImageView ivBackArrow;
    private TextView tvSave, tvError;
    private RecyclerView rvSubSubCategoryDetails;

    private LinkedList<SubSubCategoryDetails> listOfSubSubCategoryDetails;

    private String selectedSubCategoryId = "";
    private String selectedSubSubCategoryId = "";
    private String selectedSubSubCategoryName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_sub_sub_categories_details);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.selected_sub_category_id)))
                selectedSubCategoryId = bundle.getString(getString(R.string.selected_sub_category_id), "");
            if (bundle.containsKey(getString(R.string.sub_sub_category_id)))
                selectedSubSubCategoryId = bundle.getString(getString(R.string.sub_sub_category_id), "");
            if (bundle.containsKey(getString(R.string.sub_sub_category_name)))
                selectedSubSubCategoryName = bundle.getString(getString(R.string.sub_sub_category_name), "");
        }
    }

    private void initializeUi() {
        tvSave = findViewById(R.id.tvSave);
        tvError = findViewById(R.id.tvError);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        rvSubSubCategoryDetails = findViewById(R.id.rvSubSubCategoryDetails);
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareDetails() {
        showProgressBar(this);
        SubSubCategoryDetailsApiCall.serviceCallForSubSubCategoryDetails(this, null, null, selectedSubCategoryId);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_SUB_SUB_CATEGORY_DETAILS:
                if (jsonResponse != null) {
                    SubSubCategoryDetailsResponse subSubCategoryDetailsResponse = new Gson().fromJson(jsonResponse, SubSubCategoryDetailsResponse.class);
                    if (subSubCategoryDetailsResponse != null) {
                        boolean status = subSubCategoryDetailsResponse.getStatus();
                        if (status) {
                            Data data = subSubCategoryDetailsResponse.getData();
                            if (data != null) {
                                listOfSubSubCategoryDetails = data.getListOfSubSubCategoryDetails();
                                if (listOfSubSubCategoryDetails != null) {
                                    if (listOfSubSubCategoryDetails.size() != 0) {
                                        listIsFull();
                                        initializeAdapter();
                                    } else {
                                        listIsEmpty();
                                    }
                                } else {
                                    listIsEmpty();
                                }
                            }
                        }
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    private void initializeAdapter() {
        SubSubCategoryDetailsAdapter subSubCategoryDetailsAdapter = new SubSubCategoryDetailsAdapter(this, listOfSubSubCategoryDetails, selectedSubSubCategoryId, selectedSubSubCategoryName, tvSave);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvSubSubCategoryDetails.setLayoutManager(layoutManager);
        rvSubSubCategoryDetails.setItemAnimator(new DefaultItemAnimator());
        rvSubSubCategoryDetails.setAdapter(subSubCategoryDetailsAdapter);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvSubSubCategoryDetails.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvSubSubCategoryDetails.setVisibility(View.GONE);
    }

    @Override
    public void close() {
        finish();
    }
}
