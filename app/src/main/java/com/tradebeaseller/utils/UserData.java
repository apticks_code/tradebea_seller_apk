package com.tradebeaseller.utils;

import android.content.Context;

import androidx.fragment.app.Fragment;

import com.tradebeaseller.fragments.OrdersFragment;
import com.tradebeaseller.models.responseModels.editProductResponse.EditProductProductImage;
import com.tradebeaseller.models.responseModels.editProductResponse.EditProductSellerVariantDetails;

import java.util.LinkedList;

public class UserData {

    private static UserData userData = null;

    private Context context;
    private Context selectedOrderDetailsContext;

    private Fragment fragment;

    private Fragment walletFragment;
    private Fragment ordersFragment;

    private EditProductSellerVariantDetails editProductSellerVariantDetails;

    private LinkedList<EditProductProductImage> listOfEditProductProductImage;

    private UserData() {
    }

    public static UserData getInstance() {
        if (userData == null) {
            userData = new UserData();
        }
        return userData;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public EditProductSellerVariantDetails getEditProductSellerVariantDetails() {
        return editProductSellerVariantDetails;
    }

    public void setEditProductSellerVariantDetails(EditProductSellerVariantDetails editProductSellerVariantDetails) {
        this.editProductSellerVariantDetails = editProductSellerVariantDetails;
    }

    public LinkedList<EditProductProductImage> getListOfEditProductProductImage() {
        return listOfEditProductProductImage;
    }

    public void setListOfEditProductProductImage(LinkedList<EditProductProductImage> listOfEditProductProductImage) {
        this.listOfEditProductProductImage = listOfEditProductProductImage;
    }

    public Fragment getOrdersFragment() {
        return ordersFragment;
    }

    public void setOrdersFragment(Fragment ordersFragment) {
        this.ordersFragment = ordersFragment;
    }

    public Context getSelectedOrderDetailsContext() {
        return selectedOrderDetailsContext;
    }

    public void setSelectedOrderDetailsContext(Context selectedOrderDetailsContext) {
        this.selectedOrderDetailsContext = selectedOrderDetailsContext;
    }

    public Fragment getWalletFragment() {
        return walletFragment;
    }

    public void setWalletFragment(Fragment walletFragment) {
        this.walletFragment = walletFragment;
    }
}
