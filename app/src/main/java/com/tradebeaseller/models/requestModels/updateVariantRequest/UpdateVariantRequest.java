package com.tradebeaseller.models.requestModels.updateVariantRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateVariantRequest {

    @SerializedName("variant_id")
    @Expose
    private Integer variantId;
    @SerializedName("mrp")
    @Expose
    private String mrp;

    public Integer getVariantId() {
        return variantId;
    }

    public void setVariantId(Integer variantId) {
        this.variantId = variantId;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }
}
