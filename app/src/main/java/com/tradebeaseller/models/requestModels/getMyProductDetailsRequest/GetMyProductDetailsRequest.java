package com.tradebeaseller.models.requestModels.getMyProductDetailsRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetMyProductDetailsRequest {

    @SerializedName("page_no")
    @Expose
    public int pageNo;
    @SerializedName("q")
    @Expose
    public String q;
    @SerializedName("menu_id")
    @Expose
    public int menuId;
    @SerializedName("cat_id")
    @Expose
    public int catId;
    @SerializedName("sub_cat_id")
    @Expose
    public int subCatId;
    @SerializedName("sub_sub_cat_id")
    @Expose
    public String subSubCatId;
    @SerializedName("brand_id")
    @Expose
    public int brandId;
}
