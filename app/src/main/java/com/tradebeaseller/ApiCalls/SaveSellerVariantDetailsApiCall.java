package com.tradebeaseller.ApiCalls;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.jsonBuilderParser.JsonBuilderParser;
import com.tradebeaseller.models.requestModels.updateSellerVariantDetailsRequest.UpdateSellerVariantDetailsRequest;
import com.tradebeaseller.utils.CommonMethods;
import com.tradebeaseller.utils.Constants;

import org.json.JSONObject;

public class SaveSellerVariantDetailsApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallForSaveSellerVariantDetails(Context context, Fragment fragment, RecyclerView.Adapter adapter, int id, int productId, int variantId, String sellerPrice, String sellerMrp, String quantity, String status, String token) {
        String url = Constants.UPDATE_PRODUCT_SELLER_VARIANT_DETAILS;
        UpdateSellerVariantDetailsRequest updateSellerVariantDetailsRequest = new UpdateSellerVariantDetailsRequest();
        updateSellerVariantDetailsRequest.id = id;
        updateSellerVariantDetailsRequest.productId = productId;
        updateSellerVariantDetailsRequest.variantId = variantId;
        updateSellerVariantDetailsRequest.sellingPrice = sellerPrice;
        updateSellerVariantDetailsRequest.sellerMrp = sellerMrp;
        updateSellerVariantDetailsRequest.qty = quantity;
        updateSellerVariantDetailsRequest.status = status;

        JSONObject postObject = JsonBuilderParser.jsonBuilder(updateSellerVariantDetailsRequest);
        AndroidNetworking.post(url)
                .addJSONObjectBody(postObject)
                .setPriority(Priority.HIGH)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("X_AUTH_TOKEN", token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_UPDATE_SELLER_VARIANT_DETAILS);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_UPDATE_SELLER_VARIANT_DETAILS);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_UPDATE_SELLER_VARIANT_DETAILS);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_UPDATE_SELLER_VARIANT_DETAILS);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_UPDATE_SELLER_VARIANT_DETAILS);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_UPDATE_SELLER_VARIANT_DETAILS);
                        }
                    }
                });
    }
}
