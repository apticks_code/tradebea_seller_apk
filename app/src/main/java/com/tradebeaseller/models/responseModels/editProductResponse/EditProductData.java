package com.tradebeaseller.models.responseModels.editProductResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class EditProductData {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("product_code")
    @Expose
    private String productCode;
    @SerializedName("desc")
    @Expose
    private String desc = "";
    @SerializedName("menu_id")
    @Expose
    private Integer menuId;
    @SerializedName("cat_id")
    @Expose
    private Integer catId;
    @SerializedName("sub_cat_id")
    @Expose
    private Object subCatId;
    @SerializedName("sub_sub_cat_id")
    @Expose
    private Object subSubCatId;
    @SerializedName("brand_id")
    @Expose
    private Integer brandId;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("sounds_like")
    @Expose
    private String soundsLike;
    @SerializedName("created_user_id")
    @Expose
    private Integer createdUserId;
    @SerializedName("updated_user_id")
    @Expose
    private Object updatedUserId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private Object deletedAt;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("menu")
    @Expose
    private EditProductMenuDetails editProductMenuDetails;
    @SerializedName("category")
    @Expose
    private EditProductCategoryDetails editProductCategoryDetails;
    @SerializedName("sub_category")
    @Expose
    private EditProductSubCategoryDetails editProductSubCategoryDetails;
    @SerializedName("sub_sub_category")
    @Expose
    private EditProductSubSubCategoryDetails editProductSubSubCategoryDetails;
    @SerializedName("brand")
    @Expose
    private EditProductBrandDetails editProductBrandDetails;
    @SerializedName("variants")
    @Expose
    private LinkedList<EditProductVariantDetails> listOfEditProductVariantDetails;
    @SerializedName("seller_variants")
    @Expose
    private LinkedList<EditProductSellerVariantDetails> listOfEditProductSellerVariantDetails;
    @SerializedName("product_images")
    @Expose
    private LinkedList<EditProductProductImage> listOfEditProductProductImage;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Object getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(Object subCatId) {
        this.subCatId = subCatId;
    }

    public Object getSubSubCatId() {
        return subSubCatId;
    }

    public void setSubSubCatId(Object subSubCatId) {
        this.subSubCatId = subSubCatId;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getSoundsLike() {
        return soundsLike;
    }

    public void setSoundsLike(String soundsLike) {
        this.soundsLike = soundsLike;
    }

    public Integer getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(Integer createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Object getUpdatedUserId() {
        return updatedUserId;
    }

    public void setUpdatedUserId(Object updatedUserId) {
        this.updatedUserId = updatedUserId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public EditProductMenuDetails getEditProductMenuDetails() {
        return editProductMenuDetails;
    }

    public void setEditProductMenuDetails(EditProductMenuDetails editProductMenuDetails) {
        this.editProductMenuDetails = editProductMenuDetails;
    }

    public EditProductCategoryDetails getEditProductCategoryDetails() {
        return editProductCategoryDetails;
    }

    public void setEditProductCategoryDetails(EditProductCategoryDetails editProductCategoryDetails) {
        this.editProductCategoryDetails = editProductCategoryDetails;
    }

    public EditProductSubCategoryDetails getEditProductSubCategoryDetails() {
        return editProductSubCategoryDetails;
    }

    public void setEditProductSubCategoryDetails(EditProductSubCategoryDetails editProductSubCategoryDetails) {
        this.editProductSubCategoryDetails = editProductSubCategoryDetails;
    }

    public EditProductSubSubCategoryDetails getEditProductSubSubCategoryDetails() {
        return editProductSubSubCategoryDetails;
    }

    public void setEditProductSubSubCategoryDetails(EditProductSubSubCategoryDetails editProductSubSubCategoryDetails) {
        this.editProductSubSubCategoryDetails = editProductSubSubCategoryDetails;
    }

    public EditProductBrandDetails getEditProductBrandDetails() {
        return editProductBrandDetails;
    }

    public void setEditProductBrandDetails(EditProductBrandDetails editProductBrandDetails) {
        this.editProductBrandDetails = editProductBrandDetails;
    }

    public LinkedList<EditProductVariantDetails> getListOfEditProductVariantDetails() {
        return listOfEditProductVariantDetails;
    }

    public void setListOfEditProductVariantDetails(LinkedList<EditProductVariantDetails> listOfEditProductVariantDetails) {
        this.listOfEditProductVariantDetails = listOfEditProductVariantDetails;
    }

    public LinkedList<EditProductProductImage> getListOfEditProductProductImage() {
        return listOfEditProductProductImage;
    }

    public void setListOfEditProductProductImage(LinkedList<EditProductProductImage> listOfEditProductProductImage) {
        this.listOfEditProductProductImage = listOfEditProductProductImage;
    }

    public LinkedList<EditProductSellerVariantDetails> getListOfEditProductSellerVariantDetails() {
        return listOfEditProductSellerVariantDetails;
    }

    public void setListOfEditProductSellerVariantDetails(LinkedList<EditProductSellerVariantDetails> listOfEditProductSellerVariantDetails) {
        this.listOfEditProductSellerVariantDetails = listOfEditProductSellerVariantDetails;
    }
}
