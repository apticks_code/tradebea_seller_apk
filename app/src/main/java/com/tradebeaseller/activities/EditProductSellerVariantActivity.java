package com.tradebeaseller.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.squareup.picasso.Picasso;
import com.tradebeaseller.ApiCalls.SaveSellerVariantDetailsApiCall;
import com.tradebeaseller.R;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.models.responseModels.editProductResponse.EditProductProductImage;
import com.tradebeaseller.models.responseModels.editProductResponse.EditProductSellerVariantDetails;
import com.tradebeaseller.models.responseModels.editProductResponse.VariantsValue;
import com.tradebeaseller.utils.Constants;
import com.tradebeaseller.utils.PreferenceConnector;
import com.tradebeaseller.utils.UserData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.Timer;

public class EditProductSellerVariantActivity extends BaseActivity implements View.OnClickListener, /*CompoundButton.OnCheckedChangeListener,*/ HttpReqResCallBack {

    private ViewPager viewPager;
    private ImageView ivBackArrow;
    private SwitchCompat statusSwitch;
    private LinearLayout llBanners, llDots, llSaveChanges;
    private EditText etSellerPrice, etSellerMRP, etQuantity;
    private EditProductSellerVariantDetails editProductSellerVariantDetails;
    private TextView tvMenu, tvCategory, tvSubCategory, tvBrand, tvProductId, tvProductName, tvVariantName, tvMrp, tvSaveChanges, tvStatus;

    private LinkedList<EditProductProductImage> listOfEditProductProductImage;

    private String token = "";
    private String productId = "";
    private String productName = "";
    private String selectedMenuName = "";
    private String selectedBrandName = "";
    private String selectedCategoryName = "";
    private String selectedSubCategoryName = "";

    private int count = 0;
    private int status = 1;

    private boolean switchStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_edit_product_seller_variant);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.product_id)))
                productId = bundle.getString(getString(R.string.product_id), "");
            if (bundle.containsKey(getString(R.string.product_name)))
                productName = bundle.getString(getString(R.string.product_name), "");
            if (bundle.containsKey(getString(R.string.selected_menu_name)))
                selectedMenuName = bundle.getString(getString(R.string.selected_menu_name), "");
            if (bundle.containsKey(getString(R.string.selected_category_name)))
                selectedCategoryName = bundle.getString(getString(R.string.selected_category_name), "");
            if (bundle.containsKey(getString(R.string.selected_sub_category_name)))
                selectedSubCategoryName = bundle.getString(getString(R.string.selected_sub_category_name), "");
            if (bundle.containsKey(getString(R.string.selected_brand_name)))
                selectedBrandName = bundle.getString(getString(R.string.selected_brand_name), "");
        }

        listOfEditProductProductImage = UserData.getInstance().getListOfEditProductProductImage();
        editProductSellerVariantDetails = UserData.getInstance().getEditProductSellerVariantDetails();
    }

    private void initializeUi() {
        tvMrp = findViewById(R.id.tvMrp);
        llDots = findViewById(R.id.llDots);
        tvMenu = findViewById(R.id.tvMenu);
        tvBrand = findViewById(R.id.tvBrand);
        tvStatus = findViewById(R.id.tvStatus);
        llBanners = findViewById(R.id.llBanners);
        viewPager = findViewById(R.id.viewPager);
        tvCategory = findViewById(R.id.tvCategory);
        etQuantity = findViewById(R.id.etQuantity);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        tvProductId = findViewById(R.id.tvProductId);
        etSellerMRP = findViewById(R.id.etSellerMRP);
        statusSwitch = findViewById(R.id.statusSwitch);
        tvSaveChanges = findViewById(R.id.tvSaveChanges);
        llSaveChanges = findViewById(R.id.llSaveChanges);
        tvSubCategory = findViewById(R.id.tvSubCategory);
        tvProductName = findViewById(R.id.tvProductName);
        tvVariantName = findViewById(R.id.tvVariantName);
        etSellerPrice = findViewById(R.id.etSellerPrice);
        tvVariantName = findViewById(R.id.tvVariantName);
        statusSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (statusSwitch.isChecked()) {
                    tvStatus.setText(getString(R.string.active));
                    switchStatus = true;
                    status = 1;
                } else {
                    tvStatus.setText(getString(R.string.in_active));
                    switchStatus = false;
                    status = 2;
                }
            }
        });
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
        tvSaveChanges.setOnClickListener(this);
        llSaveChanges.setOnClickListener(this);
        //statusSwitch.setOnCheckedChangeListener(this);
    }

    private void prepareDetails() {
        tvProductName.setText(productName);
        tvProductId.setText(productId);
        tvMenu.setText(selectedMenuName);
        tvCategory.setText(selectedCategoryName);
        tvSubCategory.setText(selectedSubCategoryName);
        tvBrand.setText(selectedBrandName);


        if (editProductSellerVariantDetails != null) {
            int sellerMrp = editProductSellerVariantDetails.getSellerMrp();
            int sellerPrice = editProductSellerVariantDetails.getSellingPrice();
            int quantity = editProductSellerVariantDetails.getQty();
            status = editProductSellerVariantDetails.getStatus();
            double mrp = editProductSellerVariantDetails.getMrp();
            LinkedList<VariantsValue> listOfVariantsValues = editProductSellerVariantDetails.getListOfVariantsValues();
            etSellerMRP.setText(String.valueOf(sellerMrp));
            etSellerPrice.setText(String.valueOf(sellerPrice));
            etQuantity.setText(String.valueOf(quantity));
            tvMrp.setText(String.valueOf(mrp));
            if (status == 1) {
                tvStatus.setText(getString(R.string.active));
                switchStatus = true;
                statusSwitch.setChecked(true);
            } else {
                tvStatus.setText(getString(R.string.in_active));
                switchStatus = false;
                statusSwitch.setChecked(false);
            }
            StringBuilder variantInfoName = new StringBuilder();
            for (int index = 0; index < listOfVariantsValues.size(); index++) {
                VariantsValue variantsValue = listOfVariantsValues.get(index);
                if (index == 0) {
                    variantInfoName.append(variantsValue.getOptionItem().getValue());
                } else {
                    variantInfoName.append(" | ").append(variantsValue.getOptionItem().getValue());
                }
            }
            tvVariantName.setText(variantInfoName);
        }

        if (listOfEditProductProductImage != null) {
            if (listOfEditProductProductImage.size() != 0) {
                llBanners.setVisibility(View.VISIBLE);
                initializeViewPager();
            } else {
                listOfEditProductProductImage = new LinkedList<>();
                llBanners.setVisibility(View.GONE);
            }
        } else {
            listOfEditProductProductImage = new LinkedList<>();
            llBanners.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            case R.id.llSaveChanges:
            case R.id.tvSaveChanges:
                prepareSaveChanges();
                break;
            default:
                break;
        }
    }

    private void prepareSaveChanges() {
        showProgressBar(this);
        int variantId = editProductSellerVariantDetails.getVariantId();
        int id = editProductSellerVariantDetails.getId();
        int productId = editProductSellerVariantDetails.getProductId();
        String sellerPrice = etSellerPrice.getText().toString();
        String sellerMrp = etSellerMRP.getText().toString();
        String quantity = etQuantity.getText().toString();
        String status = "";
        if (switchStatus) {
            status = "1";
        } else {
            status = "2";
        }
        token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
        SaveSellerVariantDetailsApiCall.serviceCallForSaveSellerVariantDetails(this, null, null, id, productId, variantId, sellerPrice, sellerMrp, quantity, status, token);
    }

    private void initializeViewPager() {
        ProductBannersImagesAdapter productBannersImagesAdapter = new ProductBannersImagesAdapter(this, listOfEditProductProductImage);
        viewPager.setAdapter(productBannersImagesAdapter);
        for (int index = 0; index < productBannersImagesAdapter.getCount(); index++) {
            ImageButton ibDot = new ImageButton(this);
            ibDot.setTag(index);
            ibDot.setImageResource(R.drawable.dot_selector);
            ibDot.setBackgroundResource(0);
            ibDot.setPadding(0, 0, 5, 0);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(60, 60);
            ibDot.setLayoutParams(params);
            if (index == 0)
                ibDot.setSelected(true);
            llDots.addView(ibDot);
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int index = 0; index < productBannersImagesAdapter.getCount(); index++) {
                    if (index != position) {
                        (llDots.findViewWithTag(index)).setSelected(false);
                    }
                }
                (llDots.findViewWithTag(position)).setSelected(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask(), 5000, 5000);
    }

    /*@Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switchStatus = isChecked;
    }*/

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_UPDATE_SELLER_VARIANT_DETAILS:
                if (jsonResponse != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(jsonResponse);
                        boolean status = jsonObject.getBoolean("status");
                        String message = jsonObject.getString("message");
                        if (status) {
                            PreferenceConnector.writeBoolean(this, getString(R.string.refresh_seller_product_details), true);
                            finish();
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    private class ProductBannersImagesAdapter extends PagerAdapter {

        private Context context;
        private LinkedList<EditProductProductImage> listOfEditProductProductImage;

        public ProductBannersImagesAdapter(Context context, LinkedList<EditProductProductImage> listOfEditProductProductImage) {
            this.context = context;
            this.listOfEditProductProductImage = listOfEditProductProductImage;
        }

        @Override
        public int getCount() {
            return listOfEditProductProductImage.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return (view == object);
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.layout_home_banner_images_pager_item, container, false);
            EditProductProductImage editProductProductImage = listOfEditProductProductImage.get(position);
            String bannerImageUrl = editProductProductImage.getImageUrl();
            ImageView ivImage = view.findViewById(R.id.ivImage);
            Picasso.get()
                    .load(bannerImageUrl)
                    .error(R.drawable.image_placeholder)
                    .placeholder(R.drawable.image_placeholder)
                    .into(ivImage);
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            //container.removeView( object);
        }
    }

    private class TimerTask extends java.util.TimerTask {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (listOfEditProductProductImage.size() - 1 != count) {
                        count = count + 1;
                        viewPager.setCurrentItem(count);
                    } else {
                        viewPager.setCurrentItem(0);
                        count = 0;
                    }
                }
            });
        }
    }
}
