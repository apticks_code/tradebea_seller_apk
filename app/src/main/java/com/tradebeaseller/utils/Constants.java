package com.tradebeaseller.utils;

public class Constants {

    public static final String APP_ID = "TWc9PQ==";

    public static final int SPLASH_TIME_OUT = 3000;
    public static final int TIME_OUT_THIRTY_SECONDS = 30000;
    public static final long MIN_TIME_MILLIS = new Long("1598918400000");

    public static final String AUTH_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjkiLCJ1c2VyZGV0YWlsIjp7InVzZXJuYW1lIjoiODUyMjgwODc4NSIsImVtYWlsIjoibWVoYXJncmVwdGhvckBnbWFpbC5jb20iLCJwaG9uZSI6Ijg1MjI4MDg3ODUiLCJpZCI6IjkiLCJncm91cHMiOnsiMyI6eyJpZCI6IjMiLCJ1c2VyX2lkIjoiOSIsIm5hbWUiOiJkZWxpdmVyeSJ9LCI1Ijp7ImlkIjoiNSIsInVzZXJfaWQiOiI5IiwibmFtZSI6InVzZXIifX19LCJ0aW1lIjoxNjQxNzk0NDg1fQ.vLUBdNx1mo9C1oXcxE6OyxMOiqE7Tgpe_BieDqslu5E";

    public static final String BASE_URL = "https://apticks.com/tradebea/";//testing
    //public static final String BASE_URL = "http://tradebea.com/";//production
    public static final String LOGIN_URL = BASE_URL + "auth/api/auth/login";
    public static final String SOCIAL_LOGIN_URL = BASE_URL + "auth/api/auth/social_login/google";
    public static final String GENERATE_OTP_URL = BASE_URL + "auth/api/auth/otp_gen";
    public static final String VERIFY_OTP_URL = BASE_URL + "auth/api/auth/verify_otp";
    public static final String SIGN_UP_URL = BASE_URL + "auth/api/auth/create_user/user";
    public static final String FORGOT_PASSWORD_URL = BASE_URL + "auth/api/auth/forgot_password";
    public static final String GET_PROFILE_DETAILS_URL = BASE_URL + "auth/api/users/profile/r";
    public static final String PROFILE_UPDATE_URL = BASE_URL + "auth/api/users/profile/u/seller";
    public static final String CATALOG_PRODUCT_DETAILS_URL = BASE_URL + "catalogue/api/products/manage_product/products";
    public static final String MY_PRODUCT_DETAILS_URL = BASE_URL + "catalogue/api/products/seller_products/seller_products_list";
    public static final String APPROVED_PRODUCT_DETAILS_URL = BASE_URL + "catalogue/api/products/manage_product/products";
    public static final String PENDING_PRODUCT_DETAILS_URL = BASE_URL + "catalogue/api/products/manage_product/products";
    public static final String MENU_DETAILS_URL = BASE_URL + "catalogue/api/catalogue/menus";
    public static final String CATEGORY_DETAILS_URL = BASE_URL + "catalogue/api/catalogue/menus/";
    public static final String SUB_CATEGORY_DETAILS_URL = BASE_URL + "catalogue/api/catalogue/category/";
    public static final String SUB_SUB_CATEGORY_DETAILS_URL = BASE_URL + "catalogue/api/catalogue/sub_category/";
    public static final String BRAND_DETAILS_URL = BASE_URL + "catalogue/api/catalogue/brands/";
    public static final String OPTIONS_DETAILS_URL = BASE_URL + "catalogue/api/catalogue/options/";
    public static final String GROUPS_DETAILS_URL = BASE_URL + "catalogue/api/catalogue/options/";
    public static final String ITEMS_DETAILS_URL = BASE_URL + "catalogue/api/catalogue/option_items/";
    public static final String SAVE_ADD_PRODUCT_DETAILS_URL = BASE_URL + "catalogue/api/products/manage_product/create_product";
    public static final String DELETE_PRODUCT_DETAILS_URL = BASE_URL + "catalogue/api/products/manage_product/delete_product/";
    public static final String GET_PRODUCT_DETAILS_URL = BASE_URL + "catalogue/api/products/manage_product/products/";
    public static final String DELETE_GENERATED_VARIANT_DETAILS_URL = BASE_URL + "catalogue/api/products/manage_product/delete_variant/";
    public static final String UPDATE_GENERATED_VARIANT_DETAILS_URL = BASE_URL + "catalogue/api/products/manage_product/update_product_variant";
    public static final String DELETE_PRODUCT_IMAGE_DETAILS_URL = BASE_URL + "catalogue/api/products/manage_product/delete_product_image/";
    public static final String UPDATE_PRODUCT_DETAILS_URL = BASE_URL + "catalogue/api/products/manage_product/update_product";
    public static final String ADD_PRODUCT_TO_LIST_URL = BASE_URL + "catalogue/api/products/seller_products/add_to_my_list/";
    public static final String GET_SELLER_PRODUCT_DETAILS_URL = BASE_URL + "catalogue/api/products/seller_products/seller_product_details/";
    public static final String UPDATE_PRODUCT_SELLER_VARIANT_DETAILS = BASE_URL + "catalogue/api/products/seller_products/update_seller_varinat";
    public static final String ORDER_DETAILS_URL = BASE_URL + "ecom/api/ecom/orders/seller_orders";
    public static final String SELECTED_ORDER_DETAILS_URL = BASE_URL + "ecom/api/ecom/orders/order_details/";
    public static final String ORDER_ACCEPT_DETAILS_URL = BASE_URL + "ecom/api/ecom/orders/seller_changes_status";
    public static final String ORDER__REJECT_DETAILS_URL = BASE_URL + "ecom/api/ecom/orders/seller_changes_status";
    public static final String GET_DASHBOARD_DETAILS_URL = BASE_URL + "ecom/api/ecom/seller_dashboard";
    public static final String DEVICE_TOKEN_URL = BASE_URL + "admin/api/fcm_notify/grant_fcm_permission";
    public static final String VERIFY_PICK_UP_OTP_URL = BASE_URL + "ecom/api/ecom/orders/verify_pickup_otp";
    public static final String RETURN_ORDER_URL = BASE_URL + "ecom/api/returns/return_requests";
    public static final String ACCEPT_RETURN_ORDER = BASE_URL + "ecom/api/returns/return_requests";
    public static final String WALLET_BALANCE_URL = BASE_URL + "payment/api/payment/wallet_account";

    public static final int REQUEST_CODE_FOR_EXTERNAL_STORAGE_PERMISSION = 1;
    public static final int REQUEST_CODE_FOR_CAMERA_PERMISSION = 2;
    public static final int REQUEST_CODE_FOR_LOCATION_PERMISSION = 3;
    public static final int REQUEST_CODE_FOR_PHONE_PERMISSION = 4;
    public static final int PICK_GALLERY = 5;
    public static final int REQUEST_CODE_CAPTURE_IMAGE = 6;

    // Api calls
    public static final int SERVICE_CALL_FOR_LOGIN = 1;
    public static final int SERVICE_CALL_FOR_GENERATE_OTP = 2;
    public static final int SERVICE_CALL_FOR_VERIFY_OTP = 3;
    public static final int SERVICE_CALL_FOR_SIGN_UP = 4;
    public static final int SERVICE_CALL_FOR_FORGOT_PASSWORD = 5;
    public static final int SERVICE_CALL_TO_GET_PROFILE_DETAILS = 6;
    public static final int SERVICE_CALL_FOR_SOCIAL_LOGIN = 7;
    public static final int SERVICE_CALL_FOR_PROFILE_UPDATE = 8;
    public static final int SERVICE_CALL_TO_GET_CATALOG_PRODUCT_DETAILS = 9;
    public static final int SERVICE_CALL_TO_GET_MY_PRODUCT_DETAILS = 10;
    public static final int SERVICE_CALL_TO_GET_APPROVED_PRODUCT_DETAILS = 11;
    public static final int SERVICE_CALL_TO_GET_PENDING_PRODUCT_DETAILS = 12;
    public static final int SERVICE_CALL_TO_GET_MENU_DETAILS = 13;
    public static final int SERVICE_CALL_TO_GET_CATEGORY_DETAILS = 14;
    public static final int SERVICE_CALL_TO_GET_SUB_CATEGORY_DETAILS = 15;
    public static final int SERVICE_CALL_TO_GET_BRAND_DETAILS = 16;
    public static final int SERVICE_CALL_TO_GET_SUB_SUB_CATEGORY_DETAILS = 17;
    public static final int SERVICE_CALL_TO_GET_OPTIONS_DETAILS = 18;
    public static final int SERVICE_CALL_TO_GET_GROUP_DETAILS = 19;
    public static final int SERVICE_CALL_TO_GET_ITEMS_DETAILS = 20;
    public static final int SERVICE_CALL_FOR_ADD_SAVE_PRODUCT_DETAILS = 21;
    public static final int SERVICE_CALL_TO_DELETE_PRODUCT = 22;
    public static final int SERVICE_CALL_TO_GET_PRODUCT_DETAILS = 23;
    public static final int SERVICE_CALL_TO_DELETE_GENERATED_VARIANT = 24;
    public static final int SERVICE_CALL_TO_UPDATE_GENERATED_VARIANT = 25;
    public static final int SERVICE_CALL_TO_DELETE_PRODUCT_IMAGE = 26;
    public static final int SERVICE_CALL_FOR_UPDATE_PRODUCT_DETAILS = 27;
    public static final int SERVICE_CALL_TO_ADD_PRODUCT_TO_LIST = 28;
    public static final int SERVICE_CALL_TO_GET_SELLER_PRODUCT_DETAILS = 29;
    public static final int SERVICE_CALL_TO_UPDATE_SELLER_VARIANT_DETAILS = 30;
    public static final int SERVICE_CALL_TO_GET_ORDER_DETAILS = 31;
    public static final int SERVICE_CALL_TO_GET_SELECTED_ORDER_DETAILS = 32;
    public static final int SERVICE_CALL_FOR_ORDER_ACCEPT_DETAILS = 33;
    public static final int SERVICE_CALL_FOR_ORDER_REJECT_DETAILS = 34;
    public static final int SERVICE_CALL_TO_GET_DASHBOARD_DETAILS = 35;
    public static final int SERVICE_CALL_FOR_DEVICE_TOKEN = 36;
    public static final int SERVICE_CALL_FOR_VERIFY_PICK_UP_OTP = 37;
    public static final int SERVICE_CALL_TO_GET_RETURN_ORDER_DETAILS = 38;
    public static final int SERVICE_CALL_TO_ACCEPT_RETURN_ORDER = 39;
    public static final int SERVICE_CALL_TO_GET_WALLET_BALANCE = 40;
    public static final int SERVICE_CALL_TO_GET_TRANSACTION_DETAILS = 41;

    public static final String NOTIFICATION_CHANNEL_ID = "tradebeaSeller_channel";
}
