package com.tradebeaseller.activities;

import android.app.DatePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.tradebeaseller.R;
import com.tradebeaseller.interfaces.OrderFilterDetailsCallBack;
import com.tradebeaseller.utils.UserData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.tradebeaseller.utils.Constants.MIN_TIME_MILLIS;

public class OrderFilterDetailsActivity extends BaseActivity implements View.OnClickListener {

    private EditText etSearch;
    private TextView tvStartDate, tvEndDate, tvApply;
    private DatePickerDialog datePickerDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_order_filter_details);
        initializeUi();
        initializeListeners();
    }

    private void initializeUi() {
        tvApply = findViewById(R.id.tvApply);
        etSearch = findViewById(R.id.etSearch);
        tvEndDate = findViewById(R.id.tvEndDate);
        tvStartDate = findViewById(R.id.tvStartDate);
    }

    private void initializeListeners() {
        tvApply.setOnClickListener(this);
        tvEndDate.setOnClickListener(this);
        tvStartDate.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.tvApply:
                prepareApplyDetails();
                break;
            case R.id.tvStartDate:
                prepareStartDateDetails();
                break;
            case R.id.tvEndDate:
                prepareEndDateDetails();
                break;
            default:
                break;
        }
    }


    private void prepareApplyDetails() {
        String searchText = etSearch.getText().toString().trim();
        String startDate = tvStartDate.getText().toString().trim();
        String endDate = tvEndDate.getText().toString().trim();

        long startDate_long = dateConversion(startDate);
        long endDate_long = dateConversion(endDate);

        if (startDate_long < endDate_long) {
            Fragment fragment = UserData.getInstance().getOrdersFragment();
            if (fragment != null) {
                OrderFilterDetailsCallBack orderFilterDetailsCallBack = (OrderFilterDetailsCallBack) fragment;
                orderFilterDetailsCallBack.orderFilterDetails(searchText, startDate, endDate);
            }
            datePickerDialog.dismiss();
        } else {
            Toast.makeText(this, "Please select correct dates", Toast.LENGTH_SHORT).show();
        }
    }

    private long dateConversion(String date) {
        long req = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date optDate = sdf.parse(date);
            req = optDate.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return req;
    }

    private void prepareStartDateDetails() {
        showDatePicker(tvStartDate);
    }

    private void prepareEndDateDetails() {
        showDatePicker(tvEndDate);
    }

    private void showDatePicker(TextView textView) {

        Calendar calendar = Calendar.getInstance();
        Calendar date = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(this, R.style.DateAndTimePicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date.set(year, monthOfYear, dayOfMonth);
                //String date = year + "-" + ((monthOfYear + 1) < 10 ? ("0" + (monthOfYear + 1)) : ((monthOfYear + 1))) + "-" + (dayOfMonth < 10 ? ("0" + dayOfMonth) : (dayOfMonth));
                String date = (dayOfMonth < 10 ? ("0" + dayOfMonth) : (dayOfMonth)) + "-" + ((monthOfYear + 1) < 10 ? ("0" + (monthOfYear + 1)) : ((monthOfYear + 1))) + "-" + year;
                textView.setText(date);
            }
        }, year, month, day);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.getDatePicker().setMinDate(MIN_TIME_MILLIS);
        datePickerDialog.show();
    }
}
