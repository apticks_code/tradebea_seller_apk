package com.tradebeaseller.models.responseModels.categoryDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class SelectedMenuData {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("ext")
    @Expose
    private String ext;
    @SerializedName("categories")
    @Expose
    private LinkedList<CategoryDetails> listOfCategoryDetails;
    @SerializedName("image")
    @Expose
    private String imageUrl = "";


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public LinkedList<CategoryDetails> getListOfCategoryDetails() {
        return listOfCategoryDetails;
    }

    public void setListOfCategoryDetails(LinkedList<CategoryDetails> listOfCategoryDetails) {
        this.listOfCategoryDetails = listOfCategoryDetails;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
