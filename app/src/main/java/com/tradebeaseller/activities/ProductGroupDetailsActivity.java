package com.tradebeaseller.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.tradebeaseller.ApiCalls.GroupsApiCall;
import com.tradebeaseller.R;
import com.tradebeaseller.adapters.ProductGroupDetailsAdapter;
import com.tradebeaseller.interfaces.CloseCallBack;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.models.responseModels.groupDetailsResponse.GroupData;
import com.tradebeaseller.models.responseModels.groupDetailsResponse.GroupDetailsResponse;
import com.tradebeaseller.utils.Constants;

import java.util.LinkedList;

public class ProductGroupDetailsActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack, CloseCallBack {

    private ImageView ivBackArrow;
    private TextView tvSave, tvError;
    private RecyclerView rvProductGroupDetails;

    private LinkedList<GroupData> listOfGroupData;

    private String selectedCategoryId = "";
    private String selectedProductOptionId = "";
    private String selectedProductOptionName = "";
    private String selectedProductGroupId = "";
    private String selectedProductGroupName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_group_option_details);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.selected_category_id)))
                selectedCategoryId = bundle.getString(getString(R.string.selected_category_id), "");
            if (bundle.containsKey(getString(R.string.selected_product_option_id)))
                selectedProductOptionId = bundle.getString(getString(R.string.selected_product_option_id), "");
            if (bundle.containsKey(getString(R.string.selected_product_option_name)))
                selectedProductOptionName = bundle.getString(getString(R.string.selected_product_option_name), "");
            if (bundle.containsKey(getString(R.string.selected_product_group_id)))
                selectedProductGroupId = bundle.getString(getString(R.string.selected_product_group_id), "");
            if (bundle.containsKey(getString(R.string.selected_product_group_name)))
                selectedProductGroupName = bundle.getString(getString(R.string.selected_product_group_name), "");
        }
    }

    private void initializeUi() {
        tvSave = findViewById(R.id.tvSave);
        tvError = findViewById(R.id.tvError);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        rvProductGroupDetails = findViewById(R.id.rvProductGroupDetails);
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareDetails() {
        showProgressBar(this);
        GroupsApiCall.serviceCallToGetGroupsDetails(this, null, null, selectedCategoryId, selectedProductOptionId);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_GROUP_DETAILS:
                if (jsonResponse != null) {
                    GroupDetailsResponse groupDetailsResponse = new Gson().fromJson(jsonResponse, GroupDetailsResponse.class);
                    if (groupDetailsResponse != null) {
                        boolean status = groupDetailsResponse.getStatus();
                        if (status) {
                            listOfGroupData = groupDetailsResponse.getListOfGroupData();
                            if (listOfGroupData != null) {
                                if (listOfGroupData.size() != 0) {
                                    listIsFull();
                                    initializeAdapter();
                                } else {
                                    listIsEmpty();
                                }
                            } else {
                                listIsEmpty();
                            }
                        }
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    private void initializeAdapter() {
        ProductGroupDetailsAdapter productGroupDetailsAdapter = new ProductGroupDetailsAdapter(this, listOfGroupData, selectedProductGroupId, selectedProductGroupName, tvSave);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvProductGroupDetails.setLayoutManager(layoutManager);
        rvProductGroupDetails.setItemAnimator(new DefaultItemAnimator());
        rvProductGroupDetails.setAdapter(productGroupDetailsAdapter);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvProductGroupDetails.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvProductGroupDetails.setVisibility(View.GONE);
    }

    @Override
    public void close() {
        finish();
    }
}
