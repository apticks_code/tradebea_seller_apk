package com.tradebeaseller.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mukesh.OtpView;
import com.tradebeaseller.ApiCalls.VerifyPickUpOTPApiCall;
import com.tradebeaseller.R;
import com.tradebeaseller.interfaces.CloseCallBack;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.models.responseModels.verifyPickUpOTPResponse.VerifyPickUpOTPResponse;
import com.tradebeaseller.utils.Constants;
import com.tradebeaseller.utils.UserData;

public class VerifyDeliveryBoyOTPActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack {

    private OtpView otpView;
    private TextView tvSubmit;
    private ImageView ivBackArrow;
    private LinearLayout llSubmit;

    private String selectedOrderId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_verify_delivery_boy_otp);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.selected_order_id))) {
                selectedOrderId = bundle.getString(getString(R.string.selected_order_id));
            }
        }
    }

    private void initializeUi() {
        otpView = findViewById(R.id.otpView);
        llSubmit = findViewById(R.id.llSubmit);
        tvSubmit = findViewById(R.id.tvSubmit);
        ivBackArrow = findViewById(R.id.ivBackArrow);
    }

    private void initializeListeners() {
        llSubmit.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            case R.id.llSubmit:
                prepareDetails();
                break;
            case R.id.tvSubmit:
                prepareDetails();
                break;
            default:
                break;
        }
    }

    private void prepareDetails() {
        String otp = otpView.getText().toString();
        if (!otp.isEmpty()) {
            showProgressBar(this);
            VerifyPickUpOTPApiCall.serviceCallForVerifyPickUpOTP(this, null, null, selectedOrderId, otp);
        } else {
            Toast.makeText(this, getString(R.string.please_enter_otp), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_FOR_VERIFY_PICK_UP_OTP:
                if (jsonResponse != null) {
                    VerifyPickUpOTPResponse verifyPickUpOTPResponse = new Gson().fromJson(jsonResponse, VerifyPickUpOTPResponse.class);
                    if (verifyPickUpOTPResponse != null) {
                        Boolean status = verifyPickUpOTPResponse.getStatus();
                        String message = verifyPickUpOTPResponse.getMessage();
                        if (status) {
                            Context context = UserData.getInstance().getSelectedOrderDetailsContext();
                            if (context != null) {
                                CloseCallBack closeCallBack = (CloseCallBack) context;
                                closeCallBack.close();
                            }
                            finish();
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }
}
