package com.tradebeaseller.interfaces;

public interface SelectedMenuDetailsCallBack {
    void selectedMenuDetails(String selectedMenuId, String selectedMenuName);
}
