package com.tradebeaseller.ApiCalls;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeaseller.fragments.OrdersFragment;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.jsonBuilderParser.JsonBuilderParser;
import com.tradebeaseller.models.requestModels.orderDetailsRequest.OrderDetailsRequest;
import com.tradebeaseller.utils.CommonMethods;
import com.tradebeaseller.utils.Constants;

import org.json.JSONObject;

public class OrderDetailsApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallForOrderDetails(Context context, Fragment fragment, RecyclerView.Adapter adapter, String startDate, String endDate, String lastDays, String lastYears, String token) {
        String url = Constants.ORDER_DETAILS_URL;
        OrderDetailsRequest orderDetailsRequest = new OrderDetailsRequest();
        orderDetailsRequest.startDate = startDate;
        orderDetailsRequest.endDate = endDate;
        orderDetailsRequest.lastDays = lastDays;
        orderDetailsRequest.lastYears = lastYears;
        orderDetailsRequest.status = 0;

        JSONObject postObject = JsonBuilderParser.jsonBuilder(orderDetailsRequest);
        AndroidNetworking.post(url)
                .addJSONObjectBody(postObject)
                .setPriority(Priority.HIGH)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("X_AUTH_TOKEN", token/*"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjMiLCJ1c2VyZGV0YWlsIjp7ImlkIjoiMyIsImlwX2FkZHJlc3MiOiIxNTcuNDYuMjMzLjIwMSIsInVzZXJuYW1lIjoiNzAxMzkwNzI5MSIsIndhbGxldCI6IjAuMDAiLCJmbG9hdGluZ193YWxsZXQiOiIwLjAwIiwidW5pcXVlX2lkIjoiVEJTMDAwMiIsInBhc3N3b3JkIjoiJDJ5JDEwJGJJSG9aQnpLdWtZN0t6d3BWTmZ4NE9yQUdFUVBrTFR6U1pFNVEzNGZJSDNBSmRyaWY2d2NHIiwic2FsdCI6bnVsbCwiZW1haWwiOiJhcHRpY2tzaXRAZ21haWwuY29tIiwiYWN0aXZhdGlvbl9jb2RlIjpudWxsLCJmb3Jnb3R0ZW5fcGFzc3dvcmRfY29kZSI6bnVsbCwiZm9yZ290dGVuX3Bhc3N3b3JkX3RpbWUiOm51bGwsInJlbWVtYmVyX2NvZGUiOm51bGwsInBhc3Njb2RlIjpudWxsLCJyZW1lbWJlcl9zZWxlY3RvciI6IiIsImNyZWF0ZWRfb24iOiIxNjA2MzYyMTkxIiwibGFzdF9sb2dpbiI6IjE2MjQ1NDgyMzUiLCJhY3RpdmUiOiIxIiwibGlzdF9pZCI6IjAiLCJmaXJzdF9uYW1lIjoic2VsbGVyIiwibGFzdF9uYW1lIjpudWxsLCJzdG9yZSI6bnVsbCwicGhvbmUiOiI3MDEzOTA3MjkxIiwiY3JlYXRlZF91c2VyX2lkIjpudWxsLCJ1cGRhdGVkX3VzZXJfaWQiOm51bGwsImNyZWF0ZWRfYXQiOiIyMDIwLTExLTI2IDAzOjQzOjExIiwidXBkYXRlZF9hdCI6IjIwMjEtMDUtMDQgMTM6MDE6NDAiLCJkZWxldGVkX2F0IjpudWxsLCJsYXN0X2RlbGl2ZXJ5X3BhcnRuZXJfc2VzaW9uX2lkIjpudWxsLCJkZWxpdmVyeV9wYXJ0bmVyX3N0YXR1cyI6bnVsbCwiYWN0aXZhdGlvbl9zZWxlY3RvciI6bnVsbCwiZm9yZ290dGVuX3Bhc3N3b3JkX3NlbGVjdG9yIjpudWxsLCJzdGF0dXMiOiIxIiwiZ3JvdXBzIjp7IjIiOnsiaWQiOiIyIiwidXNlcl9pZCI6IjMiLCJuYW1lIjoic2VsbGVyIn0sIjUiOnsiaWQiOiI1IiwidXNlcl9pZCI6IjMiLCJuYW1lIjoidXNlciJ9fX0sInRpbWUiOjE2MjQ1NDgzODJ9.bpxGSzoTtxhpCYV1nbJJMwkMKQxLOAA_n3AJ1fezwTA"*/)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_GET_ORDER_DETAILS);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_GET_ORDER_DETAILS);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_GET_ORDER_DETAILS);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_ORDER_DETAILS);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_ORDER_DETAILS);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_ORDER_DETAILS);
                        }
                    }
                });
    }
}
