package com.tradebeaseller.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tradebeaseller.ApiCalls.AcceptReturnOrderApiCall;
import com.tradebeaseller.ApiCalls.SelectedOrderDetailsApiCall;
import com.tradebeaseller.R;
import com.tradebeaseller.adapters.SelectedOrderDetailsAdapter;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.models.requestModels.returnAcceptOrderRequest.ReturnAcceptOrderRequest;
import com.tradebeaseller.models.responseModels.acceptReturnOrderResponse.AcceptReturnOrderResponse;
import com.tradebeaseller.models.responseModels.returnOrderDetailsResponse.ReturnReasonId;
import com.tradebeaseller.models.responseModels.selectedOrderDetailsResponse.DeliveryJobs;
import com.tradebeaseller.models.responseModels.selectedOrderDetailsResponse.OrderDetails;
import com.tradebeaseller.models.responseModels.selectedOrderDetailsResponse.ReturnOrderStatus;
import com.tradebeaseller.models.responseModels.selectedOrderDetailsResponse.ReturnRequest;
import com.tradebeaseller.models.responseModels.selectedOrderDetailsResponse.SelectedOrderData;
import com.tradebeaseller.models.responseModels.selectedOrderDetailsResponse.SelectedOrderDetailsResponse;
import com.tradebeaseller.utils.Constants;
import com.tradebeaseller.utils.PreferenceConnector;

import java.util.LinkedList;

public class SelectedReturnOrderDetailsActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack {

    private ImageView ivBackArrow;
    private RecyclerView rvOrderDetails;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView tvJobId, tvReturnReason, tvStatus;
    private TextView tvTotalItems, tvTotalPrice, tvSubTotal, tvTax, tvPromoDiscount, tvDiscount, tvGrandTotal, tvError, tvAccept;

    private SelectedOrderData selectedOrderData;
    private LinkedList<DeliveryJobs> listOfDeliveryJobs;
    private LinkedList<OrderDetails> listOfOrderDetails;

    private String token = "";
    private String jobId = "";
    private String orderId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_selected_return_order_details);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();
        refreshContent();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.order_id))) {
                orderId = bundle.getString(getString(R.string.order_id));
            }
        }
    }

    private void initializeUi() {
        tvError = findViewById(R.id.tvError);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        rvOrderDetails = findViewById(R.id.rvOrderDetails);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);

        tvTax = findViewById(R.id.tvTax);
        tvSubTotal = findViewById(R.id.tvSubTotal);
        tvDiscount = findViewById(R.id.tvDiscount);
        tvTotalItems = findViewById(R.id.tvTotalItems);
        tvTotalPrice = findViewById(R.id.tvTotalPrice);
        tvGrandTotal = findViewById(R.id.tvGrandTotal);
        tvPromoDiscount = findViewById(R.id.tvPromoDiscount);

        tvAccept = findViewById(R.id.tvAccept);

        tvJobId = findViewById(R.id.tvJobId);
        tvStatus = findViewById(R.id.tvStatus);
        tvReturnReason = findViewById(R.id.tvReturnReason);
    }

    private void initializeListeners() {
        tvAccept.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareDetails() {
        showProgressBar(this);
        token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
        SelectedOrderDetailsApiCall.serviceCallForSelectedOrderDetails(this, null, null, orderId, token);
    }

    private void refreshContent() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                prepareDetails();
            }
        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivBackArrow) {
            onBackPressed();
        } else if (id == R.id.tvAccept) {
            prepareAcceptDetails();
        }
    }

    private void prepareAcceptDetails() {
        showProgressBar(this);
        token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
        ReturnRequest returnRequest = selectedOrderData.getReturnRequest();
        int returnRequestId = returnRequest.getId();

        ReturnAcceptOrderRequest returnAcceptOrderRequest = new ReturnAcceptOrderRequest();
        returnAcceptOrderRequest.setReturnRequestId(returnRequestId);
        returnAcceptOrderRequest.setStatus(2);
        AcceptReturnOrderApiCall.serviceCallForAcceptReturnOrder(this, token, returnAcceptOrderRequest);
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_TO_GET_SELECTED_ORDER_DETAILS) {
            if (jsonResponse != null) {
                SelectedOrderDetailsResponse selectedOrderDetailsResponse = new Gson().fromJson(jsonResponse, SelectedOrderDetailsResponse.class);
                if (selectedOrderDetailsResponse != null) {
                    boolean status = selectedOrderDetailsResponse.getStatus();
                    if (status) {
                        selectedOrderData = selectedOrderDetailsResponse.getSelectedOrderData();
                        if (selectedOrderData != null) {
                            listOfOrderDetails = selectedOrderData.getListOfOrderDetails();
                            if (listOfOrderDetails != null) {
                                if (listOfOrderDetails.size() != 0) {
                                    prepareDeliveryJobId();
                                    prepareReturnOrderDetails();
                                    listIsFull();
                                    preparePriceDetails();
                                    initializeAdapter();
                                } else {
                                    listIsEmpty();
                                }
                            } else {
                                listIsEmpty();
                            }
                        }
                    }
                }
            }
            closeProgressbar();
        } else if (requestType == Constants.SERVICE_CALL_TO_ACCEPT_RETURN_ORDER) {
            if (jsonResponse != null) {
                AcceptReturnOrderResponse acceptReturnOrderResponse = new Gson().fromJson(jsonResponse, AcceptReturnOrderResponse.class);
                if (acceptReturnOrderResponse != null) {
                    boolean status = acceptReturnOrderResponse.getStatus();
                    if (status) {
                        tvAccept.setVisibility(View.GONE);
                        Toast.makeText(this, getString(R.string.return_order_accepted), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, getString(R.string.status_error), Toast.LENGTH_SHORT).show();
                    }
                }
            }
            closeProgressbar();
        }
    }

    @SuppressLint("SetTextI18n")
    private void prepareDeliveryJobId() {
        listOfDeliveryJobs = selectedOrderData.getListOfDeliveryJobs();
        if (listOfDeliveryJobs != null) {
            if (listOfDeliveryJobs.size() != 0) {
                if (listOfDeliveryJobs.size() == 2) {
                    DeliveryJobs deliveryJobs = listOfDeliveryJobs.get(1);
                    jobId = deliveryJobs.getJobId();

                    tvJobId.setText("Job Id" + "  :  " + jobId);
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private void prepareReturnOrderDetails() {
        ReturnRequest returnRequest = selectedOrderData.getReturnRequest();
        if (returnRequest != null) {
            ReturnReasonId returnReasonId = returnRequest.getReturnReasonId();
            if (returnReasonId != null) {
                String returnReason = returnReasonId.getReason();
                tvReturnReason.setText("Return Reason" + "  :  " + returnReason);
            }

            ReturnOrderStatus returnOrderStatus = returnRequest.getReturnOrderStatus();
            if (returnOrderStatus != null) {
                String statusName = returnOrderStatus.getStatusName();
                tvStatus.setText("Return Status" + "  :  " + statusName);
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private void preparePriceDetails() {
        int promoDiscount = 0;
        int totalPrice = 0;
        int subTotalPrice = 0;
        int discount = 0;
        int tax = 0;
        OrderDetails orderDetails = listOfOrderDetails.get(0);
        if (orderDetails.getTotal() != null)
            totalPrice = orderDetails.getTotal();
        if (orderDetails.getSubTotal() != null)
            subTotalPrice = orderDetails.getSubTotal();
        if (orderDetails.getDiscount() != null)
            discount = orderDetails.getDiscount();
        if (orderDetails.getPromoDiscount() != null)
            promoDiscount = orderDetails.getPromoDiscount();
        if (orderDetails.getTax() != null)
            tax = orderDetails.getTax();
        tvTotalItems.setText(getString(R.string.total_items) + "  " + " - " + "  " + listOfOrderDetails.size() + " " + "Items");

        tvPromoDiscount.setText("(-)" + getString(R.string.current_currency) + " " + promoDiscount + "/-");
        tvDiscount.setText("(-)" + getString(R.string.current_currency) + " " + discount + "/-");
        tvSubTotal.setText(getString(R.string.current_currency) + " " + subTotalPrice + "/-");
        tvTax.setText(getString(R.string.current_currency) + " " + tax + "/-");
        tvGrandTotal.setText(getString(R.string.current_currency) + " " + totalPrice + "/-");
    }

    private void initializeAdapter() {
        SelectedOrderDetailsAdapter selectedOrderDetailsAdapter = new SelectedOrderDetailsAdapter(this, listOfOrderDetails);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvOrderDetails.setLayoutManager(layoutManager);
        rvOrderDetails.setItemAnimator(new DefaultItemAnimator());
        rvOrderDetails.setAdapter(selectedOrderDetailsAdapter);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvOrderDetails.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvOrderDetails.setVisibility(View.GONE);
    }
}