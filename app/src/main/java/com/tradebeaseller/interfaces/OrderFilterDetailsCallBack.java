package com.tradebeaseller.interfaces;

public interface OrderFilterDetailsCallBack {
    void orderFilterDetails(String searchedText, String startDate, String endDate);
}
