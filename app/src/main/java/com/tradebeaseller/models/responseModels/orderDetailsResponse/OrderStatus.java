package com.tradebeaseller.models.responseModels.orderDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;
import java.util.List;

public class OrderStatus {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("current_status")
    @Expose
    private String currentStatus;
    @SerializedName("all_statuses_list")
    @Expose
    private LinkedList<String> listOfAllStatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public LinkedList<String> getListOfAllStatus() {
        return listOfAllStatus;
    }

    public void setListOfAllStatus(LinkedList<String> listOfAllStatus) {
        this.listOfAllStatus = listOfAllStatus;
    }
}
