package com.tradebeaseller.models.responseModels.displayProductDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class Data {

    @SerializedName("result")
    @Expose
    private LinkedList<ProductDetails> listOfProductDetails;
    @SerializedName("total_products_count")
    @Expose
    private Integer totalProductsCount;
    @SerializedName("products_per_page")
    @Expose
    private Integer productsPerPage;
    @SerializedName("total number of pages")
    @Expose
    private Integer totalNumberOfPages;
    @SerializedName("current_page")
    @Expose
    private Integer currentPage;


    public LinkedList<ProductDetails> getListOfProductDetails() {
        return listOfProductDetails;
    }

    public void setListOfProductDetails(LinkedList<ProductDetails> listOfProductDetails) {
        this.listOfProductDetails = listOfProductDetails;
    }

    public Integer getTotalProductsCount() {
        return totalProductsCount;
    }

    public void setTotalProductsCount(Integer totalProductsCount) {
        this.totalProductsCount = totalProductsCount;
    }

    public Integer getProductsPerPage() {
        return productsPerPage;
    }

    public void setProductsPerPage(Integer productsPerPage) {
        this.productsPerPage = productsPerPage;
    }

    public Integer getTotalNumberOfPages() {
        return totalNumberOfPages;
    }

    public void setTotalNumberOfPages(Integer totalNumberOfPages) {
        this.totalNumberOfPages = totalNumberOfPages;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }
}
