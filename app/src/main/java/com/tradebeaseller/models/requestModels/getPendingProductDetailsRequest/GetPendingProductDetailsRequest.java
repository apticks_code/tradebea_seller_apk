package com.tradebeaseller.models.requestModels.getPendingProductDetailsRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetPendingProductDetailsRequest {

    @SerializedName("status")
    @Expose
    public int status;
    @SerializedName("page_no")
    @Expose
    public int pageNo;
    @SerializedName("q")
    @Expose
    public String q;
    @SerializedName("menu_id")
    @Expose
    public int menuId;
    @SerializedName("cat_id")
    @Expose
    public int catId;
    @SerializedName("sub_cat_id")
    @Expose
    public int subCatId;
    @SerializedName("sub_sub_cat_id")
    @Expose
    public String subSubCatId;
    @SerializedName("brand_id")
    @Expose
    public int brandId;
}
