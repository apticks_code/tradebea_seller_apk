package com.tradebeaseller.ApiCalls;

import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.jsonBuilderParser.JsonBuilderParser;
import com.tradebeaseller.models.requestModels.signUpRequest.SignUpRequest;
import com.tradebeaseller.utils.CommonMethods;
import com.tradebeaseller.utils.Constants;

import org.json.JSONObject;

public class SignUpApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallToSignUp(final Context context, String firstName, String lastName, String gender, String email, String mobileNumber, String password) {
        String url = Constants.SIGN_UP_URL;
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.firstName = firstName;
        signUpRequest.lastName = lastName;
        signUpRequest.gender = gender;
        signUpRequest.email = email;
        signUpRequest.mobile = mobileNumber;
        signUpRequest.password = password;
        signUpRequest.confirmPassword = password;
        signUpRequest.group = "2";
        JSONObject postObject = JsonBuilderParser.jsonBuilder(signUpRequest);

        AndroidNetworking.post(url)
                .addJSONObjectBody(postObject) // posting json
                .setPriority(Priority.HIGH)
                .addHeaders("APP_id", Constants.APP_ID)
                .addHeaders("Content-Type", "application/json")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_SIGN_UP);
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_SIGN_UP);
                    }
                });
    }
}
