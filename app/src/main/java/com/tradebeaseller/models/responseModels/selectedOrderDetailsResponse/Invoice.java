package com.tradebeaseller.models.responseModels.selectedOrderDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Invoice {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("invoice_number")
    @Expose
    private String invoiceNumber;
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("payment_method_id")
    @Expose
    private Integer paymentMethodId;
    @SerializedName("payment_id")
    @Expose
    private Integer paymentId;
    @SerializedName("payment_order_id")
    @Expose
    private Integer paymentOrderId;
    @SerializedName("is_wallet_used_to_pay")
    @Expose
    private Integer isWalletUsedToPay;
    @SerializedName("created_user_id")
    @Expose
    private Integer createdUserId;
    @SerializedName("seller_user_id")
    @Expose
    private Integer sellerUserId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private Object deletedAt;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("order")
    @Expose
    private InvoiceOrder order;
    @SerializedName("payment_method")
    @Expose
    private InvoicePaymentMethod paymentMethod;
    @SerializedName("payment")
    @Expose
    private InvoicePayment payment;
    @SerializedName("payment_order")
    @Expose
    private InvoicePaymentOrder paymentOrder;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Integer paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    public Integer getPaymentOrderId() {
        return paymentOrderId;
    }

    public void setPaymentOrderId(Integer paymentOrderId) {
        this.paymentOrderId = paymentOrderId;
    }

    public Integer getIsWalletUsedToPay() {
        return isWalletUsedToPay;
    }

    public void setIsWalletUsedToPay(Integer isWalletUsedToPay) {
        this.isWalletUsedToPay = isWalletUsedToPay;
    }

    public Integer getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(Integer createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Integer getSellerUserId() {
        return sellerUserId;
    }

    public void setSellerUserId(Integer sellerUserId) {
        this.sellerUserId = sellerUserId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public InvoiceOrder getOrder() {
        return order;
    }

    public void setOrder(InvoiceOrder order) {
        this.order = order;
    }

    public InvoicePaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(InvoicePaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public InvoicePayment getPayment() {
        return payment;
    }

    public void setPayment(InvoicePayment payment) {
        this.payment = payment;
    }

    public InvoicePaymentOrder getPaymentOrder() {
        return paymentOrder;
    }

    public void setPaymentOrder(InvoicePaymentOrder paymentOrder) {
        this.paymentOrder = paymentOrder;
    }
}
