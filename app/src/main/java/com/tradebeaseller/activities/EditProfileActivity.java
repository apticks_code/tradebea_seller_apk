package com.tradebeaseller.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.tradebeaseller.ApiCalls.UpdateProfileApiCall;
import com.tradebeaseller.ApiCalls.GetProfileDetailsApiCall;
import com.tradebeaseller.R;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.interfaces.LocationUpdateCallBack;
import com.tradebeaseller.models.responseModels.profileResponse.GetProfileData;
import com.tradebeaseller.models.responseModels.profileResponse.LocationDetails;
import com.tradebeaseller.models.responseModels.profileResponse.ProfileResponse;
import com.tradebeaseller.models.responseModels.profileResponse.UserDetails;
import com.tradebeaseller.models.responseModels.profileUpdateResponse.ProfileUpdateResponse;
import com.tradebeaseller.utils.AppPermissions;
import com.tradebeaseller.utils.Constants;
import com.tradebeaseller.utils.GetCurrentLocation;
import com.tradebeaseller.utils.PreferenceConnector;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

public class EditProfileActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack, LocationUpdateCallBack, RadioGroup.OnCheckedChangeListener {

    private LatLng latLng;
    private RadioGroup radioGroup;
    private RadioButton rbProprietor, rbPartnership;
    private BottomSheetDialog pickerBottomSheetDialog;
    private EditText etShopName, etDesignation, etLocation, etPan, etEmailId, etMobile, etGST;
    private LinearLayout llSignatureImage, llShopImage, llElectricityBillImage, llPanImage, llSaveChanges;
    private TextView tvName, tvSignatureImage, tvShopImage, tvElectricityBillImage, tvPanImage, tvSaveChanges;
    private ImageView ivBackArrow, ivProfilePic, ivCurrentLocation, ivSignatureImage, ivShopImage, ivElectricityBillImage, ivPanImage;

    private ArrayList<String> listOfPhotoPaths;

    private String token = "";
    private String latitude = "";
    private String longitude = "";
    private String selectedImageName = "";
    private String selectedPanImageUrl = "";
    private String selectedShopImageUrl = "";
    private String selectedOwnerShipType = "";
    private String selectedProfileImageUrl = "";
    private String selectedSignatureImageUrl = "";
    private String selectedElectricityBillImageUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_edit_profile);
        initializeUi();
        initializeListeners();
        prepareProfileDetails();
    }

    private void initializeUi() {
        etGST = findViewById(R.id.etGST);
        etPan = findViewById(R.id.etPan);
        tvName = findViewById(R.id.tvName);
        etMobile = findViewById(R.id.etMobile);
        etEmailId = findViewById(R.id.etEmailId);
        tvPanImage = findViewById(R.id.tvPanImage);
        radioGroup = findViewById(R.id.radioGroup);
        etShopName = findViewById(R.id.etShopName);
        etLocation = findViewById(R.id.etLocation);
        llPanImage = findViewById(R.id.llPanImage);
        ivPanImage = findViewById(R.id.ivPanImage);
        ivShopImage = findViewById(R.id.ivShopImage);
        tvShopImage = findViewById(R.id.tvShopImage);
        llShopImage = findViewById(R.id.llShopImage);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        ivProfilePic = findViewById(R.id.ivProfilePic);
        rbProprietor = findViewById(R.id.rbProprietor);
        rbPartnership = findViewById(R.id.rbPartnership);
        etDesignation = findViewById(R.id.etDesignation);
        llSaveChanges = findViewById(R.id.llSaveChanges);
        tvSaveChanges = findViewById(R.id.tvSaveChanges);
        ivSignatureImage = findViewById(R.id.ivSignatureImage);
        tvSignatureImage = findViewById(R.id.tvSignatureImage);
        llSignatureImage = findViewById(R.id.llSignatureImage);
        ivCurrentLocation = findViewById(R.id.ivCurrentLocation);
        tvElectricityBillImage = findViewById(R.id.tvElectricityBillImage);
        llElectricityBillImage = findViewById(R.id.llElectricityBillImage);
        ivElectricityBillImage = findViewById(R.id.ivElectricityBillImage);

        listOfPhotoPaths = new ArrayList<>();
    }

    private void initializeListeners() {
        tvPanImage.setOnClickListener(this);
        ivPanImage.setOnClickListener(this);
        llPanImage.setOnClickListener(this);
        tvShopImage.setOnClickListener(this);
        ivShopImage.setOnClickListener(this);
        llShopImage.setOnClickListener(this);
        ivProfilePic.setOnClickListener(this);
        tvSignatureImage.setOnClickListener(this);
        ivSignatureImage.setOnClickListener(this);
        llSignatureImage.setOnClickListener(this);
        ivCurrentLocation.setOnClickListener(this);
        radioGroup.setOnCheckedChangeListener(this);
        tvElectricityBillImage.setOnClickListener(this);
        ivElectricityBillImage.setOnClickListener(this);
        llElectricityBillImage.setOnClickListener(this);

        ivBackArrow.setOnClickListener(this);
        llSaveChanges.setOnClickListener(this);
        tvSaveChanges.setOnClickListener(this);
    }

    private void prepareProfileDetails() {
        showProgressBar(this);
        token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
        GetProfileDetailsApiCall.serviceCallToGetProfileDetails(this, null, null, token);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            case R.id.ivCurrentLocation:
                checkLocationAccessPermission();
                break;
            case R.id.llSaveChanges:
            case R.id.tvSaveChanges:
                prepareSaveChangeDetails();
                break;
            case R.id.tvPanImage:
            case R.id.ivPanImage:
            case R.id.llPanImage:
                selectedImageName = getString(R.string.pan_image);
                prepareImageDetails();
                break;
            case R.id.tvShopImage:
            case R.id.ivShopImage:
            case R.id.llShopImage:
                selectedImageName = getString(R.string.shop_image);
                prepareImageDetails();
                break;
            case R.id.tvSignatureImage:
            case R.id.ivSignatureImage:
            case R.id.llSignatureImage:
                selectedImageName = getString(R.string.signature_image);
                prepareImageDetails();
                break;
            case R.id.tvElectricityBillImage:
            case R.id.ivElectricityBillImage:
            case R.id.llElectricityBillImage:
                selectedImageName = getString(R.string.electricity_bill_image);
                prepareImageDetails();
                break;
            case R.id.ivProfilePic:
                selectedImageName = getString(R.string.profile_pic);
                prepareImageDetails();
                break;
            case R.id.tvImagePicker:
                listOfPhotoPaths = new ArrayList<>();
                onPickPhoto();
                closeBottomSheetView();
                break;
            case R.id.tvCamera:
                listOfPhotoPaths = new ArrayList<>();
                captureImage();
                closeBottomSheetView();
                break;
            case R.id.tvCancel:
                closeBottomSheetView();
                break;
            default:
                break;
        }
    }

    private void prepareSaveChangeDetails() {
        showProgressBar(this);
        String shopName = etShopName.getText().toString();
        String designation = etDesignation.getText().toString();
        String location = etLocation.getText().toString();
        String pan = etPan.getText().toString();
        String gst = etGST.getText().toString();
        String emailId = etEmailId.getText().toString();
        String mobile = etMobile.getText().toString();
        String signatureImageBase64 = base64Converter(ivSignatureImage);
        String shopImageBase64 = base64Converter(ivShopImage);
        String electricityBillImageBase64 = base64Converter(ivElectricityBillImage);
        String panImageBase64 = base64Converter(ivPanImage);
        String profileImageBase64 = base64Converter(ivProfilePic);

        if (!selectedOwnerShipType.isEmpty()) {
            if (!shopName.isEmpty()) {
                if (!designation.isEmpty()) {
                    if (!location.isEmpty()) {
                        if (!pan.isEmpty()) {
                            if (!gst.isEmpty()) {
                                if (!emailId.isEmpty()) {
                                    if (!mobile.isEmpty()) {
                                        if (signatureImageBase64 != null) {
                                            if (shopImageBase64 != null) {
                                                if (electricityBillImageBase64 != null) {
                                                    if (panImageBase64 != null) {
                                                        if (profileImageBase64 != null) {
                                                            showProgressBar(EditProfileActivity.this);
                                                            UpdateProfileApiCall.serviceCallToUpdateProfile(this, tvName.getText().toString(), shopName, pan, gst, designation, selectedOwnerShipType, emailId, mobile, profileImageBase64, panImageBase64, shopImageBase64, signatureImageBase64, electricityBillImageBase64, token, location, latitude, longitude);
                                                        } else {
                                                            closeProgressbar();
                                                            Toast.makeText(this, getString(R.string.please_enter_profile_image), Toast.LENGTH_SHORT).show();
                                                        }
                                                    } else {
                                                        closeProgressbar();
                                                        Toast.makeText(this, getString(R.string.please_enter_pan_image), Toast.LENGTH_SHORT).show();
                                                    }
                                                } else {
                                                    closeProgressbar();
                                                    Toast.makeText(this, getString(R.string.please_select_electricity_bill_image), Toast.LENGTH_SHORT).show();
                                                }
                                            } else {
                                                closeProgressbar();
                                                Toast.makeText(this, getString(R.string.please_select_shop_image), Toast.LENGTH_SHORT).show();
                                            }
                                        } else {
                                            closeProgressbar();
                                            Toast.makeText(this, getString(R.string.please_select_signature), Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        closeProgressbar();
                                        Toast.makeText(this, getString(R.string.please_enter_mobile_number), Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    closeProgressbar();
                                    Toast.makeText(this, getString(R.string.please_enter_email_id), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                closeProgressbar();
                                Toast.makeText(this, getString(R.string.please_enter_gst), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            closeProgressbar();
                            Toast.makeText(this, getString(R.string.please_enter_pan), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        closeProgressbar();
                        Toast.makeText(this, getString(R.string.please_enter_location), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    closeProgressbar();
                    Toast.makeText(this, getString(R.string.please_enter_designation), Toast.LENGTH_SHORT).show();
                }
            } else {
                closeProgressbar();
                Toast.makeText(this, getString(R.string.please_enter_shop_name), Toast.LENGTH_SHORT).show();
            }
        } else {
            closeProgressbar();
            Toast.makeText(this, getString(R.string.please_select_owener_type), Toast.LENGTH_SHORT).show();
        }
    }

    private String base64Converter(ImageView imageView) {
        imageView.buildDrawingCache();
        Bitmap bitmap = imageView.getDrawingCache();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
        byte[] image = stream.toByteArray();
        return Base64.encodeToString(image, 0);
    }

    private void checkLocationAccessPermission() {
        if (AppPermissions.checkPermissionForAccessLocation(this)) {
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            new GetCurrentLocation(this, null, locationManager);
        } else {
            AppPermissions.requestPermissionForLocation(this);
        }
    }

    private void prepareImageDetails() {
        if (AppPermissions.checkPermissionForAccessExternalStorage(this)) {
            if (AppPermissions.checkPermissionForCamera(this)) {
                showBottomSheetView();
            } else {
                AppPermissions.requestPermissionForCamera(this);
            }
        } else {
            AppPermissions.requestPermissionForAccessExternalStorage(this);
        }
    }

    @SuppressLint("InflateParams")
    private void showBottomSheetView() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.layout_image_picker_sheet, null);

        TextView tvCancel = view.findViewById(R.id.tvCancel);
        TextView tvCamera = view.findViewById(R.id.tvCamera);
        TextView tvImagePicker = view.findViewById(R.id.tvImagePicker);

        tvCancel.setOnClickListener(this);
        tvCamera.setOnClickListener(this);
        tvImagePicker.setOnClickListener(this);

        pickerBottomSheetDialog = new BottomSheetDialog(this, R.style.BottomSheetDialog);
        pickerBottomSheetDialog.setContentView(view);
        pickerBottomSheetDialog.setCanceledOnTouchOutside(true);
        pickerBottomSheetDialog.show();
    }

    private void closeBottomSheetView() {
        if (pickerBottomSheetDialog != null) {
            pickerBottomSheetDialog.cancel();
        }
    }

    private void onPickPhoto() {
        FilePickerBuilder.getInstance()
                .setMaxCount(1)
                .setSelectedFiles(new ArrayList<>())
                .setActivityTheme(R.style.FilePickerTheme)
                .setActivityTitle(getString(R.string.select_image))
                .enableVideoPicker(false)
                .enableCameraSupport(true)
                .showGifs(true)
                .showFolderView(true)
                .enableSelectAll(false)
                .enableImagePicker(true)
                .setCameraPlaceholder(R.drawable.image_placeholder)
                .withOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                .pickPhoto(this, Constants.PICK_GALLERY);
    }

    private void captureImage() {
        ImagePicker.cameraOnly().start(this, Constants.REQUEST_CODE_CAPTURE_IMAGE);
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_PROFILE_DETAILS:
                if (jsonResponse != null) {
                    ProfileResponse profileResponse = new Gson().fromJson(jsonResponse, ProfileResponse.class);
                    if (profileResponse != null) {
                        boolean status = profileResponse.getStatus();
                        String message = profileResponse.getMessage();
                        if (status) {
                            GetProfileData getProfileData = profileResponse.getGetProfileData();
                            if (getProfileData != null) {
                                UserDetails userDetails = getProfileData.getUserDetails();
                                if (userDetails != null) {
                                    prepareUserDetails(userDetails);
                                }
                            }
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_FOR_PROFILE_UPDATE:
                if (jsonResponse != null) {
                    ProfileUpdateResponse profileUpdateResponse = new Gson().fromJson(jsonResponse, ProfileUpdateResponse.class);
                    if (profileUpdateResponse != null) {
                        boolean status = profileUpdateResponse.getStatus();
                        String message = profileUpdateResponse.getMessage();
                        if (status) {
                            finish();
                        }
                        Toast.makeText(this, Html.fromHtml(message), Toast.LENGTH_SHORT).show();
                    }

                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    private void prepareUserDetails(UserDetails userDetails) {
        String profileImageUrl = userDetails.getProfileImage();
        String name = userDetails.getFirstName();
        String shopName = userDetails.getShopName();
        String designation = userDetails.getDesignation();
        String pan = userDetails.getPan();
        String gst = userDetails.getGst();
        String emailId = userDetails.getEmail();
        Long mobile = userDetails.getMobile();
        String signatureImageUrl = userDetails.getSignatureImage();
        String shopImageUrl = userDetails.getShopImage();
        String electricityBillImageUrl = userDetails.getElectricityBillImage();
        String panImageUrl = userDetails.getPanCardImage();
        selectedOwnerShipType = userDetails.getOwnershipType();

        LocationDetails locationDetails = userDetails.getLocationDetails();

        if (locationDetails != null) {
            String address = locationDetails.getGeoLocationAddress();
            if (address != null) {
                if (!address.isEmpty()) {
                    etLocation.setText(address);
                }
            }
        }

        if (name != null) {
            if (!name.isEmpty()) {
                tvName.setText(name);
            }
        }

        if (shopName != null) {
            if (!shopName.isEmpty()) {
                etShopName.setText(shopName);
            }
        }

        if (designation != null) {
            if (!designation.isEmpty()) {
                etDesignation.setText(designation);
            }
        }

        if (pan != null) {
            if (!pan.isEmpty()) {
                etPan.setText(pan);
            }
        }

        if (gst != null) {
            if (!gst.isEmpty()) {
                etGST.setText(gst);
            }
        }

        if (emailId != null) {
            if (!emailId.isEmpty()) {
                etEmailId.setText(emailId);
            }
        }

        if (mobile != null) {
            etMobile.setText(String.valueOf(mobile));
        }

        if (profileImageUrl != null) {
            if (!profileImageUrl.isEmpty()) {
                Glide.with(this)
                        .load(profileImageUrl)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .placeholder(R.drawable.ic_user_image)
                        .error(R.drawable.ic_user_image)
                        .into(ivProfilePic);
            }
        }

        if (signatureImageUrl != null) {
            if (!signatureImageUrl.isEmpty()) {
                Glide.with(this)
                        .load(signatureImageUrl)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .error(R.drawable.ic_default_place_holder)
                        .into(ivSignatureImage);
            }
        }

        if (shopImageUrl != null) {
            if (!shopImageUrl.isEmpty()) {
                Glide.with(this)
                        .load(shopImageUrl)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .error(R.drawable.ic_default_place_holder)
                        .into(ivShopImage);
            }
        }

        if (electricityBillImageUrl != null) {
            if (!electricityBillImageUrl.isEmpty()) {
                Glide.with(this)
                        .load(electricityBillImageUrl)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .error(R.drawable.ic_default_place_holder)
                        .into(ivElectricityBillImage);
            }
        }

        if (panImageUrl != null) {
            if (!panImageUrl.isEmpty()) {
                Glide.with(this)
                        .load(panImageUrl)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .error(R.drawable.ic_default_place_holder)
                        .into(ivPanImage);
            }
        }

        if (selectedOwnerShipType != null) {
            if (selectedOwnerShipType.equalsIgnoreCase(getString(R.string.proprietor))) {
                rbProprietor.setChecked(true);
            } else if (selectedOwnerShipType.equalsIgnoreCase(getString(R.string.partnership))) {
                rbPartnership.setChecked(true);
            }
        } else {
            selectedOwnerShipType = "";
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultIntent) {
        super.onActivityResult(requestCode, resultCode, resultIntent);
        switch (requestCode) {
            case Constants.PICK_GALLERY:
                listOfPhotoPaths = new ArrayList<>();
                if (resultIntent != null) {
                    if (resultCode == Activity.RESULT_OK) {
                        listOfPhotoPaths.addAll(resultIntent.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
                        if (listOfPhotoPaths != null) {
                            if (listOfPhotoPaths.size() != 0) {
                                String selectedImageUrl = listOfPhotoPaths.get(0);
                                if (selectedImageName.equalsIgnoreCase(getString(R.string.pan_image))) {
                                    selectedPanImageUrl = selectedImageUrl;
                                    Glide.with(this)
                                            .load(new File(selectedPanImageUrl))
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .skipMemoryCache(true)
                                            .placeholder(R.drawable.image_placeholder)
                                            .error(R.drawable.image_placeholder)
                                            .into(ivPanImage);
                                } else if (selectedImageName.equalsIgnoreCase(getString(R.string.electricity_bill_image))) {
                                    selectedElectricityBillImageUrl = selectedImageUrl;
                                    Glide.with(this)
                                            .load(new File(selectedElectricityBillImageUrl))
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .skipMemoryCache(true)
                                            .placeholder(R.drawable.image_placeholder)
                                            .error(R.drawable.image_placeholder)
                                            .into(ivElectricityBillImage);
                                } else if (selectedImageName.equalsIgnoreCase(getString(R.string.shop_image))) {
                                    selectedShopImageUrl = selectedImageUrl;
                                    Glide.with(this)
                                            .load(new File(selectedShopImageUrl))
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .skipMemoryCache(true)
                                            .placeholder(R.drawable.image_placeholder)
                                            .error(R.drawable.image_placeholder)
                                            .into(ivShopImage);
                                } else if (selectedImageName.equalsIgnoreCase(getString(R.string.signature_image))) {
                                    selectedSignatureImageUrl = selectedImageUrl;
                                    Glide.with(this)
                                            .load(new File(selectedSignatureImageUrl))
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .skipMemoryCache(true)
                                            .placeholder(R.drawable.image_placeholder)
                                            .error(R.drawable.image_placeholder)
                                            .into(ivSignatureImage);
                                } else if (selectedImageName.equalsIgnoreCase(getString(R.string.profile))) {
                                    selectedProfileImageUrl = selectedImageUrl;
                                    Glide.with(this)
                                            .load(new File(selectedProfileImageUrl))
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .skipMemoryCache(true)
                                            .placeholder(R.drawable.image_placeholder)
                                            .error(R.drawable.image_placeholder)
                                            .into(ivProfilePic);
                                }
                            }
                        }
                    }
                }
                break;
            case Constants.REQUEST_CODE_CAPTURE_IMAGE:
                listOfPhotoPaths = new ArrayList<>();
                List<Image> listOfImages = ImagePicker.getImages(resultIntent);
                if (listOfImages != null) {
                    if (listOfImages.size() != 0) {
                        String selectedImageUrl = listOfImages.get(0).getPath();
                        listOfPhotoPaths.add(selectedImageUrl);
                        if (selectedImageName.equalsIgnoreCase(getString(R.string.pan_image))) {
                            selectedPanImageUrl = selectedImageUrl;
                            Glide.with(this)
                                    .load(new File(selectedPanImageUrl))
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true)
                                    .placeholder(R.drawable.image_placeholder)
                                    .error(R.drawable.image_placeholder)
                                    .into(ivPanImage);
                        } else if (selectedImageName.equalsIgnoreCase(getString(R.string.electricity_bill_image))) {
                            selectedElectricityBillImageUrl = selectedImageUrl;
                            Glide.with(this)
                                    .load(new File(selectedElectricityBillImageUrl))
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true)
                                    .placeholder(R.drawable.image_placeholder)
                                    .error(R.drawable.image_placeholder)
                                    .into(ivElectricityBillImage);
                        } else if (selectedImageName.equalsIgnoreCase(getString(R.string.shop_image))) {
                            selectedShopImageUrl = selectedImageUrl;
                            Glide.with(this)
                                    .load(new File(selectedShopImageUrl))
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true)
                                    .placeholder(R.drawable.image_placeholder)
                                    .error(R.drawable.image_placeholder)
                                    .into(ivShopImage);
                        } else if (selectedImageName.equalsIgnoreCase(getString(R.string.signature_image))) {
                            selectedSignatureImageUrl = selectedImageUrl;
                            Glide.with(this)
                                    .load(new File(selectedSignatureImageUrl))
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true)
                                    .placeholder(R.drawable.image_placeholder)
                                    .error(R.drawable.ic_valid)
                                    .into(ivSignatureImage);
                        } else if (selectedImageName.equalsIgnoreCase(getString(R.string.profile))) {
                            selectedProfileImageUrl = selectedImageUrl;
                            Glide.with(this)
                                    .load(new File(selectedProfileImageUrl))
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true)
                                    .placeholder(R.drawable.image_placeholder)
                                    .error(R.drawable.image_placeholder)
                                    .into(ivProfilePic);
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (permissions.length != 0 && grantResults.length != 0) {
            switch (requestCode) {
                case Constants.REQUEST_CODE_FOR_CAMERA_PERMISSION:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        prepareImageDetails();
                    } else {
                        prepareImageDetails();
                    }
                    break;
                case Constants.REQUEST_CODE_FOR_EXTERNAL_STORAGE_PERMISSION:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        prepareImageDetails();
                    } else {
                        prepareImageDetails();
                    }
                    break;
                case Constants.REQUEST_CODE_FOR_LOCATION_PERMISSION:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        checkLocationAccessPermission();
                    } else {
                        checkLocationAccessPermission();
                    }
                default:
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    @Override
    public void OnLatLongReceived(double latitude, double longitude) {
        latLng = new LatLng(latitude, longitude);
        getCurrentLocation();
    }

    private void getCurrentLocation() {
        String errorMessage = "";
        latitude = String.valueOf(latLng.latitude);
        longitude = String.valueOf(latLng.longitude);
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);

        } catch (IOException ioException) {
            errorMessage = getString(R.string.service_not_available);
            Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
        } catch (IllegalArgumentException illegalArgumentException) {
            errorMessage = getString(R.string.invalid_lat_lng_used);
            Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
        }

        if (addresses == null || addresses.size() == 0) {
            if (errorMessage.isEmpty()) {
                errorMessage = getString(R.string.no_address_found);
                Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
            }
        } else {
            Address address = addresses.get(0);
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                builder.append(address.getAddressLine(i));
            }
            etLocation.setText(builder.toString());
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
        RadioButton radioButton = radioGroup.findViewById(checkedId);
        selectedOwnerShipType = radioButton.getText().toString();
    }
}
