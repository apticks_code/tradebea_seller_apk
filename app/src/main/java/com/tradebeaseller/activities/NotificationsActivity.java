package com.tradebeaseller.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.tradebeaseller.R;

public class NotificationsActivity extends BaseActivity implements View.OnClickListener {

    private TextView tvError;
    private ImageView ivBackArrow;
    private RecyclerView rvNotifications;
    private SwipeRefreshLayout swipeRefreshLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_notifications);
        initializeUi();
        initializeListeners();
        refreshContent();
        listIsEmpty();
    }

    private void initializeUi() {
        tvError = findViewById(R.id.tvError);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        rvNotifications = findViewById(R.id.rvNotifications);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }

    private void refreshContent() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                listIsEmpty();
            }
        });
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvNotifications.setVisibility(View.GONE);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvNotifications.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivBackArrow) {
            onBackPressed();
        }
    }
}
