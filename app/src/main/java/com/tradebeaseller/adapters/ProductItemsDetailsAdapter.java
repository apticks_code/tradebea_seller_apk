package com.tradebeaseller.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.tradebeaseller.R;
import com.tradebeaseller.interfaces.CloseCallBack;
import com.tradebeaseller.interfaces.SelectedItemsDetailsCallBack;
import com.tradebeaseller.models.responseModels.itemsDetailsResponse.ItemDetails;
import com.tradebeaseller.utils.UserData;

import java.util.Arrays;
import java.util.LinkedList;

public class ProductItemsDetailsAdapter extends RecyclerView.Adapter<ProductItemsDetailsAdapter.ViewHolder> implements View.OnClickListener {

    private Context context;
    private ColorGenerator generator;
    private TextDrawable.IBuilder mDrawableBuilder;

    private LinkedList<ItemDetails> listOfItemDetails;
    private LinkedList<String> listOfSelectedProductItemIds;
    private LinkedList<String> listOfSelectedProductItemNames;

    private int selectedPosition = -1;
    private int selectionType;

    public ProductItemsDetailsAdapter(Context context, LinkedList<ItemDetails> listOfItemDetails, String selectedProductItemIds, String selectedProductItemNames, TextView tvSave, int selectionType) {
        this.context = context;
        this.selectionType = selectionType;
        tvSave.setOnClickListener(this);
        generator = ColorGenerator.MATERIAL;
        this.listOfItemDetails = listOfItemDetails;
        mDrawableBuilder = TextDrawable.builder().round();
        if (!selectedProductItemIds.isEmpty()) {
            listOfSelectedProductItemIds = new LinkedList<>(Arrays.asList(selectedProductItemIds.split("\\s*,\\s*")));
            listOfSelectedProductItemNames = new LinkedList<>(Arrays.asList(selectedProductItemNames.split("\\s*,\\s*")));
        } else {
            listOfSelectedProductItemIds = new LinkedList<>();
            listOfSelectedProductItemNames = new LinkedList<>();
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_product_item_details_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ItemDetails itemDetails = listOfItemDetails.get(position);
        if (itemDetails != null) {
            String id = String.valueOf(itemDetails.getId());
            String name = itemDetails.getValue();
            int randomColor = generator.getRandomColor();
            TextDrawable drawable = mDrawableBuilder.build(String.valueOf(name.charAt(0)), randomColor);

            if(selectionType==1){// radio button selection
                holder.checkBox.setButtonDrawable(R.drawable.radio_button_drawable);
            }
            if(selectionType==2){//2-check box selection
                holder.checkBox.setButtonDrawable(R.drawable.check_box_drawable);
            }

            holder.tvProductOptionName.setText(name);
            holder.ivPic.setImageDrawable(drawable);

            if (listOfSelectedProductItemIds.contains(id)) {
                holder.checkBox.setChecked(true);
            } else {
                holder.checkBox.setChecked(false);
            }
        }
    }

    @Override
    public int getItemCount() {
        return listOfItemDetails.size();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.tvSave:
                prepareSaveDetails();
                break;
            default:
                break;
        }
    }

    private void prepareSaveDetails() {
        Context context = UserData.getInstance().getContext();
        if (context != null) {
            String selectedProductItemIds = "";
            String selectedProductItemNames = "";
            if (listOfSelectedProductItemIds.size() != 0) {
                selectedProductItemIds = TextUtils.join(",", listOfSelectedProductItemIds);
                selectedProductItemNames = TextUtils.join(",", listOfSelectedProductItemNames);
            }
            SelectedItemsDetailsCallBack selectedItemsDetailsCallBack = (SelectedItemsDetailsCallBack) context;
            selectedItemsDetailsCallBack.selectedItemsDetails(selectedProductItemIds, selectedProductItemNames);
        }

        CloseCallBack closeCallBack = (CloseCallBack) this.context;
        closeCallBack.close();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {

        private ImageView ivPic;
        private CheckBox checkBox;
        private TextView tvProductOptionName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivPic = itemView.findViewById(R.id.ivPic);
            checkBox = itemView.findViewById(R.id.checkBox);
            tvProductOptionName = itemView.findViewById(R.id.tvProductOptionName);
            checkBox.setOnCheckedChangeListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            String itemIds = String.valueOf(listOfItemDetails.get(getLayoutPosition()).getId());
            String itemNames = listOfItemDetails.get(getLayoutPosition()).getValue();
            if (isChecked) {
                if (!listOfSelectedProductItemIds.contains(itemIds)) {
                    listOfSelectedProductItemIds.add(itemIds);
                    listOfSelectedProductItemNames.add(itemNames);
                }
            } else {
                if (listOfSelectedProductItemIds.contains(itemIds)) {
                    int index = listOfSelectedProductItemIds.indexOf(itemIds);
                    if (index != -1) {
                        listOfSelectedProductItemIds.remove(index);
                        listOfSelectedProductItemNames.remove(index);
                    }
                }
            }
        }
    }
}
