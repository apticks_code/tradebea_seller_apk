package com.tradebeaseller.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.tradebeaseller.ApiCalls.ItemDetailsApiCall;
import com.tradebeaseller.R;
import com.tradebeaseller.adapters.ProductItemsDetailsAdapter;
import com.tradebeaseller.interfaces.CloseCallBack;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.models.responseModels.itemsDetailsResponse.ItemDetails;
import com.tradebeaseller.models.responseModels.itemsDetailsResponse.ItemsDetailsResponse;
import com.tradebeaseller.utils.Constants;

import java.util.LinkedList;

public class ProductItemsDetailsActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack, CloseCallBack {

    private ImageView ivBackArrow;
    private TextView tvSave, tvError;
    private RecyclerView rvProductItemDetails;

    private LinkedList<ItemDetails> listOfItemDetails;

    private String selectedCategoryId = "";
    private String selectedProductOptionId = "";
    private String selectedProductOptionName = "";
    private String selectedProductGroupId = "";
    private String selectedProductGroupName = "";
    private String selectedProductItemIds = "";
    private String selectedProductItemNames = "";
    private int selectionType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_product_item_details);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.selected_category_id)))
                selectedCategoryId = bundle.getString(getString(R.string.selected_category_id), "");
            if (bundle.containsKey(getString(R.string.selected_product_option_id)))
                selectedProductOptionId = bundle.getString(getString(R.string.selected_product_option_id), "");
            if (bundle.containsKey(getString(R.string.selected_product_option_name)))
                selectedProductOptionName = bundle.getString(getString(R.string.selected_product_option_name), "");
            if (bundle.containsKey(getString(R.string.selected_product_group_id)))
                selectedProductGroupId = bundle.getString(getString(R.string.selected_product_group_id), "");
            if (bundle.containsKey(getString(R.string.selected_product_group_name)))
                selectedProductGroupName = bundle.getString(getString(R.string.selected_product_group_name), "");
            if (bundle.containsKey(getString(R.string.selected_product_item_ids)))
                selectedProductItemIds = bundle.getString(getString(R.string.selected_product_item_ids), "");
            if (bundle.containsKey(getString(R.string.selected_product_item_names)))
                selectedProductItemNames = bundle.getString(getString(R.string.selected_product_item_names), "");
            if (bundle.containsKey("SelectionType"))
                selectionType = bundle.getInt("SelectionType", 0);
        }
    }

    private void initializeUi() {
        tvSave = findViewById(R.id.tvSave);
        tvError = findViewById(R.id.tvError);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        rvProductItemDetails = findViewById(R.id.rvProductItemDetails);
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareDetails() {
        showProgressBar(this);
        ItemDetailsApiCall.serviceCallToGetItemDetails(this, null, null, selectedProductGroupId);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_ITEMS_DETAILS:
                if (jsonResponse != null) {
                    try {
                        ItemsDetailsResponse itemsDetailsResponse = new Gson().fromJson(jsonResponse, ItemsDetailsResponse.class);
                        if (itemsDetailsResponse != null) {
                            boolean status = itemsDetailsResponse.getStatus();
                            String message = itemsDetailsResponse.getMessage();
                            if (status) {
                                listOfItemDetails = itemsDetailsResponse.getListOfItemDetails();
                                if (listOfItemDetails != null) {
                                    if (listOfItemDetails.size() != 0) {
                                        listIsFull();
                                        initializeAdapter();
                                    } else {
                                        listIsEmpty();
                                    }
                                } else {
                                    listIsEmpty();
                                }
                            } else {
                                listIsEmpty();
                                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception exception) {
                        listIsEmpty();
                        exception.printStackTrace();
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    private void initializeAdapter() {
        ProductItemsDetailsAdapter productItemsDetailsAdapter = new ProductItemsDetailsAdapter(this, listOfItemDetails, selectedProductItemIds, selectedProductItemNames, tvSave,selectionType);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvProductItemDetails.setLayoutManager(layoutManager);
        rvProductItemDetails.setItemAnimator(new DefaultItemAnimator());
        rvProductItemDetails.setAdapter(productItemsDetailsAdapter);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvProductItemDetails.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvProductItemDetails.setVisibility(View.GONE);
    }

    @Override
    public void close() {
        finish();
    }
}
