package com.tradebeaseller.models.requestModels.saveAddProductRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class SaveAddProductRequest {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("menu_id")
    @Expose
    private String menuId;
    @SerializedName("cat_id")
    @Expose
    private String catId;
    @SerializedName("sub_cat_id")
    @Expose
    private String subCatId;
    @SerializedName("sub_sub_cat_id")
    @Expose
    private String subSubCatId;
    @SerializedName("brand_id")
    @Expose
    private String brandId;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("variants")
    @Expose
    private LinkedList<VariantDetails> listOfVariantDetails;
    @SerializedName("product_images")
    @Expose
    private LinkedList<ProductImageDetails> listOfProductImages;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(String subCatId) {
        this.subCatId = subCatId;
    }

    public String getSubSubCatId() {
        return subSubCatId;
    }

    public void setSubSubCatId(String subSubCatId) {
        this.subSubCatId = subSubCatId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public LinkedList<VariantDetails> getListOfVariantDetails() {
        return listOfVariantDetails;
    }

    public void setListOfVariantDetails(LinkedList<VariantDetails> listOfVariantDetails) {
        this.listOfVariantDetails = listOfVariantDetails;
    }

    public LinkedList<ProductImageDetails> getListOfProductImages() {
        return listOfProductImages;
    }

    public void setListOfProductImages(LinkedList<ProductImageDetails> listOfProductImages) {
        this.listOfProductImages = listOfProductImages;
    }
}
