package com.tradebeaseller.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.tradebeaseller.ApiCalls.SellerProductDetailsApiCall;
import com.tradebeaseller.R;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.models.responseModels.editProductResponse.EditProductBrandDetails;
import com.tradebeaseller.models.responseModels.editProductResponse.EditProductCategoryDetails;
import com.tradebeaseller.models.responseModels.editProductResponse.EditProductData;
import com.tradebeaseller.models.responseModels.editProductResponse.EditProductMenuDetails;
import com.tradebeaseller.models.responseModels.editProductResponse.EditProductProductImage;
import com.tradebeaseller.models.responseModels.editProductResponse.EditProductResponse;
import com.tradebeaseller.models.responseModels.editProductResponse.EditProductSellerVariantDetails;
import com.tradebeaseller.models.responseModels.editProductResponse.EditProductSubCategoryDetails;
import com.tradebeaseller.models.responseModels.editProductResponse.VariantsValue;
import com.tradebeaseller.utils.Constants;
import com.tradebeaseller.utils.PreferenceConnector;
import com.tradebeaseller.utils.UserData;

import java.util.LinkedList;
import java.util.Timer;

public class EditProductSellerActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack {

    private ViewPager viewPager;
    private ImageView ivBackArrow;
    private LinearLayout llBanners, llDots, llGenerateVariantDetails;
    private TextView tvPrice, tvMenu, tvCategory, tvSubCategory, tvBrand, tvProductId, tvProductName;

    private LinkedList<EditProductProductImage> listOfEditProductProductImage;
    private LinkedList<EditProductSellerVariantDetails> listOfEditProductSellerVariantDetails;

    private String name = "";
    private String token = "";
    private String productId = "";

    private String selectedMenuId = "";
    private String selectedMenuName = "";

    private String selectedBrandId = "";
    private String selectedBrandName = "";

    private String selectedCategoryId = "";
    private String selectedCategoryName = "";

    private String selectedSubCategoryId = "";
    private String selectedSubCategoryName = "";

    private String selectedSubSubCategoryId = "";
    private String selectedSubSubCategoryName = "";

    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_edit_product_seller);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.product_id)))
                productId = bundle.getString(getString(R.string.product_id));
        }
    }

    private void initializeUi() {
        tvMenu = findViewById(R.id.tvMenu);
        llDots = findViewById(R.id.llDots);
        tvPrice = findViewById(R.id.tvPrice);
        tvBrand = findViewById(R.id.tvBrand);
        viewPager = findViewById(R.id.viewPager);
        llBanners = findViewById(R.id.llBanners);
        tvCategory = findViewById(R.id.tvCategory);
        tvProductId = findViewById(R.id.tvProductId);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        tvSubCategory = findViewById(R.id.tvSubCategory);
        tvProductName = findViewById(R.id.tvProductName);
        llGenerateVariantDetails = findViewById(R.id.llGenerateVariantDetails);
        PreferenceConnector.writeBoolean(this, getString(R.string.refresh_seller_product_details), false);
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareDetails() {
        showProgressBar(this);
        token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
        SellerProductDetailsApiCall.serviceCallToGetSellerProductDetails(this, null, null, token, productId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean refreshSellerProductDetails = PreferenceConnector.readBoolean(this, getString(R.string.refresh_seller_product_details), false);
        if (refreshSellerProductDetails) {
            if (llGenerateVariantDetails != null) {
                llGenerateVariantDetails.removeAllViewsInLayout();
            }
            prepareDetails();
            PreferenceConnector.writeBoolean(this, getString(R.string.refresh_seller_product_details), false);
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            case R.id.ivEdit:
                prepareEditVariant(view);
                break;
            default:
                break;
        }
    }

    private void prepareEditVariant(View view) {
        int position = (int) view.getTag();
        EditProductSellerVariantDetails editProductSellerVariantDetails = listOfEditProductSellerVariantDetails.get(position);
        UserData.getInstance().setEditProductSellerVariantDetails(editProductSellerVariantDetails);
        UserData.getInstance().setListOfEditProductProductImage(listOfEditProductProductImage);

        Intent editProductSellerVariantIntent = new Intent(this, EditProductSellerVariantActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(getString(R.string.product_id), productId);
        bundle.putString(getString(R.string.product_name), name);
        bundle.putString(getString(R.string.selected_menu_name), selectedMenuName);
        bundle.putString(getString(R.string.selected_category_name), selectedCategoryName);
        bundle.putString(getString(R.string.selected_sub_category_name), selectedSubCategoryName);
        bundle.putString(getString(R.string.selected_brand_name), selectedBrandName);
        editProductSellerVariantIntent.putExtras(bundle);
        startActivity(editProductSellerVariantIntent);
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_SELLER_PRODUCT_DETAILS:
                if (jsonResponse != null) {
                    EditProductResponse editProductResponse = new Gson().fromJson(jsonResponse, EditProductResponse.class);
                    if (editProductResponse != null) {
                        boolean status = editProductResponse.getStatus();
                        String message = editProductResponse.getMessage();
                        if (status) {
                            EditProductData editProductData = editProductResponse.getEditProductData();
                            if (editProductData != null) {
                                name = editProductData.getName();
                                String description = editProductData.getDesc();
                                EditProductMenuDetails editProductMenuDetails = editProductData.getEditProductMenuDetails();
                                EditProductCategoryDetails editProductCategoryDetails = editProductData.getEditProductCategoryDetails();
                                EditProductSubCategoryDetails editProductSubCategoryDetails = editProductData.getEditProductSubCategoryDetails();
                                EditProductBrandDetails editProductBrandDetails = editProductData.getEditProductBrandDetails();
                                listOfEditProductProductImage = editProductData.getListOfEditProductProductImage();
                                listOfEditProductSellerVariantDetails = editProductData.getListOfEditProductSellerVariantDetails();

                                tvProductName.setText(name);
                                tvProductId.setText(editProductData.getProductCode());
                                if (editProductMenuDetails != null) {
                                    selectedMenuId = String.valueOf(editProductMenuDetails.getId());
                                    selectedMenuName = editProductMenuDetails.getName();

                                    tvMenu.setText(selectedMenuName);
                                }

                                if (editProductCategoryDetails != null) {
                                    selectedCategoryId = String.valueOf(editProductCategoryDetails.getId());
                                    selectedCategoryName = editProductCategoryDetails.getName();

                                    tvCategory.setText(selectedCategoryName);
                                }

                                if (editProductSubCategoryDetails != null) {
                                    selectedSubCategoryId = String.valueOf(editProductSubCategoryDetails.getId());
                                    selectedSubCategoryName = editProductSubCategoryDetails.getName();

                                    tvSubCategory.setText(selectedSubCategoryName);
                                }

                                if (editProductBrandDetails != null) {
                                    selectedBrandId = String.valueOf(editProductBrandDetails.getId());
                                    selectedBrandName = editProductBrandDetails.getName();

                                    tvBrand.setText(selectedBrandName);
                                }


                                if (listOfEditProductSellerVariantDetails != null) {
                                    if (listOfEditProductSellerVariantDetails.size() != 0) {
                                        prepareEditProductVariantDetails();
                                    }
                                }

                                if (listOfEditProductProductImage != null) {
                                    if (listOfEditProductProductImage.size() != 0) {
                                        llBanners.setVisibility(View.VISIBLE);
                                        initializeViewPager();
                                    } else {
                                        listOfEditProductProductImage = new LinkedList<>();
                                        llBanners.setVisibility(View.GONE);
                                    }
                                } else {
                                    listOfEditProductProductImage = new LinkedList<>();
                                    llBanners.setVisibility(View.GONE);
                                }
                            }
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    private void prepareEditProductVariantDetails() {
        for (int index = 0; index < listOfEditProductSellerVariantDetails.size(); index++) {
            EditProductSellerVariantDetails editProductSellerVariantDetails = listOfEditProductSellerVariantDetails.get(index);
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            assert inflater != null;
            @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.layout_product_seller_generate_variant, null);
            TextView tvVariantName = view.findViewById(R.id.tvVariantName);
            ImageView ivEdit = view.findViewById(R.id.ivEdit);
            TextView tvVariantCode = view.findViewById(R.id.tvVariantCode);
            TextView tvMrp = view.findViewById(R.id.tvMrp);

            Integer variantCode = editProductSellerVariantDetails.getSku();
            int mrp = editProductSellerVariantDetails.getSellerMrp();

            LinkedList<VariantsValue> listOfVariantsValues = editProductSellerVariantDetails.getListOfVariantsValues();
            StringBuilder variantInfoName = new StringBuilder();

            for (int optionValueIndex = 0; optionValueIndex < listOfVariantsValues.size(); optionValueIndex++) {
                VariantsValue variantsValue = listOfVariantsValues.get(optionValueIndex);
                if (optionValueIndex == 0) {
                    variantInfoName.append(variantsValue.getOptionItem().getValue());
                } else {
                    variantInfoName.append(" | ").append(variantsValue.getOptionItem().getValue());
                }
            }

            tvVariantName.setText(variantInfoName);
            tvVariantCode.setText("Variant Code" + " : " + String.valueOf(variantCode));
            tvMrp.setText(String.valueOf(mrp));

            ivEdit.setTag(index);
            ivEdit.setOnClickListener(this);
            llGenerateVariantDetails.addView(view);
        }
    }

    private void initializeViewPager() {
        ProductBannersImagesAdapter productBannersImagesAdapter = new ProductBannersImagesAdapter(this, listOfEditProductProductImage);
        viewPager.setAdapter(productBannersImagesAdapter);
        for (int index = 0; index < productBannersImagesAdapter.getCount(); index++) {
            ImageButton ibDot = new ImageButton(this);
            ibDot.setTag(index);
            ibDot.setImageResource(R.drawable.dot_selector);
            ibDot.setBackgroundResource(0);
            ibDot.setPadding(0, 0, 5, 0);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(60, 60);
            ibDot.setLayoutParams(params);
            if (index == 0)
                ibDot.setSelected(true);
            llDots.addView(ibDot);
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int index = 0; index < productBannersImagesAdapter.getCount(); index++) {
                    if (index != position) {
                        (llDots.findViewWithTag(index)).setSelected(false);
                    }
                }
                (llDots.findViewWithTag(position)).setSelected(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask(), 5000, 5000);
    }

    private class ProductBannersImagesAdapter extends PagerAdapter {

        private Context context;
        private LinkedList<EditProductProductImage> listOfEditProductProductImage;

        public ProductBannersImagesAdapter(Context context, LinkedList<EditProductProductImage> listOfEditProductProductImage) {
            this.context = context;
            this.listOfEditProductProductImage = listOfEditProductProductImage;
        }

        @Override
        public int getCount() {
            return listOfEditProductProductImage.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return (view == object);
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.layout_home_banner_images_pager_item, container, false);
            EditProductProductImage editProductProductImage = listOfEditProductProductImage.get(position);
            String bannerImageUrl = editProductProductImage.getImageUrl();
            ImageView ivImage = view.findViewById(R.id.ivImage);
            Picasso.get()
                    .load(bannerImageUrl)
                    .error(R.drawable.image_placeholder)
                    .placeholder(R.drawable.image_placeholder)
                    .into(ivImage);
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            //container.removeView( object);
        }
    }

    private class TimerTask extends java.util.TimerTask {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (listOfEditProductProductImage.size() - 1 != count) {
                        count = count + 1;
                        viewPager.setCurrentItem(count);
                    } else {
                        viewPager.setCurrentItem(0);
                        count = 0;
                    }
                }
            });
        }
    }
}
