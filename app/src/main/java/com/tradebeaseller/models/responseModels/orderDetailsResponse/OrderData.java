package com.tradebeaseller.models.responseModels.orderDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderData {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("track_id")
    @Expose
    private String trackId;
    @SerializedName("order_pickup_otp")
    @Expose
    private String orderPickupOtp;
    @SerializedName("preparation_time")
    @Expose
    private Object preparationTime;
    @SerializedName("delivery_address_id")
    @Expose
    private Integer deliveryAddressId;
    @SerializedName("payment_method_id")
    @Expose
    private Integer paymentMethodId;
    @SerializedName("payment_id")
    @Expose
    private Object paymentId;
    @SerializedName("promo_id")
    @Expose
    private Object promoId;
    @SerializedName("message")
    @Expose
    private Object message;
    @SerializedName("created_user_id")
    @Expose
    private Integer createdUserId;
    @SerializedName("seller_user_id")
    @Expose
    private Integer sellerUserId;
    @SerializedName("delivery_boy_user_id")
    @Expose
    private Object deliveryBoyUserId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;
    @SerializedName("order_status")
    @Expose
    private OrderStatus orderStatus;
    @SerializedName("seller_updated_at")
    @Expose
    private Object sellerUpdatedAt;
    @SerializedName("delivery_boy_updated_at")
    @Expose
    private Object deliveryBoyUpdatedAt;
    @SerializedName("delivery_boy_status")
    @Expose
    private String deliveryBoyStatus;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("customer")
    @Expose
    private String customer;
    @SerializedName("delivery_boy")
    @Expose
    private String deliveryBoy;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getOrderPickupOtp() {
        return orderPickupOtp;
    }

    public void setOrderPickupOtp(String orderPickupOtp) {
        this.orderPickupOtp = orderPickupOtp;
    }

    public Object getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(Object preparationTime) {
        this.preparationTime = preparationTime;
    }

    public Integer getDeliveryAddressId() {
        return deliveryAddressId;
    }

    public void setDeliveryAddressId(Integer deliveryAddressId) {
        this.deliveryAddressId = deliveryAddressId;
    }

    public Integer getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Integer paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public Object getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Object paymentId) {
        this.paymentId = paymentId;
    }

    public Object getPromoId() {
        return promoId;
    }

    public void setPromoId(Object promoId) {
        this.promoId = promoId;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public Integer getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(Integer createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Integer getSellerUserId() {
        return sellerUserId;
    }

    public void setSellerUserId(Integer sellerUserId) {
        this.sellerUserId = sellerUserId;
    }

    public Object getDeliveryBoyUserId() {
        return deliveryBoyUserId;
    }

    public void setDeliveryBoyUserId(Object deliveryBoyUserId) {
        this.deliveryBoyUserId = deliveryBoyUserId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Object getSellerUpdatedAt() {
        return sellerUpdatedAt;
    }

    public void setSellerUpdatedAt(Object sellerUpdatedAt) {
        this.sellerUpdatedAt = sellerUpdatedAt;
    }

    public Object getDeliveryBoyUpdatedAt() {
        return deliveryBoyUpdatedAt;
    }

    public void setDeliveryBoyUpdatedAt(Object deliveryBoyUpdatedAt) {
        this.deliveryBoyUpdatedAt = deliveryBoyUpdatedAt;
    }

    public String getDeliveryBoyStatus() {
        return deliveryBoyStatus;
    }

    public void setDeliveryBoyStatus(String deliveryBoyStatus) {
        this.deliveryBoyStatus = deliveryBoyStatus;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getDeliveryBoy() {
        return deliveryBoy;
    }

    public void setDeliveryBoy(String deliveryBoy) {
        this.deliveryBoy = deliveryBoy;
    }
}
