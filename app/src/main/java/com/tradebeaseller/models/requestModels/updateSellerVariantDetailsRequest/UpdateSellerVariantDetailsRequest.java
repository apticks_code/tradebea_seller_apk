package com.tradebeaseller.models.requestModels.updateSellerVariantDetailsRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateSellerVariantDetailsRequest {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("product_id")
    @Expose
    public Integer productId;
    @SerializedName("variant_id")
    @Expose
    public Integer variantId;
    @SerializedName("selling_price")
    @Expose
    public String sellingPrice;
    @SerializedName("seller_mrp")
    @Expose
    public String sellerMrp;
    @SerializedName("qty")
    @Expose
    public String qty;
    @SerializedName("status")
    @Expose
    public String status;
}
