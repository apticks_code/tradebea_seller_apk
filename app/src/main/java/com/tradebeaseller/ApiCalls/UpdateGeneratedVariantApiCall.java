package com.tradebeaseller.ApiCalls;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.jsonBuilderParser.JsonBuilderParser;
import com.tradebeaseller.models.requestModels.updateVariantRequest.UpdateVariantRequest;
import com.tradebeaseller.utils.CommonMethods;
import com.tradebeaseller.utils.Constants;

import org.json.JSONObject;

public class UpdateGeneratedVariantApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallToUpdateGeneratedVariant(Context context, Fragment fragment, RecyclerView.Adapter adapter, String id, String mrp, String token) {
        String url = Constants.UPDATE_GENERATED_VARIANT_DETAILS_URL;
        int variantId = Integer.parseInt(id);
        UpdateVariantRequest updateVariantRequest = new UpdateVariantRequest();
        updateVariantRequest.setVariantId(variantId);
        updateVariantRequest.setMrp(mrp);
        JSONObject postObject = JsonBuilderParser.jsonBuilder(updateVariantRequest);
        AndroidNetworking.post(url)
                .addJSONObjectBody(postObject)
                .setPriority(Priority.HIGH)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("X_AUTH_TOKEN", token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_UPDATE_GENERATED_VARIANT);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_UPDATE_GENERATED_VARIANT);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_UPDATE_GENERATED_VARIANT);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_UPDATE_GENERATED_VARIANT);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_UPDATE_GENERATED_VARIANT);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_UPDATE_GENERATED_VARIANT);
                        }
                    }
                });
    }
}
