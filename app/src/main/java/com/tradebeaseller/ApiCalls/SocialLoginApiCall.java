package com.tradebeaseller.ApiCalls;

import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.jsonBuilderParser.JsonBuilderParser;
import com.tradebeaseller.models.requestModels.socialLoginDetailsRequest.SocialLoginDetailsRequest;
import com.tradebeaseller.utils.CommonMethods;
import com.tradebeaseller.utils.Constants;

import org.json.JSONObject;

public class SocialLoginApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallForSocialLoginDetails(Context context, String name, String imageUrl, String emailId, String id, String token) {
        String url = Constants.SOCIAL_LOGIN_URL;
        SocialLoginDetailsRequest socialLoginDetailsRequest = new SocialLoginDetailsRequest();
        socialLoginDetailsRequest.name = name;
        socialLoginDetailsRequest.groupId = 2;
        socialLoginDetailsRequest.authToken = token;
        socialLoginDetailsRequest.authId = id;
        socialLoginDetailsRequest.profileImage = imageUrl;
        socialLoginDetailsRequest.mobile = "";
        socialLoginDetailsRequest.mail = emailId;
        JSONObject postObject = JsonBuilderParser.jsonBuilder(socialLoginDetailsRequest);
        AndroidNetworking.post(url)
                .addJSONObjectBody(postObject) // posting json
                .setPriority(Priority.HIGH)
                .addHeaders("APP_id", Constants.APP_ID)
                .addHeaders("Content-Type", "application/json")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_SOCIAL_LOGIN);
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_SOCIAL_LOGIN);
                    }
                });
    }
}
