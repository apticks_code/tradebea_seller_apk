package com.tradebeaseller.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.tradebeaseller.R;
import com.tradebeaseller.interfaces.CloseCallBack;
import com.tradebeaseller.interfaces.SelectedSubCategoryCallBack;
import com.tradebeaseller.models.responseModels.subCategoryDetailsResponse.SubCategoryDetails;
import com.tradebeaseller.utils.UserData;

import java.util.LinkedList;

public class SubCategoryDetailsAdapter extends RecyclerView.Adapter<SubCategoryDetailsAdapter.ViewHolder> implements View.OnClickListener {

    private Context context;
    private LinkedList<SubCategoryDetails> listOfSubCategoryDetails;

    private String selectedSubCategoryId = "";
    private String selectedSubCategoryName = "";

    private int selectedPosition = -1;

    public SubCategoryDetailsAdapter(Context context, LinkedList<SubCategoryDetails> listOfSubCategoryDetails, String selectedSubCategoryId, String selectedSubCategoryName, TextView tvSave) {
        this.context = context;
        tvSave.setOnClickListener(this);
        this.selectedSubCategoryId = selectedSubCategoryId;
        this.selectedSubCategoryName = selectedSubCategoryName;
        this.listOfSubCategoryDetails = listOfSubCategoryDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_sub_category_details_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        SubCategoryDetails subCategoryDetails = listOfSubCategoryDetails.get(position);
        if (subCategoryDetails != null) {
            String id = String.valueOf(subCategoryDetails.getId());
            String imageUrl = subCategoryDetails.getImageUrl();
            String subCategoryName = subCategoryDetails.getName();
            holder.tvSubCategoryName.setText(subCategoryName);

            if (id.equalsIgnoreCase(selectedSubCategoryId)) {
                holder.checkBox.setChecked(true);
            } else {
                holder.checkBox.setChecked(false);
            }

            if (imageUrl != null) {
                if (!imageUrl.isEmpty()) {
                    Glide.with(context)
                            .load(imageUrl)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .placeholder(R.drawable.ic_user_image)
                            .error(R.drawable.ic_user_image)
                            .into(holder.ivPic);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return listOfSubCategoryDetails.size();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.tvSave:
                prepareSaveDetails();
                break;
            default:
                break;
        }
    }

    private void prepareSaveDetails() {
        Context context = UserData.getInstance().getContext();
        if (context != null) {
            SelectedSubCategoryCallBack selectedSubCategoryCallBack = (SelectedSubCategoryCallBack) context;
            selectedSubCategoryCallBack.selectedSubCategoryDetails(selectedSubCategoryId, selectedSubCategoryName);
        }

        CloseCallBack closeCallBack = (CloseCallBack) this.context;
        closeCallBack.close();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {

        private ImageView ivPic;
        private CheckBox checkBox;
        private TextView tvSubCategoryName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivPic = itemView.findViewById(R.id.ivPic);
            checkBox = itemView.findViewById(R.id.checkBox);
            tvSubCategoryName = itemView.findViewById(R.id.tvSubCategoryName);
            checkBox.setOnCheckedChangeListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                selectedSubCategoryId = String.valueOf(listOfSubCategoryDetails.get(getLayoutPosition()).getId());
                selectedSubCategoryName = listOfSubCategoryDetails.get(getLayoutPosition()).getName();
                if (selectedPosition == -1) {
                    selectedPosition = getLayoutPosition();
                } else {
                    if (selectedPosition != getLayoutPosition()) {
                        selectedPosition = getLayoutPosition();
                        android.os.Handler handler = new android.os.Handler();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                notifyDataSetChanged();
                            }
                        });
                    }
                }
            } else {
                if (selectedPosition == getLayoutPosition()) {
                    selectedSubCategoryId = "";
                    selectedSubCategoryName = "";
                    selectedPosition = -1;
                }
            }
        }
    }
}
