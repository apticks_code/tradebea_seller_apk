
package com.tradebeaseller.models.responseModels.dashboardResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Dashboarddata {

    @SerializedName("received_orders")
    @Expose
    private Integer receivedOrders;
    @SerializedName("accepted_orders")
    @Expose
    private Integer acceptedOrders;
    @SerializedName("completed_orders")
    @Expose
    private Integer completedOrders;
    @SerializedName("active_products")
    @Expose
    private Integer activeProducts;
    @SerializedName("approved_products")
    @Expose
    private Integer approvedProducts;
    @SerializedName("pending_products")
    @Expose
    private Integer pendingProducts;
    @SerializedName("wallet_money")
    @Expose
    private Integer walletMoney;
    @SerializedName("next_settlement_date")
    @Expose
    private String nextSettlementDate;

    public Integer getReceivedOrders() {
        return receivedOrders;
    }

    public void setReceivedOrders(Integer receivedOrders) {
        this.receivedOrders = receivedOrders;
    }

    public Integer getAcceptedOrders() {
        return acceptedOrders;
    }

    public void setAcceptedOrders(Integer acceptedOrders) {
        this.acceptedOrders = acceptedOrders;
    }

    public Integer getCompletedOrders() {
        return completedOrders;
    }

    public void setCompletedOrders(Integer completedOrders) {
        this.completedOrders = completedOrders;
    }

    public Integer getActiveProducts() {
        return activeProducts;
    }

    public void setActiveProducts(Integer activeProducts) {
        this.activeProducts = activeProducts;
    }

    public Integer getApprovedProducts() {
        return approvedProducts;
    }

    public void setApprovedProducts(Integer approvedProducts) {
        this.approvedProducts = approvedProducts;
    }

    public Integer getPendingProducts() {
        return pendingProducts;
    }

    public void setPendingProducts(Integer pendingProducts) {
        this.pendingProducts = pendingProducts;
    }

    public Integer getWalletMoney() {
        return walletMoney;
    }

    public void setWalletMoney(Integer walletMoney) {
        this.walletMoney = walletMoney;
    }

    public String getNextSettlementDate() {
        return nextSettlementDate;
    }

    public void setNextSettlementDate(String nextSettlementDate) {
        this.nextSettlementDate = nextSettlementDate;
    }

}
