package com.tradebeaseller.models.requestModels.orderAcceptRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderAcceptRequest {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("status")
    @Expose
    public Integer status;
    @SerializedName("message")
    @Expose
    public String message = "";
    @SerializedName("preparation_time")
    @Expose
    public String preparationTime = "";
}
