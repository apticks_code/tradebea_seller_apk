package com.tradebeaseller.models.responseModels.selectedOrderDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderDetails {

    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("variant_id")
    @Expose
    private Integer variantId;
    @SerializedName("seller_variant_id")
    @Expose
    private Integer sellerVariantId;
    @SerializedName("qty")
    @Expose
    private Integer qty;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("tax")
    @Expose
    private Integer tax;
    @SerializedName("delivery_fee")
    @Expose
    private Integer deliveryFee;
    @SerializedName("sub_total")
    @Expose
    private Integer subTotal;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("promo_discount")
    @Expose
    private Integer promoDiscount;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("product")
    @Expose
    private Product product;
    @SerializedName("varinat")
    @Expose
    private Varinat varinat;
    @SerializedName("seller_varinat")
    @Expose
    private SellerVarinat sellerVarinat;
    @SerializedName("rules")
    @Expose
    private Rules rules;


    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getVariantId() {
        return variantId;
    }

    public void setVariantId(Integer variantId) {
        this.variantId = variantId;
    }

    public Integer getSellerVariantId() {
        return sellerVariantId;
    }

    public void setSellerVariantId(Integer sellerVariantId) {
        this.sellerVariantId = sellerVariantId;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getTax() {
        return tax;
    }

    public void setTax(Integer tax) {
        this.tax = tax;
    }

    public Integer getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(Integer deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public Integer getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Integer subTotal) {
        this.subTotal = subTotal;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getPromoDiscount() {
        return promoDiscount;
    }

    public void setPromoDiscount(Integer promoDiscount) {
        this.promoDiscount = promoDiscount;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Varinat getVarinat() {
        return varinat;
    }

    public void setVarinat(Varinat varinat) {
        this.varinat = varinat;
    }

    public SellerVarinat getSellerVarinat() {
        return sellerVarinat;
    }

    public void setSellerVarinat(SellerVarinat sellerVarinat) {
        this.sellerVarinat = sellerVarinat;
    }

    public Rules getRules() {
        return rules;
    }

    public void setRules(Rules rules) {
        this.rules = rules;
    }
}
