package com.tradebeaseller.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.tradebeaseller.ApiCalls.BrandDetailsApiCall;
import com.tradebeaseller.R;
import com.tradebeaseller.adapters.BrandDetailsAdapter;
import com.tradebeaseller.interfaces.CloseCallBack;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.models.responseModels.brandDetailsResponse.BrandDetails;
import com.tradebeaseller.models.responseModels.brandDetailsResponse.BrandDetailsResponse;
import com.tradebeaseller.utils.Constants;

import java.util.LinkedList;

public class BrandDetailsActivity extends BaseActivity implements HttpReqResCallBack, View.OnClickListener, CloseCallBack {

    private ImageView ivBackArrow;
    private TextView tvSave, tvError;
    private RecyclerView rvBrandDetails;

    private LinkedList<BrandDetails> listOfBrandDetails;

    private String selectedBrandId = "";
    private String selectedBrandName = "";
    private String selectedCategoryId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_brand_details);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.selected_category_id)))
                selectedCategoryId = bundle.getString(getString(R.string.selected_category_id), "");
            if (bundle.containsKey(getString(R.string.selected_brand_id)))
                selectedBrandId = bundle.getString(getString(R.string.selected_brand_id), "");
            if (bundle.containsKey(getString(R.string.selected_brand_name)))
                selectedBrandName = bundle.getString(getString(R.string.selected_brand_name), "");
        }
    }

    private void initializeUi() {
        tvSave = findViewById(R.id.tvSave);
        tvError = findViewById(R.id.tvError);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        rvBrandDetails = findViewById(R.id.rvBrandDetails);
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareDetails() {
        showProgressBar(this);
        BrandDetailsApiCall.serviceCallForBrandDetails(this, null, null, selectedCategoryId);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_BRAND_DETAILS:
                if (jsonResponse != null) {
                    BrandDetailsResponse brandDetailsResponse = new Gson().fromJson(jsonResponse, BrandDetailsResponse.class);
                    if (brandDetailsResponse != null) {
                        boolean status = brandDetailsResponse.getStatus();
                        if (status) {
                            listOfBrandDetails = brandDetailsResponse.getListOfBrandDetails();
                            if (listOfBrandDetails != null) {
                                if (listOfBrandDetails.size() != 0) {
                                    listIsFull();
                                    initializeAdapter();
                                } else {
                                    listIsEmpty();
                                }
                            } else {
                                listIsEmpty();
                            }
                        }
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    private void initializeAdapter() {
        BrandDetailsAdapter brandDetailsAdapter = new BrandDetailsAdapter(this, listOfBrandDetails, selectedBrandId, selectedBrandName, tvSave);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvBrandDetails.setLayoutManager(layoutManager);
        rvBrandDetails.setItemAnimator(new DefaultItemAnimator());
        rvBrandDetails.setAdapter(brandDetailsAdapter);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvBrandDetails.setVisibility(View.GONE);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvBrandDetails.setVisibility(View.VISIBLE);
    }

    @Override
    public void close() {
        finish();
    }
}
