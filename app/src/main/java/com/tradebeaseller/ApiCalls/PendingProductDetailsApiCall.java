package com.tradebeaseller.ApiCalls;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.jsonBuilderParser.JsonBuilderParser;
import com.tradebeaseller.models.requestModels.getPendingProductDetailsRequest.GetPendingProductDetailsRequest;
import com.tradebeaseller.utils.CommonMethods;
import com.tradebeaseller.utils.Constants;

import org.json.JSONObject;

public class PendingProductDetailsApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallToGetPendingProductDetails(Context context, Fragment fragment, RecyclerView.Adapter adapter, String token, String searchedText, int menuId, int catId, int subCatId, String subSubCatId, int brandId) {
        String url = Constants.PENDING_PRODUCT_DETAILS_URL;
        GetPendingProductDetailsRequest getPendingProductDetailsRequest = new GetPendingProductDetailsRequest();
        getPendingProductDetailsRequest.status = 3;
        getPendingProductDetailsRequest.pageNo = 1;
        getPendingProductDetailsRequest.q = searchedText;
        if (menuId != -1)
            getPendingProductDetailsRequest.menuId = menuId;
        if (catId != -1)
            getPendingProductDetailsRequest.catId = catId;
        if (subCatId != -1)
            getPendingProductDetailsRequest.subCatId = subCatId;
        if (!subSubCatId.isEmpty())
            getPendingProductDetailsRequest.subSubCatId = subSubCatId;
        if (brandId != -1)
            getPendingProductDetailsRequest.brandId = brandId;
        JSONObject postObject = JsonBuilderParser.jsonBuilder(getPendingProductDetailsRequest);
        AndroidNetworking.post(url)
                .addJSONObjectBody(postObject)
                .setPriority(Priority.HIGH)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("X_AUTH_TOKEN", token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_GET_PENDING_PRODUCT_DETAILS);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_GET_PENDING_PRODUCT_DETAILS);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_GET_PENDING_PRODUCT_DETAILS);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_PENDING_PRODUCT_DETAILS);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_PENDING_PRODUCT_DETAILS);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_PENDING_PRODUCT_DETAILS);
                        }
                    }
                });
    }
}
