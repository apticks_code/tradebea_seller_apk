
package com.tradebeaseller.models.responseModels.SingleOrderDetailsResponse;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class OrderStatus {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("current_status")
    @Expose
    private String currentStatus;
    @SerializedName("all_statuses_list")
    @Expose
    private List<String> allStatusesList = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public List<String> getAllStatusesList() {
        return allStatusesList;
    }

    public void setAllStatusesList(List<String> allStatusesList) {
        this.allStatusesList = allStatusesList;
    }

}
