package com.tradebeaseller.interfaces;

public interface HomeNavigationCallBack {
    void homeNavigationCallBack(String goTo);
}
