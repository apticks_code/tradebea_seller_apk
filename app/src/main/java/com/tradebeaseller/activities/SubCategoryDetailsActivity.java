package com.tradebeaseller.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.tradebeaseller.ApiCalls.SubCategoryDetailsApiCall;
import com.tradebeaseller.R;
import com.tradebeaseller.adapters.SubCategoryDetailsAdapter;
import com.tradebeaseller.interfaces.CloseCallBack;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.models.responseModels.subCategoryDetailsResponse.Data;
import com.tradebeaseller.models.responseModels.subCategoryDetailsResponse.SubCategoryDetails;
import com.tradebeaseller.models.responseModels.subCategoryDetailsResponse.SubCategoryDetailsResponse;
import com.tradebeaseller.utils.Constants;

import java.util.LinkedList;

public class SubCategoryDetailsActivity extends BaseActivity implements HttpReqResCallBack, View.OnClickListener, CloseCallBack {

    private ImageView ivBackArrow;
    private TextView tvSave, tvError;
    private RecyclerView rvSubCategoryDetails;

    private LinkedList<SubCategoryDetails> listOfSubCategoryDetails;

    private String selectedCategoryId = "";
    private String selectedCategoryName = "";
    private String selectedSubCategoryId = "";
    private String selectedSubCategoryName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_sub_category_details);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.selected_category_id)))
                selectedCategoryId = bundle.getString(getString(R.string.selected_category_id), "");
            if (bundle.containsKey(getString(R.string.selected_category_name)))
                selectedCategoryName = bundle.getString(getString(R.string.selected_category_name), "");
            if (bundle.containsKey(getString(R.string.selected_sub_category_id)))
                selectedSubCategoryId = bundle.getString(getString(R.string.selected_sub_category_id), "");
            if (bundle.containsKey(getString(R.string.selected_sub_category_name)))
                selectedSubCategoryName = bundle.getString(getString(R.string.selected_sub_category_name), "");
        }
    }

    private void initializeUi() {
        tvSave = findViewById(R.id.tvSave);
        tvError = findViewById(R.id.tvError);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        rvSubCategoryDetails = findViewById(R.id.rvSubCategoryDetails);
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareDetails() {
        showProgressBar(this);
        SubCategoryDetailsApiCall.serviceCallForSubCategoryDetails(this, null, null, selectedCategoryId);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_SUB_CATEGORY_DETAILS:
                if (jsonResponse != null) {
                    SubCategoryDetailsResponse subCategoryDetailsResponse = new Gson().fromJson(jsonResponse, SubCategoryDetailsResponse.class);
                    if (subCategoryDetailsResponse != null) {
                        boolean status = subCategoryDetailsResponse.getStatus();
                        if (status) {
                            Data data = subCategoryDetailsResponse.getData();
                            if (data != null) {
                                listOfSubCategoryDetails = data.getListOfSubCategoryDetails();
                                if (listOfSubCategoryDetails != null) {
                                    if (listOfSubCategoryDetails.size() != 0) {
                                        listIsFull();
                                        initializeAdapter();
                                    } else {
                                        listIsEmpty();
                                    }
                                } else {
                                    listIsEmpty();
                                }
                            }
                        }
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    private void initializeAdapter() {
        SubCategoryDetailsAdapter subCategoryDetailsAdapter = new SubCategoryDetailsAdapter(this, listOfSubCategoryDetails, selectedSubCategoryId, selectedSubCategoryName, tvSave);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvSubCategoryDetails.setLayoutManager(layoutManager);
        rvSubCategoryDetails.setItemAnimator(new DefaultItemAnimator());
        rvSubCategoryDetails.setAdapter(subCategoryDetailsAdapter);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvSubCategoryDetails.setVisibility(View.GONE);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvSubCategoryDetails.setVisibility(View.VISIBLE);
    }

    @Override
    public void close() {
        finish();
    }
}
