
package com.tradebeaseller.models.responseModels.dashboardResponse;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DashboardDataModel {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("http_code")
    @Expose
    private Integer httpCode;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Dashboarddata dashboarddata;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(Integer httpCode) {
        this.httpCode = httpCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Dashboarddata getDashboarddata() {
        return dashboarddata;
    }

    public void setDashboarddata(Dashboarddata dashboarddata) {
        this.dashboarddata = dashboarddata;
    }

}
