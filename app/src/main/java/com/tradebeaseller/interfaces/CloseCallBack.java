package com.tradebeaseller.interfaces;

public interface CloseCallBack {
    void close();
}
