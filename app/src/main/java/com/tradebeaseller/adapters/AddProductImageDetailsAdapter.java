package com.tradebeaseller.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.tradebeaseller.R;

import java.io.File;
import java.util.LinkedList;

public class AddProductImageDetailsAdapter extends RecyclerView.Adapter<AddProductImageDetailsAdapter.ViewHolder> {

    private Context context;
    private Transformation transformation;
    private ItemClickListener clickListener;
    private LinkedList<String> listOfSelectedPhotoPaths;

    public AddProductImageDetailsAdapter(Context context, LinkedList<String> listOfSelectedPhotoPaths) {
        this.context = context;
        this.listOfSelectedPhotoPaths = listOfSelectedPhotoPaths;


        transformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(5)
                .borderColor(Color.parseColor("#00000000"))
                .borderWidthDp(1)
                .oval(false)
                .build();
    }

    public interface ItemClickListener {
        void onClick(View view, int position);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_add_product_image_details_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String selectedImageUrl = listOfSelectedPhotoPaths.get(position);
        File file = new File(selectedImageUrl);
        Picasso.get()
                .load(file)
                .error(R.drawable.image_placeholder)
                .placeholder(R.drawable.image_placeholder)
                .transform(transformation)
                .fit().centerCrop()
                .into(holder.ivProductImage);
    }

    @Override
    public int getItemCount() {
        return listOfSelectedPhotoPaths.size();
    }

    public void updateImages(LinkedList<String> listOfSelectedPhotoPaths) {
        this.listOfSelectedPhotoPaths = listOfSelectedPhotoPaths;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView ivProductImage, ivDeleteImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivDeleteImage = itemView.findViewById(R.id.ivDeleteImage);
            ivProductImage = itemView.findViewById(R.id.ivProductImage);
            ivDeleteImage.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            if (id == R.id.ivDeleteImage) {
                if (clickListener != null)
                    clickListener.onClick(view, getLayoutPosition());
            }
        }
    }
}
