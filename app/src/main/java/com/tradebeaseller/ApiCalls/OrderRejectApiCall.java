package com.tradebeaseller.ApiCalls;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.jsonBuilderParser.JsonBuilderParser;
import com.tradebeaseller.models.requestModels.orderAcceptRequest.OrderAcceptRequest;
import com.tradebeaseller.models.requestModels.orderRejectRequest.OrderRejectRequest;
import com.tradebeaseller.utils.CommonMethods;
import com.tradebeaseller.utils.Constants;

import org.json.JSONObject;

public class OrderRejectApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallForOrderReject(Context context, Fragment fragment, RecyclerView.Adapter adapter, int orderId, String token,String message) {
        String url = Constants.ORDER__REJECT_DETAILS_URL;
        OrderRejectRequest orderRejectRequest = new OrderRejectRequest();
        orderRejectRequest.id = orderId;
        orderRejectRequest.status = 3;
        orderRejectRequest.message = message;
        JSONObject postObject = JsonBuilderParser.jsonBuilder(orderRejectRequest);
        AndroidNetworking.post(url)
                .addJSONObjectBody(postObject)
                .setPriority(Priority.HIGH)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("X_AUTH_TOKEN", token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_ORDER_REJECT_DETAILS);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_ORDER_REJECT_DETAILS);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_ORDER_REJECT_DETAILS);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_ORDER_REJECT_DETAILS);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_ORDER_REJECT_DETAILS);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_ORDER_REJECT_DETAILS);
                        }
                    }
                });
    }
}
