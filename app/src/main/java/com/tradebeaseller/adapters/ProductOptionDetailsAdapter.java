package com.tradebeaseller.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.tradebeaseller.R;
import com.tradebeaseller.activities.ProductOptionDetailsActivity;
import com.tradebeaseller.interfaces.CloseCallBack;
import com.tradebeaseller.interfaces.SelectedProductOptionDetailsCallBack;
import com.tradebeaseller.models.responseModels.optionDetailsResponse.Option;
import com.tradebeaseller.models.responseModels.optionDetailsResponse.OptionsData;
import com.tradebeaseller.utils.UserData;

import java.util.LinkedList;

public class ProductOptionDetailsAdapter extends RecyclerView.Adapter<ProductOptionDetailsAdapter.ViewHolder> implements View.OnClickListener {

    private Context context;
    private ColorGenerator generator;
    private TextDrawable.IBuilder mDrawableBuilder;

    private LinkedList<OptionsData> listOfOptionsData;

    private String selectedProductOptionId = "";
    private String selectedProductOptionName = "";

    private int selectedPosition = -1;

    public ProductOptionDetailsAdapter(Context context, LinkedList<OptionsData> listOfOptionsData, String selectedProductOptionId, String selectedProductOptionName, TextView tvSave) {
        this.context = context;
        tvSave.setOnClickListener(this);
        generator = ColorGenerator.MATERIAL;
        this.listOfOptionsData = listOfOptionsData;
        mDrawableBuilder = TextDrawable.builder().round();
        this.selectedProductOptionId = selectedProductOptionId;
        this.selectedProductOptionName = selectedProductOptionName;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_product_option_details_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OptionsData optionsData = listOfOptionsData.get(position);
        if (optionsData != null) {
            Option option = optionsData.getOption();
            if (option != null) {
                String id = String.valueOf(option.getId());
                String name = option.getName();

                int randomColor = generator.getRandomColor();
                TextDrawable drawable = mDrawableBuilder.build(String.valueOf(name.charAt(0)), randomColor);
                holder.tvProductOptionName.setText(name);
                holder.ivPic.setImageDrawable(drawable);

                if (id.equalsIgnoreCase(selectedProductOptionId)) {
                    holder.checkBox.setChecked(true);
                } else {
                    holder.checkBox.setChecked(false);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return listOfOptionsData.size();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.tvSave:
                prepareSaveDetails();
                break;
            default:
                break;
        }
    }

    private void prepareSaveDetails() {
        Context context = UserData.getInstance().getContext();
        if (context != null) {
            SelectedProductOptionDetailsCallBack selectedProductOptionDetailsCallBack = (SelectedProductOptionDetailsCallBack) context;
            selectedProductOptionDetailsCallBack.selectedProductOptionDetails(selectedProductOptionId, selectedProductOptionName);
        }

        CloseCallBack closeCallBack = (CloseCallBack) this.context;
        closeCallBack.close();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {

        private ImageView ivPic;
        private CheckBox checkBox;
        private TextView tvProductOptionName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivPic = itemView.findViewById(R.id.ivPic);
            checkBox = itemView.findViewById(R.id.checkBox);
            tvProductOptionName = itemView.findViewById(R.id.tvProductOptionName);
            checkBox.setOnCheckedChangeListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                selectedProductOptionId = String.valueOf(listOfOptionsData.get(getLayoutPosition()).getOption().getId());
                selectedProductOptionName = listOfOptionsData.get(getLayoutPosition()).getOption().getName();
                if (selectedPosition == -1) {
                    selectedPosition = getLayoutPosition();
                } else {
                    if (selectedPosition != getLayoutPosition()) {
                        selectedPosition = getLayoutPosition();
                        android.os.Handler handler = new android.os.Handler();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                notifyDataSetChanged();
                            }
                        });
                    }
                }
            } else {
                if (selectedPosition == getLayoutPosition()) {
                    selectedProductOptionId = "";
                    selectedProductOptionName = "";
                    selectedPosition = -1;
                }
            }
        }
    }
}
