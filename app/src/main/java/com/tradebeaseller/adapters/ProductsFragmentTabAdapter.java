package com.tradebeaseller.adapters;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.tabs.TabLayout;
import com.tradebeaseller.fragments.ApprovedProductsFragment;
import com.tradebeaseller.fragments.CatalogProductFragment;
import com.tradebeaseller.fragments.MyProductsFragment;
import com.tradebeaseller.fragments.PendingProductsFragment;
import com.tradebeaseller.utils.TabInfo;

import java.util.ArrayList;

public class ProductsFragmentTabAdapter implements TabLayout.OnTabSelectedListener {

    private Context context;
    private Fragment fragment;
    private TabLayout tabLayout;
    private FragmentManager fragmentManager;

    private final ArrayList<TabInfo> mTabs = new ArrayList<>();

    private int fragmentContainer;

    public ProductsFragmentTabAdapter(Context context, Fragment fragment, FragmentManager fragmentManager, TabLayout tabLayout, int fragmentContainer) {
        this.context = context;
        this.fragment = fragment;
        this.tabLayout = tabLayout;
        this.fragmentManager = fragmentManager;
        this.fragmentContainer = fragmentContainer;
        tabLayout.addOnTabSelectedListener(this);
    }

    public void addTab(TabLayout.Tab tabSpec, Class<?> clss, Bundle args) {
        String tag = (String) tabSpec.getTag();
        TabInfo info = new TabInfo(tag, clss, args);
        mTabs.add(info);
        tabLayout.addTab(tabSpec);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        int position = tab.getPosition();
        String tabName = (String) tab.getTag();
        TabInfo info = mTabs.get(position);
        switch (position) {
            case 0:
                fragment = new CatalogProductFragment();
                break;
            case 1:
                fragment = new MyProductsFragment();
                break;
            case 2:
                fragment = new ApprovedProductsFragment();
                break;
            case 3:
                fragment = new PendingProductsFragment();
                break;
            default:
                break;
        }
        fragmentManager.beginTransaction().replace(fragmentContainer, fragment).commit();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
