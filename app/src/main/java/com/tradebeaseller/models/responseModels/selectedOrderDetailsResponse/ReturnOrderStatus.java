package com.tradebeaseller.models.responseModels.selectedOrderDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReturnOrderStatus {

    @SerializedName("status_id")
    @Expose
    private Integer statusId;
    @SerializedName("status_name")
    @Expose
    private String statusName;

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
}
