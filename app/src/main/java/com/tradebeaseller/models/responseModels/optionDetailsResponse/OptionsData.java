package com.tradebeaseller.models.responseModels.optionDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OptionsData {

    @SerializedName("option_id")
    @Expose
    private Integer optionId;
    @SerializedName("option")
    @Expose
    private Option option;


    public Integer getOptionId() {
        return optionId;
    }

    public void setOptionId(Integer optionId) {
        this.optionId = optionId;
    }

    public Option getOption() {
        return option;
    }

    public void setOption(Option option) {
        this.option = option;
    }
}
