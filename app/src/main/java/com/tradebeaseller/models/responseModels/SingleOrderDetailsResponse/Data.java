
package com.tradebeaseller.models.responseModels.SingleOrderDetailsResponse;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("track_id")
    @Expose
    private Double trackId;
    @SerializedName("order_delivery_otp")
    @Expose
    private Object orderDeliveryOtp;
    @SerializedName("order_pickup_otp")
    @Expose
    private Object orderPickupOtp;
    @SerializedName("delivery_address_id")
    @Expose
    private Integer deliveryAddressId;
    @SerializedName("payment_method_id")
    @Expose
    private Integer paymentMethodId;
    @SerializedName("payment_id")
    @Expose
    private Integer paymentId;
    @SerializedName("promo_id")
    @Expose
    private Object promoId;
    @SerializedName("message")
    @Expose
    private Object message;
    @SerializedName("created_user_id")
    @Expose
    private Integer createdUserId;
    @SerializedName("seller_user_id")
    @Expose
    private Integer sellerUserId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;
    @SerializedName("order_status")
    @Expose
    private OrderStatus orderStatus;
    @SerializedName("order_details")
    @Expose
    private List<OrderDetail> orderDetails = null;
    @SerializedName("delivery_jobs")
    @Expose
    private Object deliveryJobs;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("delivery_boy_status")
    @Expose
    private String deliveryBoyStatus;
    @SerializedName("customer")
    @Expose
    private String customer;
    @SerializedName("delivery_boy")
    @Expose
    private String deliveryBoy;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getTrackId() {
        return trackId;
    }

    public void setTrackId(Double trackId) {
        this.trackId = trackId;
    }

    public Object getOrderDeliveryOtp() {
        return orderDeliveryOtp;
    }

    public void setOrderDeliveryOtp(Object orderDeliveryOtp) {
        this.orderDeliveryOtp = orderDeliveryOtp;
    }

    public Object getOrderPickupOtp() {
        return orderPickupOtp;
    }

    public void setOrderPickupOtp(Object orderPickupOtp) {
        this.orderPickupOtp = orderPickupOtp;
    }

    public Integer getDeliveryAddressId() {
        return deliveryAddressId;
    }

    public void setDeliveryAddressId(Integer deliveryAddressId) {
        this.deliveryAddressId = deliveryAddressId;
    }

    public Integer getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Integer paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    public Object getPromoId() {
        return promoId;
    }

    public void setPromoId(Object promoId) {
        this.promoId = promoId;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public Integer getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(Integer createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Integer getSellerUserId() {
        return sellerUserId;
    }

    public void setSellerUserId(Integer sellerUserId) {
        this.sellerUserId = sellerUserId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public List<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }

    public Object getDeliveryJobs() {
        return deliveryJobs;
    }

    public void setDeliveryJobs(Object deliveryJobs) {
        this.deliveryJobs = deliveryJobs;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDeliveryBoyStatus() {
        return deliveryBoyStatus;
    }

    public void setDeliveryBoyStatus(String deliveryBoyStatus) {
        this.deliveryBoyStatus = deliveryBoyStatus;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getDeliveryBoy() {
        return deliveryBoy;
    }

    public void setDeliveryBoy(String deliveryBoy) {
        this.deliveryBoy = deliveryBoy;
    }

}
