package com.tradebeaseller.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.tradebeaseller.ApiCalls.MenuDetailsApiCall;
import com.tradebeaseller.R;
import com.tradebeaseller.adapters.MenuDetailsAdapter;
import com.tradebeaseller.interfaces.CloseCallBack;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.models.responseModels.menuDetailsResponse.MenuDetails;
import com.tradebeaseller.models.responseModels.menuDetailsResponse.MenuDetailsResponse;
import com.tradebeaseller.utils.Constants;

import java.util.LinkedList;

public class MenuDetailsActivity extends BaseActivity implements HttpReqResCallBack, View.OnClickListener, CloseCallBack {

    private ImageView ivBackArrow;
    private TextView tvError, tvSave;
    private RecyclerView rvMenuDetails;
    private LinkedList<MenuDetails> listOfMenuDetails;

    private String selectedMenuId = "";
    private String selectedMenuName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_menu_details);
        getDateFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();
    }

    private void getDateFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.selected_menu_id)))
                selectedMenuId = bundle.getString(getString(R.string.selected_menu_id), "");
            if (bundle.containsKey(getString(R.string.selected_menu_name)))
                selectedMenuName = bundle.getString(getString(R.string.selected_menu_name), "");
        }
    }

    private void initializeUi() {
        tvSave = findViewById(R.id.tvSave);
        tvError = findViewById(R.id.tvError);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        rvMenuDetails = findViewById(R.id.rvMenuDetails);
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareDetails() {
        showProgressBar(this);
        MenuDetailsApiCall.serviceCallForMenuDetails(this, null, null);
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_MENU_DETAILS:
                if (jsonResponse != null) {
                    MenuDetailsResponse menuDetailsResponse = new Gson().fromJson(jsonResponse, MenuDetailsResponse.class);
                    if (menuDetailsResponse != null) {
                        boolean status = menuDetailsResponse.getStatus();
                        if (status) {
                            listOfMenuDetails = menuDetailsResponse.getListOfMenuDetails();
                            if (listOfMenuDetails != null) {
                                if (listOfMenuDetails.size() != 0) {
                                    listIsFull();
                                    initializeAdapter();
                                } else {
                                    listIsEmpty();
                                }
                            } else {
                                listIsEmpty();
                            }
                        }
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    private void initializeAdapter() {
        MenuDetailsAdapter menuDetailsAdapter = new MenuDetailsAdapter(this, listOfMenuDetails, selectedMenuId, selectedMenuName, tvSave);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvMenuDetails.setLayoutManager(layoutManager);
        rvMenuDetails.setItemAnimator(new DefaultItemAnimator());
        rvMenuDetails.setAdapter(menuDetailsAdapter);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvMenuDetails.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvMenuDetails.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    public void close() {
        finish();
    }
}
