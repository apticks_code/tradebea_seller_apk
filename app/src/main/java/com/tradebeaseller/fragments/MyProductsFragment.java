package com.tradebeaseller.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.tradebeaseller.ApiCalls.MyProductDetailsApiCall;
import com.tradebeaseller.R;
import com.tradebeaseller.activities.AddProductActivity;
import com.tradebeaseller.adapters.MyProductsFragmentAdapter;
import com.tradebeaseller.interfaces.CloseCallBack;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.models.responseModels.displayProductDetailsResponse.Data;
import com.tradebeaseller.models.responseModels.displayProductDetailsResponse.DisplayProductDetailsResponse;
import com.tradebeaseller.models.responseModels.displayProductDetailsResponse.ProductDetails;
import com.tradebeaseller.utils.Constants;
import com.tradebeaseller.utils.PreferenceConnector;

import java.util.LinkedList;

public class MyProductsFragment extends BaseFragment implements View.OnClickListener, HttpReqResCallBack, CloseCallBack {

    private TextView tvError;
    private RecyclerView rvMyProductDetails;
    private FloatingActionButton fabAddProduct;

    private LinkedList<ProductDetails> listOfProductDetails;

    private int catId = -1;
    private int menuId = -1;
    private int brandId = -1;
    private int subCatId = -1;


    private String token = "";
    private String subSubCatId = "";
    private String searchedText = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_my_products_fragment, container, false);
        initializeUi(view);
        initializeListeners();
        prepareDetails();
        return view;
    }

    private void initializeUi(View view) {
        tvError = view.findViewById(R.id.tvError);
        fabAddProduct = view.findViewById(R.id.fabAddProduct);
        rvMyProductDetails = view.findViewById(R.id.rvMyProductDetails);
        PreferenceConnector.writeBoolean(getActivity(), getString(R.string.refresh_products), false);
    }

    private void initializeListeners() {
        fabAddProduct.setOnClickListener(this);
    }

    private void prepareDetails() {
        if (getActivity() != null) {
            showProgressBar(getActivity());
            token = PreferenceConnector.readString(getActivity(), getString(R.string.user_token), "");
            MyProductDetailsApiCall.serviceCallToGetMyProductDetails(getActivity(), this, null, token, searchedText, menuId, catId, subCatId, subSubCatId, brandId);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean refreshProducts = PreferenceConnector.readBoolean(getActivity(), getString(R.string.refresh_products), false);
        if (refreshProducts) {
            prepareDetails();
            PreferenceConnector.writeBoolean(getActivity(), getString(R.string.refresh_products), false);
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.fabAddProduct:
                goToAddProduct();
                break;
            default:
                break;
        }
    }

    private void goToAddProduct() {
        if (getActivity() != null) {
            Intent addProductIntent = new Intent(getActivity(), AddProductActivity.class);
            startActivity(addProductIntent);
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_MY_PRODUCT_DETAILS:
                if (jsonResponse != null) {
                    DisplayProductDetailsResponse displayProductDetailsResponse = new Gson().fromJson(jsonResponse, DisplayProductDetailsResponse.class);
                    if (displayProductDetailsResponse != null) {
                        boolean status = displayProductDetailsResponse.getStatus();
                        String message = displayProductDetailsResponse.getMessage();
                        if (status) {
                            Data data = displayProductDetailsResponse.getData();
                            if (data != null) {
                                listOfProductDetails = displayProductDetailsResponse.getData().getListOfProductDetails();
                                if (listOfProductDetails != null) {
                                    if (listOfProductDetails.size() != 0) {
                                        listIsFull();
                                        initializeAdapter();
                                    } else {
                                        listIsEmpty();
                                    }
                                } else {
                                    listIsEmpty();
                                }
                            }
                        } else {
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    private void initializeAdapter() {
        MyProductsFragmentAdapter myProductsFragmentAdapter = new MyProductsFragmentAdapter(getActivity(), this, listOfProductDetails);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvMyProductDetails.setLayoutManager(layoutManager);
        rvMyProductDetails.setItemAnimator(new DefaultItemAnimator());
        rvMyProductDetails.setAdapter(myProductsFragmentAdapter);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvMyProductDetails.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvMyProductDetails.setVisibility(View.GONE);
    }

    @Override
    public void close() {
        listIsEmpty();
    }
}
