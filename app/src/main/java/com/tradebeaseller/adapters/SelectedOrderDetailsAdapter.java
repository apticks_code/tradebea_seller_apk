package com.tradebeaseller.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tradebeaseller.R;
import com.tradebeaseller.models.responseModels.selectedOrderDetailsResponse.Option;
import com.tradebeaseller.models.responseModels.selectedOrderDetailsResponse.OptionItem;
import com.tradebeaseller.models.responseModels.selectedOrderDetailsResponse.OrderDetails;
import com.tradebeaseller.models.responseModels.selectedOrderDetailsResponse.SellerVarinat;
import com.tradebeaseller.models.responseModels.selectedOrderDetailsResponse.VariantValue;

import java.util.LinkedList;

public class SelectedOrderDetailsAdapter extends RecyclerView.Adapter<SelectedOrderDetailsAdapter.ViewHolder> {

    private Context context;
    private LinkedList<OrderDetails> listOfOrderDetails;

    public SelectedOrderDetailsAdapter(Context context, LinkedList<OrderDetails> listOfOrderDetails) {
        this.context = context;
        this.listOfOrderDetails = listOfOrderDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_single_order_list_item, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OrderDetails orderDetails = listOfOrderDetails.get(position);
        int quantity = orderDetails.getQty();
        int price = orderDetails.getPrice();
        LinkedList<VariantValue> listOfVariantValueDetails = orderDetails.getVarinat().getListOfVariantValues();
        StringBuilder stringBuilder = new StringBuilder();
        for (int index = 0; index < listOfVariantValueDetails.size(); index++) {
            String optionName = "";
            String optionItemName = "";
            VariantValue variantValue = listOfVariantValueDetails.get(index);
            Option option = variantValue.getOption();
            OptionItem optionItem = variantValue.getOptionItem();
            if(option != null) {
                if (option.getName() != null) {
                    optionName = option.getName();
                }
            }
            if(optionItem != null) {
                if (optionItem.getValue() != null) {
                    optionItemName = optionItem.getValue();
                }
            }
            String name = optionName + " | " + optionItemName;
            stringBuilder.append(name).append("\n");
        }
        SellerVarinat sellerVarinat = orderDetails.getSellerVarinat();
        String productName = orderDetails.getProduct().getName();

        holder.tv_ordered_item.setText(productName);
        //holder.tv_ordered_item_description.setText(stringBuilder.toString());
        holder.tv_ordered_item_quantity.setText("X" + " " + quantity);
        holder.tv_ordered_item_amount.setText(context.getString(R.string.current_currency) + " " + price);
        holder.tv_ordered_item_weight.setText("");
        /*if (sellerVarinat != null) {
            int sku = sellerVarinat.getSku();
            int sellingPrice = sellerVarinat.getSellingPrice();
            holder.tvSKU.setText(String.valueOf(sku));
            holder.tvSellingPrice.setText(context.getString(R.string.current_currency) + " " + String.valueOf(sellingPrice));
        }*/
    }

    @Override
    public int getItemCount() {
        return listOfOrderDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_ordered_item, tv_ordered_item_quantity, tv_ordered_item_description, tv_ordered_item_amount, tv_ordered_item_weight;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_ordered_item_description = itemView.findViewById(R.id.tv_ordered_item_description);
            tv_ordered_item_amount = itemView.findViewById(R.id.tv_ordered_item_amount);
            tv_ordered_item_weight = itemView.findViewById(R.id.tv_ordered_item_weight);
            tv_ordered_item_quantity = itemView.findViewById(R.id.tv_ordered_item_quantity);
            tv_ordered_item = itemView.findViewById(R.id.tv_ordered_item);
        }
    }
}
