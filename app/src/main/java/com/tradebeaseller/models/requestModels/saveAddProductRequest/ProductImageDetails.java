package com.tradebeaseller.models.requestModels.saveAddProductRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductImageDetails {

    @SerializedName("image")
    @Expose
    private String image = "";
    @SerializedName("ext")
    @Expose
    private String ext = "";

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }
}
