package com.tradebeaseller.models.responseModels.editProductResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class EditProductSellerVariantDetails {

    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("variant_id")
    @Expose
    private Integer variantId;
    @SerializedName("sku")
    @Expose
    private Integer sku;
    @SerializedName("selling_price")
    @Expose
    private Integer sellingPrice;
    @SerializedName("seller_mrp")
    @Expose
    private Integer sellerMrp;
    @SerializedName("qty")
    @Expose
    private Integer qty;
    @SerializedName("mrp")
    @Expose
    private Double mrp;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("variants_values")
    @Expose
    private LinkedList<VariantsValue> listOfVariantsValues;


    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVariantId() {
        return variantId;
    }

    public void setVariantId(Integer variantId) {
        this.variantId = variantId;
    }

    public Integer getSku() {
        return sku;
    }

    public void setSku(Integer sku) {
        this.sku = sku;
    }

    public Integer getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(Integer sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public Integer getSellerMrp() {
        return sellerMrp;
    }

    public void setSellerMrp(Integer sellerMrp) {
        this.sellerMrp = sellerMrp;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LinkedList<VariantsValue> getListOfVariantsValues() {
        return listOfVariantsValues;
    }

    public void setListOfVariantsValues(LinkedList<VariantsValue> listOfVariantsValues) {
        this.listOfVariantsValues = listOfVariantsValues;
    }

    public Double getMrp() {
        return mrp;
    }

    public void setMrp(Double mrp) {
        this.mrp = mrp;
    }
}
