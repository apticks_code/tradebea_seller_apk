package com.tradebeaseller.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.tradebeaseller.R;
import com.tradebeaseller.interfaces.CloseCallBack;
import com.tradebeaseller.interfaces.SelectedCategoryCallBack;
import com.tradebeaseller.models.responseModels.categoryDetailsResponse.CategoryDetails;
import com.tradebeaseller.utils.UserData;

import java.util.LinkedList;

public class CategoryDetailsAdapter extends RecyclerView.Adapter<CategoryDetailsAdapter.ViewHolder> implements View.OnClickListener {

    private Context context;
    private LinkedList<CategoryDetails> listOfCategoryDetails;

    private String imageUrl = "";
    private String selectedCategoryId = "";
    private String selectedCategoryName = "";

    private int selectedPosition = -1;

    public CategoryDetailsAdapter(Context context, LinkedList<CategoryDetails> listOfCategoryDetails, String imageUrl, String selectedCategoryId, String selectedCategoryName, TextView tvSave) {
        this.context = context;
        this.imageUrl = imageUrl;
        tvSave.setOnClickListener(this);
        this.selectedCategoryId = selectedCategoryId;
        this.selectedCategoryName = selectedCategoryName;
        this.listOfCategoryDetails = listOfCategoryDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_category_details_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CategoryDetails categoryDetails = listOfCategoryDetails.get(position);
        String name = categoryDetails.getName();
        String id = String.valueOf(categoryDetails.getId());

        holder.tvCategoryName.setText(name);

        if (id.equalsIgnoreCase(selectedCategoryId)) {
            holder.checkBox.setChecked(true);
        } else {
            holder.checkBox.setChecked(false);
        }

        if (imageUrl != null) {
            if (!imageUrl.isEmpty()) {
                Glide.with(context)
                        .load(imageUrl)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .placeholder(R.drawable.ic_user_image)
                        .error(R.drawable.ic_user_image)
                        .into(holder.ivPic);
            }
        }
    }

    @Override
    public int getItemCount() {
        return listOfCategoryDetails.size();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.tvSave:
                prepareSaveDetails();
                break;
            default:
                break;
        }
    }

    private void prepareSaveDetails() {
        Context context = UserData.getInstance().getContext();
        if (context != null) {
            SelectedCategoryCallBack selectedCategoryCallBack = (SelectedCategoryCallBack) context;
            selectedCategoryCallBack.selectedCategoryDetails(selectedCategoryId, selectedCategoryName);
        }

        CloseCallBack closeCallBack = (CloseCallBack) this.context;
        closeCallBack.close();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {

        private ImageView ivPic;
        private CheckBox checkBox;
        private TextView tvCategoryName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivPic = itemView.findViewById(R.id.ivPic);
            checkBox = itemView.findViewById(R.id.checkBox);
            tvCategoryName = itemView.findViewById(R.id.tvCategoryName);
            checkBox.setOnCheckedChangeListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                selectedCategoryId = String.valueOf(listOfCategoryDetails.get(getLayoutPosition()).getId());
                selectedCategoryName = listOfCategoryDetails.get(getLayoutPosition()).getName();
                if (selectedPosition == -1) {
                    selectedPosition = getLayoutPosition();
                } else {
                    if (selectedPosition != getLayoutPosition()) {
                        selectedPosition = getLayoutPosition();
                        android.os.Handler handler = new android.os.Handler();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                notifyDataSetChanged();
                            }
                        });
                    }
                }
            } else {
                if (selectedPosition == getLayoutPosition()) {
                    selectedCategoryId = "";
                    selectedCategoryName = "";
                    selectedPosition = -1;
                }
            }
        }
    }
}
