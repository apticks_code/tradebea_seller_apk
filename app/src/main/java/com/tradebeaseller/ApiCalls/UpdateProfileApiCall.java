package com.tradebeaseller.ApiCalls;

import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.jsonBuilderParser.JsonBuilderParser;
import com.tradebeaseller.models.requestModels.profileUpdateRequest.ProfileUpdateRequest;
import com.tradebeaseller.utils.CommonMethods;
import com.tradebeaseller.utils.Constants;

import org.json.JSONObject;

public class UpdateProfileApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallToUpdateProfile(final Context context, String name, String shopName, String pan, String gst, String designation, String ownerShipType, String email, String mobile, String profileImage, String panImage, String shopImage, String signatureImage, String electricityBillImage, String token, String location, String latitude, String longitude) {
        String url = Constants.PROFILE_UPDATE_URL;
        ProfileUpdateRequest profileUpdateRequest = new ProfileUpdateRequest();
        profileUpdateRequest.firstName = name;
        profileUpdateRequest.shopName = shopName;
        profileUpdateRequest.pan = pan;
        profileUpdateRequest.gst = gst;
        profileUpdateRequest.email = email;
        profileUpdateRequest.mobile = mobile;
        profileUpdateRequest.designation = designation;
        profileUpdateRequest.ownershipType = ownerShipType;
        profileUpdateRequest.panCardImage = panImage;
        profileUpdateRequest.shopImage = shopImage;
        profileUpdateRequest.signatureImage = signatureImage;
        profileUpdateRequest.profileImage = profileImage;
        profileUpdateRequest.electricityBillImage = electricityBillImage;
        profileUpdateRequest.longitude = longitude;
        profileUpdateRequest.latitude = latitude;
        profileUpdateRequest.geoLocationAddress = location;
        profileUpdateRequest.group = "2";
        JSONObject postObject = JsonBuilderParser.jsonBuilder(profileUpdateRequest);
        AndroidNetworking.post(url)
                .addJSONObjectBody(postObject) // posting json
                .setPriority(Priority.HIGH)
                .addHeaders("X_AUTH_TOKEN", token)
                .addHeaders("Content-Type","application/json")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_PROFILE_UPDATE);
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_PROFILE_UPDATE);
                    }
                });
    }
}
