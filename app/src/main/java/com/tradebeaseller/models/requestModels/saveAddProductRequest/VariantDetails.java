package com.tradebeaseller.models.requestModels.saveAddProductRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;
import java.util.List;

public class VariantDetails {

    @SerializedName("mrp")
    @Expose
    private String mrp = "";
    @SerializedName("discount")
    @Expose
    private String discount = "";
    @SerializedName("option_item_ids")
    @Expose
    private LinkedList<String> listOfOptionItemIds;


    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public LinkedList<String> getListOfOptionItemIds() {
        return listOfOptionItemIds;
    }

    public void setListOfOptionItemIds(LinkedList<String> listOfOptionItemIds) {
        this.listOfOptionItemIds = listOfOptionItemIds;
    }
}
