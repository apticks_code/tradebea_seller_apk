package com.tradebeaseller.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tradebeaseller.ApiCalls.OrderDetailsApiCall;
import com.tradebeaseller.R;
import com.tradebeaseller.activities.HomeActivity;
import com.tradebeaseller.activities.OrderFilterDetailsActivity;
import com.tradebeaseller.adapters.OrdersFragmentAdapter;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.interfaces.OrderFilterDetailsCallBack;
import com.tradebeaseller.models.responseModels.orderDetailsResponse.OrderData;
import com.tradebeaseller.models.responseModels.orderDetailsResponse.OrderDetailsResponse;
import com.tradebeaseller.utils.Constants;
import com.tradebeaseller.utils.PreferenceConnector;
import com.tradebeaseller.utils.UserData;

import java.util.LinkedList;
import java.util.Objects;

public class OrdersFragment extends BaseFragment implements HttpReqResCallBack, View.OnClickListener, OrderFilterDetailsCallBack {

    private TextView tvError;
    private ImageView ivFilter;
    private RecyclerView rvOrderDetails;
    private SwipeRefreshLayout swipeRefreshLayout;

    private LinkedList<OrderData> listOfOrderData;

    private String endDate = "";
    private String lastDays = "";
    private String lastYears = "";
    private String startDate = "";
    protected String searchedText = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_order_details, container, false);
        initializeUi(view);
        initializeListeners();
        refreshContent();
        prepareDetails();
        return view;
    }

    private void initializeUi(View view) {
        ivFilter = ((HomeActivity) requireActivity()).ivFilter;
        tvError = view.findViewById(R.id.tvError);
        rvOrderDetails = view.findViewById(R.id.rvOrderDetails);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
    }

    private void initializeListeners() {
        ivFilter.setOnClickListener(null);
        ivFilter.setOnClickListener(this);
    }

    private void prepareDetails() {
        if (getActivity() != null) {
            showProgressBar(getActivity());
            String token = PreferenceConnector.readString(getActivity(), getString(R.string.user_token), "");
            OrderDetailsApiCall.serviceCallForOrderDetails(getActivity(), this, null, startDate, endDate, lastDays, lastYears, token);
        }
    }

    private void refreshContent() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (getActivity() != null) {
                    swipeRefreshLayout.setRefreshing(false);
                    prepareDetails();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        UserData.getInstance().setOrdersFragment(this);
        if (getActivity() != null) {
            boolean orderStatusChanged = PreferenceConnector.readBoolean(getActivity(), getString(R.string.order_status_changed), false);
            if (orderStatusChanged) {
                PreferenceConnector.writeBoolean(getActivity(), getString(R.string.order_status_changed), false);
                prepareDetails();
            }
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_TO_GET_ORDER_DETAILS) {
            if (jsonResponse != null) {
                OrderDetailsResponse orderDetailsResponse = new Gson().fromJson(jsonResponse, OrderDetailsResponse.class);
                if (orderDetailsResponse != null) {
                    boolean status = orderDetailsResponse.getStatus();
                    String message = orderDetailsResponse.getMessage();
                    if (status) {
                        listOfOrderData = orderDetailsResponse.getListOfOrderData();
                        if (listOfOrderData != null) {
                            if (listOfOrderData.size() != 0) {
                                listIsFull();
                                initializeAdapter();
                            } else {
                                listIsEmpty();
                            }
                        } else {
                            listIsEmpty();
                        }
                    } else {
                        Toast.makeText(getActivity(), Html.fromHtml(message), Toast.LENGTH_SHORT).show();
                    }
                }
            }
            closeProgressbar();
        }
    }

    private void initializeAdapter() {
        OrdersFragmentAdapter ordersFragmentAdapter = new OrdersFragmentAdapter(getActivity(), listOfOrderData);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvOrderDetails.setLayoutManager(layoutManager);
        rvOrderDetails.setItemAnimator(new DefaultItemAnimator());
        rvOrderDetails.setAdapter(ordersFragmentAdapter);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvOrderDetails.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvOrderDetails.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivFilter) {
            prepareFilterDetails();
        }
    }

    private void prepareFilterDetails() {
        if (getActivity() != null) {
            Intent orderFilterDetailsIntent = new Intent(getActivity(), OrderFilterDetailsActivity.class);
            startActivity(orderFilterDetailsIntent);
        }
    }

    @Override
    public void orderFilterDetails(String searchedText, String startDate, String endDate) {
        this.searchedText = searchedText;
        this.startDate = startDate;
        this.endDate = endDate;
        prepareDetails();
    }
}
