package com.tradebeaseller.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.tradebeaseller.R;
import com.tradebeaseller.interfaces.CloseCallBack;
import com.tradebeaseller.interfaces.SelectedMenuDetailsCallBack;
import com.tradebeaseller.models.responseModels.menuDetailsResponse.MenuDetails;
import com.tradebeaseller.utils.UserData;

import java.util.LinkedList;

public class MenuDetailsAdapter extends RecyclerView.Adapter<MenuDetailsAdapter.ViewHolder> implements View.OnClickListener {

    private Context context;
    private LinkedList<MenuDetails> listOfMenuDetails;


    private String selectedMenuId = "";
    private String selectedMenuName = "";
    private int selectedPosition = -1;

    public MenuDetailsAdapter(Context context, LinkedList<MenuDetails> listOfMenuDetails, String selectedMenuId, String selectedMenuName, TextView tvSave) {
        this.context = context;
        tvSave.setOnClickListener(this);
        this.selectedMenuId = selectedMenuId;
        this.selectedMenuName = selectedMenuName;
        this.listOfMenuDetails = listOfMenuDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_menu_details_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MenuDetails menuDetails = listOfMenuDetails.get(position);
        String name = menuDetails.getName();
        String imageUrl = menuDetails.getImageUrl();
        String id = String.valueOf(menuDetails.getId());

        holder.tvMenuName.setText(name);

        if (id.equalsIgnoreCase(selectedMenuId)) {
            holder.checkBox.setChecked(true);
        } else {
            holder.checkBox.setChecked(false);
        }

        if (imageUrl != null) {
            if (!imageUrl.isEmpty()) {
                Glide.with(context)
                        .load(imageUrl)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .placeholder(R.drawable.ic_user_image)
                        .error(R.drawable.ic_user_image)
                        .into(holder.ivPic);
            }
        }
    }

    @Override
    public int getItemCount() {
        return listOfMenuDetails.size();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.tvSave:
                prepareSaveDetails();
                break;
            default:
                break;
        }
    }

    private void prepareSaveDetails() {
        Context context = UserData.getInstance().getContext();
        if (context != null) {
            SelectedMenuDetailsCallBack selectedMenuDetailsCallBack = (SelectedMenuDetailsCallBack) context;
            selectedMenuDetailsCallBack.selectedMenuDetails(selectedMenuId, selectedMenuName);
        }

        CloseCallBack closeCallBack = (CloseCallBack) this.context;
        closeCallBack.close();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {

        private ImageView ivPic;
        private CheckBox checkBox;
        private TextView tvMenuName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivPic = itemView.findViewById(R.id.ivPic);
            checkBox = itemView.findViewById(R.id.checkBox);
            tvMenuName = itemView.findViewById(R.id.tvMenuName);
            checkBox.setOnCheckedChangeListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                selectedMenuId = String.valueOf(listOfMenuDetails.get(getLayoutPosition()).getId());
                selectedMenuName = listOfMenuDetails.get(getLayoutPosition()).getName();
                if (selectedPosition == -1) {
                    selectedPosition = getLayoutPosition();
                } else {
                    if (selectedPosition != getLayoutPosition()) {
                        selectedPosition = getLayoutPosition();
                        android.os.Handler handler = new android.os.Handler();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                notifyDataSetChanged();
                            }
                        });
                    }
                }
            } else {
                if (selectedPosition == getLayoutPosition()) {
                    selectedMenuId = "";
                    selectedMenuName = "";
                    selectedPosition = -1;
                }
            }
        }
    }
}
