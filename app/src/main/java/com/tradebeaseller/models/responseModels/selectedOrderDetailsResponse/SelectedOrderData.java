package com.tradebeaseller.models.responseModels.selectedOrderDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class SelectedOrderData {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("track_id")
    @Expose
    private String trackId;
    @SerializedName("order_pickup_otp")
    @Expose
    private String orderPickupOtp;
    @SerializedName("order_delivery_otp")
    @Expose
    private Integer orderDeliveryOtp;
    @SerializedName("delivery_address_id")
    @Expose
    private Integer deliveryAddressId;
    @SerializedName("invoice_id")
    @Expose
    private Integer invoiceId;
    @SerializedName("payment_method_id")
    @Expose
    private Integer paymentMethodId;
    @SerializedName("payment_id")
    @Expose
    private Integer paymentId;
    @SerializedName("promo_id")
    @Expose
    private Object promoId;
    @SerializedName("delivery_fee")
    @Expose
    private Integer deliveryFee;
    @SerializedName("used_wallet_amount")
    @Expose
    private Integer usedWalletAmount;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("message")
    @Expose
    private Object message;
    @SerializedName("preparation_time")
    @Expose
    private String preparationTime;
    @SerializedName("created_user_id")
    @Expose
    private Integer createdUserId;
    @SerializedName("seller_user_id")
    @Expose
    private Integer sellerUserId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("order_status")
    @Expose
    private Integer orderStatus;
    @SerializedName("order_details")
    @Expose
    private LinkedList<OrderDetails> listOfOrderDetails = null;
    @SerializedName("delivery_jobs")
    @Expose
    private LinkedList<DeliveryJobs> listOfDeliveryJobs = null;
    @SerializedName("invoice")
    @Expose
    private Invoice invoice;
    @SerializedName("return_request")
    @Expose
    private ReturnRequest returnRequest;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("customer_app_statuses")
    @Expose
    private LinkedList<CustomerAppStatus> listOfCustomerAppStatus = null;
    @SerializedName("customer")
    @Expose
    private Customer customer;
    @SerializedName("seller")
    @Expose
    private Seller seller;
    @SerializedName("delivery_boy")
    @Expose
    private DeliveryBoy deliveryBoy;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getOrderPickupOtp() {
        return orderPickupOtp;
    }

    public void setOrderPickupOtp(String orderPickupOtp) {
        this.orderPickupOtp = orderPickupOtp;
    }

    public Integer getOrderDeliveryOtp() {
        return orderDeliveryOtp;
    }

    public void setOrderDeliveryOtp(Integer orderDeliveryOtp) {
        this.orderDeliveryOtp = orderDeliveryOtp;
    }

    public Integer getDeliveryAddressId() {
        return deliveryAddressId;
    }

    public void setDeliveryAddressId(Integer deliveryAddressId) {
        this.deliveryAddressId = deliveryAddressId;
    }

    public Integer getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Integer invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Integer getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Integer paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    public Object getPromoId() {
        return promoId;
    }

    public void setPromoId(Object promoId) {
        this.promoId = promoId;
    }

    public Integer getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(Integer deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public Integer getUsedWalletAmount() {
        return usedWalletAmount;
    }

    public void setUsedWalletAmount(Integer usedWalletAmount) {
        this.usedWalletAmount = usedWalletAmount;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public String getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(String preparationTime) {
        this.preparationTime = preparationTime;
    }

    public Integer getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(Integer createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Integer getSellerUserId() {
        return sellerUserId;
    }

    public void setSellerUserId(Integer sellerUserId) {
        this.sellerUserId = sellerUserId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public LinkedList<OrderDetails> getListOfOrderDetails() {
        return listOfOrderDetails;
    }

    public void setListOfOrderDetails(LinkedList<OrderDetails> listOfOrderDetails) {
        this.listOfOrderDetails = listOfOrderDetails;
    }

    public LinkedList<DeliveryJobs> getListOfDeliveryJobs() {
        return listOfDeliveryJobs;
    }

    public void setListOfDeliveryJobs(LinkedList<DeliveryJobs> listOfDeliveryJobs) {
        this.listOfDeliveryJobs = listOfDeliveryJobs;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public LinkedList<CustomerAppStatus> getListOfCustomerAppStatus() {
        return listOfCustomerAppStatus;
    }

    public void setListOfCustomerAppStatus(LinkedList<CustomerAppStatus> listOfCustomerAppStatus) {
        this.listOfCustomerAppStatus = listOfCustomerAppStatus;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public DeliveryBoy getDeliveryBoy() {
        return deliveryBoy;
    }

    public void setDeliveryBoy(DeliveryBoy deliveryBoy) {
        this.deliveryBoy = deliveryBoy;
    }

    public ReturnRequest getReturnRequest() {
        return returnRequest;
    }

    public void setReturnRequest(ReturnRequest returnRequest) {
        this.returnRequest = returnRequest;
    }
}
