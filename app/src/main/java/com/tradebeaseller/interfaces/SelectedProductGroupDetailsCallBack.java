package com.tradebeaseller.interfaces;

public interface SelectedProductGroupDetailsCallBack {
    void selectedProductGroupDetails(String selectedProductGroupId, String selectedProductGroupName);
}
