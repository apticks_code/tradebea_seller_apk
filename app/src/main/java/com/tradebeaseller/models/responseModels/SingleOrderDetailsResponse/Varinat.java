
package com.tradebeaseller.models.responseModels.SingleOrderDetailsResponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Varinat {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("variant_code")
    @Expose
    private Integer variantCode;
    @SerializedName("mrp")
    @Expose
    private Integer mrp;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("variant_values")
    @Expose
    private List<VariantValue> variantValues = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getVariantCode() {
        return variantCode;
    }

    public void setVariantCode(Integer variantCode) {
        this.variantCode = variantCode;
    }

    public Integer getMrp() {
        return mrp;
    }

    public void setMrp(Integer mrp) {
        this.mrp = mrp;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public List<VariantValue> getVariantValues() {
        return variantValues;
    }

    public void setVariantValues(List<VariantValue> variantValues) {
        this.variantValues = variantValues;
    }

}
