package com.tradebeaseller.ApiCalls;

import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.jsonBuilderParser.JsonBuilderParser;
import com.tradebeaseller.models.requestModels.returnAcceptOrderRequest.ReturnAcceptOrderRequest;
import com.tradebeaseller.utils.CommonMethods;
import com.tradebeaseller.utils.Constants;

import org.json.JSONObject;

public class AcceptReturnOrderApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallForAcceptReturnOrder(Context context, String token, ReturnAcceptOrderRequest returnAcceptOrderRequest) {
        String url = Constants.ACCEPT_RETURN_ORDER;
        JSONObject postObject = JsonBuilderParser.jsonBuilder(returnAcceptOrderRequest);
        AndroidNetworking.patch(url)
                .addJSONObjectBody(postObject)
                .setPriority(Priority.HIGH)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("X_AUTH_TOKEN", token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_ACCEPT_RETURN_ORDER);
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_ACCEPT_RETURN_ORDER);
                    }
                });
    }
}
