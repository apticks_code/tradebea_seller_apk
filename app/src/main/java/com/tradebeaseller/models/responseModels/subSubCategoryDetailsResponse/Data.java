package com.tradebeaseller.models.responseModels.subSubCategoryDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class Data {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("ext")
    @Expose
    private String ext;
    @SerializedName("sub_sub_categories")
    @Expose
    private LinkedList<SubSubCategoryDetails> listOfSubSubCategoryDetails;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public LinkedList<SubSubCategoryDetails> getListOfSubSubCategoryDetails() {
        return listOfSubSubCategoryDetails;
    }

    public void setListOfSubSubCategoryDetails(LinkedList<SubSubCategoryDetails> listOfSubSubCategoryDetails) {
        this.listOfSubSubCategoryDetails = listOfSubSubCategoryDetails;
    }
}
