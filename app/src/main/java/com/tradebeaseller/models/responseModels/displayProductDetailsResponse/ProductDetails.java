package com.tradebeaseller.models.responseModels.displayProductDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class ProductDetails {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("product_code")
    @Expose
    private String productCode;
    @SerializedName("desc")
    @Expose
    private Object desc;
    @SerializedName("menu_id")
    @Expose
    private Integer menuId;
    @SerializedName("cat_id")
    @Expose
    private Integer catId;
    @SerializedName("sub_cat_id")
    @Expose
    private Object subCatId;
    @SerializedName("sub_sub_cat_id")
    @Expose
    private Object subSubCatId;
    @SerializedName("brand_id")
    @Expose
    private Integer brandId;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("sounds_like")
    @Expose
    private String soundsLike;
    @SerializedName("created_user_id")
    @Expose
    private Integer createdUserId;
    @SerializedName("updated_user_id")
    @Expose
    private Object updatedUserId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private Object deletedAt;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("menu")
    @Expose
    private MenuDetails menuDetails;
    @SerializedName("category")
    @Expose
    private CategoryDetails categoryDetails;
    @SerializedName("sub_category")
    @Expose
    private SubCategoryDetails subCategoryDetails;
    @SerializedName("brand")
    @Expose
    private BrandDetails brandDetails;
    @SerializedName("product_images")
    @Expose
    private LinkedList<ProductImageDetails> listOfProductImages;
    @SerializedName("sub_sub_category")
    @Expose
    private Object subSubCategory;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Object getDesc() {
        return desc;
    }

    public void setDesc(Object desc) {
        this.desc = desc;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Object getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(Object subCatId) {
        this.subCatId = subCatId;
    }

    public Object getSubSubCatId() {
        return subSubCatId;
    }

    public void setSubSubCatId(Object subSubCatId) {
        this.subSubCatId = subSubCatId;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getSoundsLike() {
        return soundsLike;
    }

    public void setSoundsLike(String soundsLike) {
        this.soundsLike = soundsLike;
    }

    public Integer getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(Integer createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Object getUpdatedUserId() {
        return updatedUserId;
    }

    public void setUpdatedUserId(Object updatedUserId) {
        this.updatedUserId = updatedUserId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public MenuDetails getMenuDetails() {
        return menuDetails;
    }

    public void setMenuDetails(MenuDetails menuDetails) {
        this.menuDetails = menuDetails;
    }

    public CategoryDetails getCategoryDetails() {
        return categoryDetails;
    }

    public void setCategoryDetails(CategoryDetails categoryDetails) {
        this.categoryDetails = categoryDetails;
    }

    public BrandDetails getBrandDetails() {
        return brandDetails;
    }

    public void setBrandDetails(BrandDetails brandDetails) {
        this.brandDetails = brandDetails;
    }

    public LinkedList<ProductImageDetails> getListOfProductImages() {
        return listOfProductImages;
    }

    public void setListOfProductImages(LinkedList<ProductImageDetails> listOfProductImages) {
        this.listOfProductImages = listOfProductImages;
    }

    public Object getSubSubCategory() {
        return subSubCategory;
    }

    public void setSubSubCategory(Object subSubCategory) {
        this.subSubCategory = subSubCategory;
    }

    public SubCategoryDetails getSubCategoryDetails() {
        return subCategoryDetails;
    }

    public void setSubCategoryDetails(SubCategoryDetails subCategoryDetails) {
        this.subCategoryDetails = subCategoryDetails;
    }
}
