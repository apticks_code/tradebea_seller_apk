package com.tradebeaseller.interfaces;

public interface SelectedCategoryCallBack {
    void selectedCategoryDetails(String selectedCategoryId, String selectedCategoryName);
}
