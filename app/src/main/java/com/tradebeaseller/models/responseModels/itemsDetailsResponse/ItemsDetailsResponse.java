package com.tradebeaseller.models.responseModels.itemsDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class ItemsDetailsResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("http_code")
    @Expose
    private Integer httpCode;
    @SerializedName("message")
    @Expose
    private String message = "";
    @SerializedName("data")
    @Expose
    private LinkedList<ItemDetails> listOfItemDetails;


    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(Integer httpCode) {
        this.httpCode = httpCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LinkedList<ItemDetails> getListOfItemDetails() {
        return listOfItemDetails;
    }

    public void setListOfItemDetails(LinkedList<ItemDetails> listOfItemDetails) {
        this.listOfItemDetails = listOfItemDetails;
    }
}
