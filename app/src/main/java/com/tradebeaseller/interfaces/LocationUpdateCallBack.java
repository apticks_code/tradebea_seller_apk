package com.tradebeaseller.interfaces;

public interface LocationUpdateCallBack {
    void OnLatLongReceived(double latitude, double longitude);
}
