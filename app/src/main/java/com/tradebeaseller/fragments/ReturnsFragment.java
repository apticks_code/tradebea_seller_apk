package com.tradebeaseller.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tradebeaseller.ApiCalls.ReturnOrderApiCall;
import com.tradebeaseller.R;
import com.tradebeaseller.activities.HomeActivity;
import com.tradebeaseller.activities.OrderFilterDetailsActivity;
import com.tradebeaseller.adapters.ReturnOrderFragmentAdapter;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.interfaces.OrderFilterDetailsCallBack;
import com.tradebeaseller.models.responseModels.returnOrderDetailsResponse.ReturnOrderData;
import com.tradebeaseller.models.responseModels.returnOrderDetailsResponse.ReturnOrderDetailsResponse;
import com.tradebeaseller.utils.Constants;
import com.tradebeaseller.utils.PreferenceConnector;
import com.tradebeaseller.utils.UserData;

import java.util.LinkedList;

public class ReturnsFragment extends BaseFragment implements View.OnClickListener, HttpReqResCallBack, OrderFilterDetailsCallBack {

    private TextView tvError;
    private ImageView ivFilter;
    private RecyclerView rvReturnOrderDetails;
    private SwipeRefreshLayout swipeRefreshLayout;

    private LinkedList<ReturnOrderData> listOfReturnOrderData;

    private String endDate = "";
    private String lastDays = "";
    private String lastYears = "";
    private String startDate = "";
    protected String searchedText = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_return_order_details, container, false);
        initializeUi(view);
        initializeListeners();
        refreshContent();
        prepareDetails();
        return view;
    }

    private void initializeUi(View view) {
        tvError = view.findViewById(R.id.tvError);
        ivFilter = view.findViewById(R.id.ivFilter);
        ivFilter = ((HomeActivity) requireActivity()).ivFilter;
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        rvReturnOrderDetails = view.findViewById(R.id.rvReturnOrderDetails);
    }

    private void initializeListeners() {
        ivFilter.setOnClickListener(null);
        ivFilter.setOnClickListener(this);
    }

    private void refreshContent() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (getActivity() != null) {
                    swipeRefreshLayout.setRefreshing(false);
                    prepareDetails();
                }
            }
        });
    }

    private void prepareDetails() {
        if (getActivity() != null) {
            showProgressBar(getActivity());
            String token = PreferenceConnector.readString(getActivity(), getString(R.string.user_token), "");
            ReturnOrderApiCall.serviceCallForReturnOrder(getActivity(), this, null, startDate, endDate, lastDays, lastYears, token);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        UserData.getInstance().setOrdersFragment(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivFilter) {
            prepareFilterDetails();
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_TO_GET_RETURN_ORDER_DETAILS) {
            if (jsonResponse != null) {
                ReturnOrderDetailsResponse returnOrderDetailsResponse = new Gson().fromJson(jsonResponse, ReturnOrderDetailsResponse.class);
                if (returnOrderDetailsResponse != null) {
                    boolean status = returnOrderDetailsResponse.getStatus();
                    String message = returnOrderDetailsResponse.getMessage();
                    listOfReturnOrderData = returnOrderDetailsResponse.getListOfReturnOrderData();
                    if (status) {
                        if (listOfReturnOrderData != null) {
                            if (listOfReturnOrderData.size() != 0) {
                                listIsFull();
                                initializeAdapter();
                            } else {
                                listIsEmpty();
                            }
                        } else {
                            listIsEmpty();
                        }
                    } else {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
            closeProgressbar();
        }
    }

    private void initializeAdapter() {
        ReturnOrderFragmentAdapter returnOrderFragmentAdapter = new ReturnOrderFragmentAdapter(getActivity(), listOfReturnOrderData);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvReturnOrderDetails.setLayoutManager(layoutManager);
        rvReturnOrderDetails.setItemAnimator(new DefaultItemAnimator());
        rvReturnOrderDetails.setAdapter(returnOrderFragmentAdapter);
    }

    private void prepareFilterDetails() {
        if (getActivity() != null) {
            Intent orderFilterDetailsIntent = new Intent(getActivity(), OrderFilterDetailsActivity.class);
            startActivity(orderFilterDetailsIntent);
        }
    }

    @Override
    public void orderFilterDetails(String searchedText, String startDate, String endDate) {
        this.searchedText = searchedText;
        this.startDate = startDate;
        this.endDate = endDate;
        prepareDetails();
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvReturnOrderDetails.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvReturnOrderDetails.setVisibility(View.GONE);
    }
}
