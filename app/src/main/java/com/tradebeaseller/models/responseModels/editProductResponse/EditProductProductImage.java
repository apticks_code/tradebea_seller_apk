package com.tradebeaseller.models.responseModels.editProductResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EditProductProductImage {

    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("ext")
    @Expose
    private String ext;
    @SerializedName("image")
    @Expose
    private String imageUrl = "";


    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
