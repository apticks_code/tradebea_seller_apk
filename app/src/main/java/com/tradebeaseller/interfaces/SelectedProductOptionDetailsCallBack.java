package com.tradebeaseller.interfaces;

public interface SelectedProductOptionDetailsCallBack {
    void selectedProductOptionDetails(String selectedProductOptionId,String selectedProductOptionName);
}
