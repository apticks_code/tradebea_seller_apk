package com.tradebeaseller.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tradebeaseller.R;
import com.tradebeaseller.activities.SelectedOrderDetailsActivity;
import com.tradebeaseller.activities.SelectedReturnOrderDetailsActivity;
import com.tradebeaseller.models.responseModels.returnOrderDetailsResponse.ReturnOrderData;
import com.tradebeaseller.models.responseModels.returnOrderDetailsResponse.Status;

import java.util.LinkedList;

public class ReturnOrderFragmentAdapter extends RecyclerView.Adapter<ReturnOrderFragmentAdapter.ViewHolder> {

    private Context context;
    private LinkedList<ReturnOrderData> listOfReturnOrderData;

    public ReturnOrderFragmentAdapter(Context context, LinkedList<ReturnOrderData> listOfReturnOrderData) {
        this.context = context;
        this.listOfReturnOrderData = listOfReturnOrderData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_return_details_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ReturnOrderData returnOrderData = listOfReturnOrderData.get(position);
        if (returnOrderData != null) {
            String orderTrackId = returnOrderData.getOrderTrackId();
            String date = returnOrderData.getDate();
            String time = returnOrderData.getTime();
            String sellerUniqueId = returnOrderData.getSellerUniqueId();
            Status status = returnOrderData.getStatus();


            if (status != null) {
                String statusName = status.getStatusName();
                holder.tvStatus.setText("Status :" + " " + statusName);
            } else {
                holder.tvStatus.setText("Status :" + " " + "N/A");
            }

            holder.tvOrderId.setText(orderTrackId);
            holder.tvDate.setText(date);
            holder.tvTime.setText(time);
            holder.tvCustomerId.setText(sellerUniqueId);
            if (returnOrderData.getOrderReturnOtp() != null) {
                int orderReturnOTP = returnOrderData.getOrderReturnOtp();
                holder.tvOTP.setText("Return OTP : " + orderReturnOTP);
            }else {
                holder.tvOTP.setText("Return OTP : " + "NA");
            }
            if (returnOrderData.getOrderPickupOtp() != null) {
                int orderPickupOtp = returnOrderData.getOrderPickupOtp();
                Log.d("orderPickupOtp***", orderPickupOtp + "");
            }
        }
    }

    @Override
    public int getItemCount() {
        return listOfReturnOrderData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvOrderId, tvOrderStatus, tvDate, tvOTP, tvCustomerId, tvTime, tvStatus;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvOTP = itemView.findViewById(R.id.tvOTP);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            tvOrderId = itemView.findViewById(R.id.tvOrderId);
            tvCustomerId = itemView.findViewById(R.id.tvCustomerId);
            tvOrderStatus = itemView.findViewById(R.id.tvOrderStatus);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            ReturnOrderData returnOrderData = listOfReturnOrderData.get(getLayoutPosition());
            if (returnOrderData != null) {
                Intent selectedReturnOrderDetailsIntent = new Intent(context, SelectedReturnOrderDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(context.getString(R.string.order_id), returnOrderData.getOrderId().toString());
                selectedReturnOrderDetailsIntent.putExtras(bundle);
                context.startActivity(selectedReturnOrderDetailsIntent);
            }
        }
    }
}
