package com.tradebeaseller.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.tradebeaseller.ApiCalls.CategoryDetailsApiCall;
import com.tradebeaseller.R;
import com.tradebeaseller.adapters.CategoryDetailsAdapter;
import com.tradebeaseller.interfaces.CloseCallBack;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.models.responseModels.categoryDetailsResponse.CategoryDetails;
import com.tradebeaseller.models.responseModels.categoryDetailsResponse.CategoryDetailsResponse;
import com.tradebeaseller.models.responseModels.categoryDetailsResponse.SelectedMenuData;
import com.tradebeaseller.utils.Constants;

import java.util.LinkedList;

public class CategoryDetailsActivity extends BaseActivity implements HttpReqResCallBack, View.OnClickListener, CloseCallBack {

    private ImageView ivBackArrow;
    private TextView tvSave, tvError;
    private RecyclerView rvCategoryDetails;

    private LinkedList<CategoryDetails> listOfCategoryDetails;

    private String imageUrl = "";
    private String selectedMenuId = "";
    private String selectedCategoryId = "";
    private String selectedCategoryName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_category_details);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.selected_menu_id)))
                selectedMenuId = bundle.getString(getString(R.string.selected_menu_id), "");
            if (bundle.containsKey(getString(R.string.selected_category_id)))
                selectedCategoryId = bundle.getString(getString(R.string.selected_category_id), "");
            if (bundle.containsKey(getString(R.string.selected_category_name)))
                selectedCategoryName = bundle.getString(getString(R.string.selected_category_name), "");
        }
    }

    private void initializeUi() {
        tvSave = findViewById(R.id.tvSave);
        tvError = findViewById(R.id.tvError);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        rvCategoryDetails = findViewById(R.id.rvCategoryDetails);
    }

    private void initializeListeners() {
        tvSave.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareDetails() {
        showProgressBar(this);
        CategoryDetailsApiCall.serviceCallForCategoryDetails(this, null, null, selectedMenuId);
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_CATEGORY_DETAILS:
                if (jsonResponse != null) {
                    CategoryDetailsResponse categoryDetailsResponse = new Gson().fromJson(jsonResponse, CategoryDetailsResponse.class);
                    if (categoryDetailsResponse != null) {
                        boolean status = categoryDetailsResponse.getStatus();
                        if (status) {
                            SelectedMenuData selectedMenuData = categoryDetailsResponse.getSelectedMenuData();
                            if (selectedMenuData != null) {
                                imageUrl = selectedMenuData.getImageUrl();
                                listOfCategoryDetails = selectedMenuData.getListOfCategoryDetails();
                                if (listOfCategoryDetails != null) {
                                    if (listOfCategoryDetails.size() != 0) {
                                        listIsFull();
                                        initializeAdapter();
                                    } else {
                                        listIsEmpty();
                                    }
                                } else {
                                    listIsEmpty();
                                }
                            }
                        }
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    private void initializeAdapter() {
        CategoryDetailsAdapter categoryDetailsAdapter = new CategoryDetailsAdapter(this, listOfCategoryDetails, imageUrl, selectedCategoryId, selectedCategoryName, tvSave);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvCategoryDetails.setLayoutManager(layoutManager);
        rvCategoryDetails.setItemAnimator(new DefaultItemAnimator());
        rvCategoryDetails.setAdapter(categoryDetailsAdapter);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvCategoryDetails.setVisibility(View.GONE);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvCategoryDetails.setVisibility(View.VISIBLE);
    }

    @Override
    public void close() {
        finish();
    }
}
