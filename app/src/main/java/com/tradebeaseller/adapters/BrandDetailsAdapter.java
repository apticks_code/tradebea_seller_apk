package com.tradebeaseller.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.tradebeaseller.R;
import com.tradebeaseller.activities.BrandDetailsActivity;
import com.tradebeaseller.interfaces.CloseCallBack;
import com.tradebeaseller.interfaces.SelectedBrandDetailsCallBack;
import com.tradebeaseller.models.responseModels.brandDetailsResponse.BrandDetails;
import com.tradebeaseller.utils.UserData;

import java.util.LinkedList;

public class BrandDetailsAdapter extends RecyclerView.Adapter<BrandDetailsAdapter.ViewHolder> implements View.OnClickListener {

    private Context context;
    private LinkedList<BrandDetails> listOfBrandDetails;

    private String selectedBrandId = "";
    private String selectedBrandName = "";
    private int selectedPosition = -1;

    public BrandDetailsAdapter(Context context, LinkedList<BrandDetails> listOfBrandDetails, String selectedBrandId, String selectedBrandName, TextView tvSave) {
        this.context = context;
        tvSave.setOnClickListener(this);
        this.selectedBrandId = selectedBrandId;
        this.selectedBrandName = selectedBrandName;
        this.listOfBrandDetails = listOfBrandDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_brand_details_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BrandDetails brandDetails = listOfBrandDetails.get(position);

        String name = brandDetails.getName();
        String imageUrl = brandDetails.getImageUrl();
        String id = String.valueOf(brandDetails.getId());

        holder.tvBrandName.setText(name);

        if (id.equalsIgnoreCase(selectedBrandId)) {
            holder.checkBox.setChecked(true);
        } else {
            holder.checkBox.setChecked(false);
        }

        if (imageUrl != null) {
            if (!imageUrl.isEmpty()) {
                Glide.with(context)
                        .load(imageUrl)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .placeholder(R.drawable.ic_user_image)
                        .error(R.drawable.ic_user_image)
                        .into(holder.ivPic);
            }
        }
    }

    @Override
    public int getItemCount() {
        return listOfBrandDetails.size();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.tvSave:
                prepareSaveDetails();
                break;
            default:
                break;
        }
    }

    private void prepareSaveDetails() {
        Context context = UserData.getInstance().getContext();
        if (context != null) {
            SelectedBrandDetailsCallBack selectedBrandDetailsCallBack = (SelectedBrandDetailsCallBack) context;
            selectedBrandDetailsCallBack.selectedBrandDetails(selectedBrandId, selectedBrandName);
        }

        CloseCallBack closeCallBack = (CloseCallBack) this.context;
        closeCallBack.close();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {

        private ImageView ivPic;
        private CheckBox checkBox;
        private TextView tvBrandName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivPic = itemView.findViewById(R.id.ivPic);
            checkBox = itemView.findViewById(R.id.checkBox);
            tvBrandName = itemView.findViewById(R.id.tvBrandName);
            checkBox.setOnCheckedChangeListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                selectedBrandId = String.valueOf(listOfBrandDetails.get(getLayoutPosition()).getId());
                selectedBrandName = listOfBrandDetails.get(getLayoutPosition()).getName();
                if (selectedPosition == -1) {
                    selectedPosition = getLayoutPosition();
                } else {
                    if (selectedPosition != getLayoutPosition()) {
                        selectedPosition = getLayoutPosition();
                        android.os.Handler handler = new android.os.Handler();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                notifyDataSetChanged();
                            }
                        });
                    }
                }
            } else {
                if (selectedPosition == getLayoutPosition()) {
                    selectedBrandId = "";
                    selectedBrandName = "";
                    selectedPosition = -1;
                }
            }
        }
    }
}
