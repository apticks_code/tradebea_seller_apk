package com.tradebeaseller.interfaces;

public interface SelectedItemsDetailsCallBack {
    void selectedItemsDetails(String selectedItemIds, String selectedItemNames);
}
