package com.tradebeaseller.interfaces;

public interface SelectedSubCategoryCallBack {
    void selectedSubCategoryDetails(String selectedSubCategoryId, String selectedSubCategoryName);
}
