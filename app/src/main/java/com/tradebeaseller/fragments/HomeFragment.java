package com.tradebeaseller.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.tradebeaseller.ApiCalls.CatalogProductDetailsApiCall;
import com.tradebeaseller.ApiCalls.DashboardDataApiCall;
import com.tradebeaseller.ApiCalls.DeviceTokenApiCall;
import com.tradebeaseller.R;
import com.tradebeaseller.interfaces.CloseCallBack;
import com.tradebeaseller.interfaces.HomeNavigationCallBack;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.models.responseModels.dashboardResponse.DashboardDataModel;
import com.tradebeaseller.models.responseModels.dashboardResponse.Dashboarddata;
import com.tradebeaseller.models.responseModels.deviceTokenResponse.DeviceTokenResponse;
import com.tradebeaseller.utils.Constants;
import com.tradebeaseller.utils.PreferenceConnector;

public class HomeFragment extends BaseFragment implements View.OnClickListener, HttpReqResCallBack, CloseCallBack {

    private String token = "";
    private TextView tv_received_orders, tv_accepted_orders, tv_completed_orders,
            tv_active_products, tv_approved_products, tv_pending_products,
            tv_next_settlement_date, tv_wallet_money;
    private ConstraintLayout cl_orders, cl_products, cl_payments;
    private View root;
    private String TAG = "HOME_FRAGMENT_DASHBOARD";
    Fragment fragment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.layout_home_fragment, container, false);
        initView();
        preparePushNotificationDetails();
        prepareDashboardDetails();
        return root;
    }

    private void preparePushNotificationDetails() {
        token = PreferenceConnector.readString(getActivity(), getString(R.string.user_token), "");
        String deviceToken = PreferenceConnector.readString(getActivity(), getString(R.string.device_token), "");
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (!deviceToken.isEmpty()) {
                    DeviceTokenApiCall.serviceCallForDeviceToken(getActivity(), HomeFragment.this, null, deviceToken, token);
                }
            }
        }).start();
    }

    private void prepareDashboardDetails() {
        if (getActivity() != null) {
            showProgressBar(getActivity());
            DashboardDataApiCall.serviceCallForDashboardData(getActivity(), this, null, token);
        }
    }

    private void initView() {
        token = PreferenceConnector.readString(getActivity(), getString(R.string.user_token), "");
        tv_received_orders = root.findViewById(R.id.tv_received_orders);
        tv_accepted_orders = root.findViewById(R.id.tv_accepted_orders);
        tv_completed_orders = root.findViewById(R.id.tv_completed_orders);
        tv_active_products = root.findViewById(R.id.tv_active_products);
        tv_approved_products = root.findViewById(R.id.tv_approved_products);
        tv_pending_products = root.findViewById(R.id.tv_pending_products);
        tv_next_settlement_date = root.findViewById(R.id.tv_next_settlement_date);
        tv_wallet_money = root.findViewById(R.id.tv_wallet_money);
        cl_orders = root.findViewById(R.id.cl_orders);
        cl_products = root.findViewById(R.id.cl_products);
        cl_payments = root.findViewById(R.id.cl_payments);
        cl_orders.setOnClickListener(this);
        cl_products.setOnClickListener(this);
        cl_payments.setOnClickListener(this);
        //Log.d(TAG+"token",token);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.cl_orders:
                HomeNavigationCallBack homeNavigationCallBack = (HomeNavigationCallBack) getActivity();
                homeNavigationCallBack.homeNavigationCallBack(getString(R.string.orders));
                break;
            case R.id.cl_products:
                fragment = new ProductsFragment();
                switchFragment(fragment);
                break;

            case R.id.cl_payments:
                fragment = new PaymentsFragment();
                switchFragment(fragment);
                break;


        }

    }

    private void switchFragment(Fragment fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.detailsFragment, fragment);
        fragmentTransaction.setTransitionStyle(R.style.Theme_Transparent);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void close() {

    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_DASHBOARD_DETAILS:
                if (jsonResponse != null) {
                    DashboardDataModel dashboardDataModel = new Gson().fromJson(jsonResponse, DashboardDataModel.class);
                    if (dashboardDataModel != null) {
                        boolean status = dashboardDataModel.getStatus();
                        String message = dashboardDataModel.getMessage();
                        if (status) {
                            Dashboarddata dashboarddata = dashboardDataModel.getDashboarddata();
                            if (dashboarddata != null) {
                                setData(dashboarddata);
                            } else {
                                Toast.makeText(getActivity(), "No data available", Toast.LENGTH_LONG).show();
                            }
                        }
                    } else {
                        Toast.makeText(getActivity(), "No data available", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "No data available", Toast.LENGTH_LONG).show();
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_FOR_DEVICE_TOKEN:
                if (jsonResponse != null) {
                    DeviceTokenResponse deviceTokenResponse = new Gson().fromJson(jsonResponse, DeviceTokenResponse.class);
                    if (deviceTokenResponse != null) {
                        boolean status = deviceTokenResponse.getStatus();
                        if (status) {
                            String message = deviceTokenResponse.getMessage();
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    private void setData(Dashboarddata dashboarddata) {
        tv_received_orders.setText(String.valueOf(dashboarddata.getReceivedOrders()));
        tv_accepted_orders.setText(String.valueOf(dashboarddata.getAcceptedOrders()));
        tv_completed_orders.setText(String.valueOf(dashboarddata.getCompletedOrders()));
        tv_active_products.setText(String.valueOf(dashboarddata.getActiveProducts()));
        tv_approved_products.setText(String.valueOf(dashboarddata.getApprovedProducts()));
        tv_pending_products.setText(String.valueOf(dashboarddata.getPendingProducts()));
        tv_next_settlement_date.setText(String.valueOf(dashboarddata.getNextSettlementDate()));
        tv_wallet_money.setText(String.valueOf(dashboarddata.getWalletMoney()));
    }

}
