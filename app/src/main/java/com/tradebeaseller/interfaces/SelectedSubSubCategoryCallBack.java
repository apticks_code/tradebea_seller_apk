package com.tradebeaseller.interfaces;

public interface SelectedSubSubCategoryCallBack {
    void selectedSubSubCategoryDetails(String selectedSubSubCategoryId, String selectedSubSubCategoryName);
}
