package com.tradebeaseller.ApiCalls;

import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.jsonBuilderParser.JsonBuilderParser;
import com.tradebeaseller.models.requestModels.generateOTPRequest.GenerateOTPRequest;
import com.tradebeaseller.utils.CommonMethods;
import com.tradebeaseller.utils.Constants;

import org.json.JSONObject;

public class GenerateOTPApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallToGenerateOTP(final Context context, String mobileNumber) {
        String url = Constants.GENERATE_OTP_URL;
        GenerateOTPRequest generateOTPRequest = new GenerateOTPRequest();
        generateOTPRequest.mobile = mobileNumber;
        generateOTPRequest.group = "2";
        JSONObject postObject = JsonBuilderParser.jsonBuilder(generateOTPRequest);

        AndroidNetworking.post(url)
                .addJSONObjectBody(postObject) // posting json
                .setPriority(Priority.HIGH)
                .addHeaders("APP_id", Constants.APP_ID)
                .addHeaders("Content-Type", "application/json")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_GENERATE_OTP);
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_GENERATE_OTP);
                    }
                });
    }
}
