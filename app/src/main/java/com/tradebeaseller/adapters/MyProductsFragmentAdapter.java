package com.tradebeaseller.adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.tradebeaseller.ApiCalls.AddProductToListApiCall;
import com.tradebeaseller.ApiCalls.DeleteProductApiCall;
import com.tradebeaseller.R;
import com.tradebeaseller.activities.AddProductActivity;
import com.tradebeaseller.activities.EditProductActivity;
import com.tradebeaseller.activities.EditProductSellerActivity;
import com.tradebeaseller.fragments.MyProductsFragment;
import com.tradebeaseller.interfaces.CloseCallBack;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.models.responseModels.displayProductDetailsResponse.BrandDetails;
import com.tradebeaseller.models.responseModels.displayProductDetailsResponse.CategoryDetails;
import com.tradebeaseller.models.responseModels.displayProductDetailsResponse.MenuDetails;
import com.tradebeaseller.models.responseModels.displayProductDetailsResponse.ProductDetails;
import com.tradebeaseller.models.responseModels.displayProductDetailsResponse.ProductImageDetails;
import com.tradebeaseller.models.responseModels.displayProductDetailsResponse.SubCategoryDetails;
import com.tradebeaseller.utils.Constants;
import com.tradebeaseller.utils.DialogUtils;
import com.tradebeaseller.utils.PreferenceConnector;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

public class MyProductsFragmentAdapter extends RecyclerView.Adapter<MyProductsFragmentAdapter.ViewHolder> implements HttpReqResCallBack {

    private Context context;
    private Fragment fragment;
    private Dialog progressDialog;
    private ProductDetails productDetails;
    private Transformation transformation;
    private LinkedList<ProductDetails> listOfProductDetails;

    private String token = "";
    private int selectedPosition = -1;

    public MyProductsFragmentAdapter(Context context, Fragment fragment, LinkedList<ProductDetails> listOfProductDetails) {
        this.context = context;
        this.fragment = fragment;
        this.listOfProductDetails = listOfProductDetails;
        token = PreferenceConnector.readString(context, context.getString(R.string.user_token), "");

        transformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(5)
                .borderColor(Color.parseColor("#00000000"))
                .borderWidthDp(1)
                .oval(false)
                .build();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_my_product_fragment_items, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ProductDetails productDetails = listOfProductDetails.get(position);
        if (productDetails != null) {
            String name = productDetails.getName();
            MenuDetails menuDetails = productDetails.getMenuDetails();
            CategoryDetails categoryDetails = productDetails.getCategoryDetails();
            SubCategoryDetails subCategoryDetails = productDetails.getSubCategoryDetails();
            BrandDetails brandDetails = productDetails.getBrandDetails();
            LinkedList<ProductImageDetails> listOfProductImages = productDetails.getListOfProductImages();

            holder.tvProductTitle.setText(name);
            holder.tvProductPrice.setText("M.R.P" + " : " + context.getString(R.string.current_currency) + "100");

            if (menuDetails != null) {
                String menuName = menuDetails.getName();
                holder.tvMainMenu.setText(menuName);
            } else {
                holder.tvMainMenu.setText("N/A");
            }

            if (categoryDetails != null) {
                String categoryName = categoryDetails.getName();
                holder.tvCategory.setText(categoryName);
            } else {
                holder.tvCategory.setText("N/A");
            }

            if (subCategoryDetails != null) {
                String subCategoryName = subCategoryDetails.getName();
                holder.tvSubCategory.setText(subCategoryName);
            } else {
                holder.tvSubCategory.setText("N/A");
            }

            if (brandDetails != null) {
                String brandName = brandDetails.getName();
                holder.tvBrand.setText(brandName);
            } else {
                holder.tvBrand.setText("N/A");
            }

            if (listOfProductImages != null) {
                if (listOfProductImages.size() != 0) {
                    ProductImageDetails productImageDetails = listOfProductImages.get(0);
                    String imageUrl = productImageDetails.getImageUrl();
                    setProductImageUrl(holder, imageUrl);
                } else {
                    Picasso.get()
                            .load(R.drawable.image_placeholder)
                            .error(R.drawable.image_placeholder)
                            .placeholder(R.drawable.image_placeholder)
                            .transform(transformation)
                            .fit().centerCrop()
                            .into(holder.ivProductImage);
                }
            } else {
                Picasso.get()
                        .load(R.drawable.image_placeholder)
                        .error(R.drawable.image_placeholder)
                        .placeholder(R.drawable.image_placeholder)
                        .transform(transformation)
                        .fit().centerCrop()
                        .into(holder.ivProductImage);
            }
        }
    }

    private void setProductImageUrl(ViewHolder holder, String imageUrl) {
        if (imageUrl != null) {
            if (!imageUrl.isEmpty()) {
                Picasso.get()
                        .load(imageUrl)
                        .error(R.drawable.image_placeholder)
                        .placeholder(R.drawable.image_placeholder)
                        .transform(transformation)
                        .fit().centerCrop()
                        .into(holder.ivProductImage);
            } else {
                Picasso.get()
                        .load(R.drawable.image_placeholder)
                        .error(R.drawable.image_placeholder)
                        .placeholder(R.drawable.image_placeholder)
                        .transform(transformation)
                        .fit().centerCrop()
                        .into(holder.ivProductImage);
            }
        } else {
            Picasso.get()
                    .load(R.drawable.image_placeholder)
                    .error(R.drawable.image_placeholder)
                    .placeholder(R.drawable.image_placeholder)
                    .transform(transformation)
                    .fit().centerCrop()
                    .into(holder.ivProductImage);
        }
    }

    @Override
    public int getItemCount() {
        return listOfProductDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView ivProductImage;
        private LinearLayout llEdit, llDelete, llAdd;
        private TextView tvProductTitle, tvProductPrice, tvMainMenu, tvCategory, tvSubCategory, tvBrand, tvEdit, tvDelete, tvAdd;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            llAdd = itemView.findViewById(R.id.llAdd);
            tvAdd = itemView.findViewById(R.id.tvAdd);
            llEdit = itemView.findViewById(R.id.llEdit);
            tvEdit = itemView.findViewById(R.id.tvEdit);
            tvBrand = itemView.findViewById(R.id.tvBrand);
            tvDelete = itemView.findViewById(R.id.tvDelete);
            llDelete = itemView.findViewById(R.id.llDelete);
            tvMainMenu = itemView.findViewById(R.id.tvMainMenu);
            tvCategory = itemView.findViewById(R.id.tvCategory);
            tvSubCategory = itemView.findViewById(R.id.tvSubCategory);
            tvProductTitle = itemView.findViewById(R.id.tvProductTitle);
            tvProductPrice = itemView.findViewById(R.id.tvProductPrice);
            ivProductImage = itemView.findViewById(R.id.ivProductImage);

            llAdd.setOnClickListener(this);
            tvAdd.setOnClickListener(this);
            llEdit.setOnClickListener(this);
            tvEdit.setOnClickListener(this);
            llDelete.setOnClickListener(this);
            tvDelete.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            selectedPosition = getLayoutPosition();
            productDetails = listOfProductDetails.get(getLayoutPosition());
            if (id == R.id.llAdd) {
                goToAddProduct();
            } else if (id == R.id.tvAdd) {
                goToAddProduct();
            } else if (id == R.id.llEdit) {
                goToEditProduct();
            } else if (id == R.id.tvEdit) {
                goToEditProduct();
            } else if (id == R.id.llDelete) {
                prepareDeleteProduct();
            } else if (id == R.id.tvDelete) {
                prepareDeleteProduct();
            }
        }
    }


    private void goToAddProduct() {
        showProgressBar(context);
        String token = PreferenceConnector.readString(context, context.getString(R.string.user_token), "");
        AddProductToListApiCall.serviceCallForAddProductToList(context, null, this, productDetails.getId(), token);
    }

    private void goToEditProduct() {
        Intent editProductDetailsIntent = new Intent(context, EditProductSellerActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(context.getString(R.string.product_id), String.valueOf(productDetails.getId()));
        editProductDetailsIntent.putExtras(bundle);
        context.startActivity(editProductDetailsIntent);
    }

    private void prepareDeleteProduct() {
        if (productDetails != null) {
            showDeleteAlertDialog();
        }
    }

    private void showDeleteAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        alertDialogBuilder.setTitle(context.getString(R.string.app_name));
        alertDialogBuilder.setMessage(context.getString(R.string.are_you_sure_do_you_want_to_delete_product));
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int productId = productDetails.getId();
                DeleteProductApiCall.serviceCallForDeleteProduct(context, null, MyProductsFragmentAdapter.this, String.valueOf(productId), token);
                dialog.cancel();
            }
        });
        alertDialogBuilder.setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_DELETE_PRODUCT:
                if (jsonResponse != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(jsonResponse);
                        boolean status = jsonObject.getBoolean("status");
                        String message = jsonObject.getString("message");
                        if (status) {
                            listOfProductDetails.remove(selectedPosition);
                            if (listOfProductDetails.size() == 0) {
                                CloseCallBack closeCallBack = (CloseCallBack) fragment;
                                closeCallBack.close();
                            }
                            notifyDataSetChanged();
                        } else {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_TO_ADD_PRODUCT_TO_LIST:
                if (jsonResponse != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(jsonResponse);
                        boolean status = jsonObject.getBoolean("status");
                        String message = jsonObject.getString("message");
                        if (status) {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    public void showProgressBar(Context context) {
        try {
            if (progressDialog != null && !progressDialog.isShowing()) {
                progressDialog = DialogUtils.getDialogUtilsInstance().progressDialog(context);
            } else {
                closeProgressbar();
                progressDialog = null;
                progressDialog = DialogUtils.getDialogUtilsInstance().progressDialog(context);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void closeProgressbar() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
