package com.tradebeaseller.models.responseModels.groupDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GroupData {

    @SerializedName("option_group_id")
    @Expose
    private Integer optionGroupId;
    @SerializedName("option_group")
    @Expose
    private GroupDetails groupDetails;


    public Integer getOptionGroupId() {
        return optionGroupId;
    }

    public void setOptionGroupId(Integer optionGroupId) {
        this.optionGroupId = optionGroupId;
    }

    public GroupDetails getGroupDetails() {
        return groupDetails;
    }

    public void setGroupDetails(GroupDetails groupDetails) {
        this.groupDetails = groupDetails;
    }
}
