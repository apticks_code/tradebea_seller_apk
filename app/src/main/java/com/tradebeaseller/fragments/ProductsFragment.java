package com.tradebeaseller.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.tabs.TabLayout;
import com.tradebeaseller.R;
import com.tradebeaseller.adapters.ProductsFragmentTabAdapter;

public class ProductsFragment extends BaseFragment {

    private TabLayout tabLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_products_fragment, container, false);
        initializeUi(view);
        initializeTabs();
        return view;
    }

    private void initializeUi(View view) {
        tabLayout = view.findViewById(R.id.tabLayout);
    }

    private void initializeTabs() {
        ProductsFragmentTabAdapter productsFragmentTabAdapter = new ProductsFragmentTabAdapter(getActivity(), this, getFragmentManager(), tabLayout, R.id.fragmentContainer);

        productsFragmentTabAdapter.addTab(tabLayout.newTab().setText(getResources().getString(R.string.catalog_product)).setTag(getResources().getString(R.string.catalog_product)), CatalogProductFragment.class, null);
        productsFragmentTabAdapter.addTab(tabLayout.newTab().setText(getResources().getString(R.string.my_inventory)).setTag(getResources().getString(R.string.my_inventory)), MyProductsFragment.class, null);
        productsFragmentTabAdapter.addTab(tabLayout.newTab().setText(getResources().getString(R.string.approved)).setTag(getResources().getString(R.string.approved)), ApprovedProductsFragment.class, null);
        productsFragmentTabAdapter.addTab(tabLayout.newTab().setText(getResources().getString(R.string.pending)).setTag(getResources().getString(R.string.pending)), PendingProductsFragment.class, null);
    }
}
