package com.tradebeaseller.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.tradebeaseller.R;
import com.tradebeaseller.interfaces.CloseCallBack;
import com.tradebeaseller.interfaces.SelectedSubSubCategoryCallBack;
import com.tradebeaseller.models.responseModels.subSubCategoryDetailsResponse.SubSubCategoryDetails;
import com.tradebeaseller.utils.UserData;

import java.util.LinkedList;

public class SubSubCategoryDetailsAdapter extends RecyclerView.Adapter<SubSubCategoryDetailsAdapter.ViewHolder> implements View.OnClickListener {

    private Context context;

    private LinkedList<SubSubCategoryDetails> listOfSubSubCategoryDetails;

    private String selectedSubSubCategoryId = "";
    private String selectedSubSubCategoryName = "";

    private int selectedPosition = -1;

    public SubSubCategoryDetailsAdapter(Context context, LinkedList<SubSubCategoryDetails> listOfSubSubCategoryDetails, String selectedSubSubCategoryId, String selectedSubSubCategoryName, TextView tvSave) {
        this.context = context;
        tvSave.setOnClickListener(this);
        this.selectedSubSubCategoryId = selectedSubSubCategoryId;
        this.selectedSubSubCategoryName = selectedSubSubCategoryName;
        this.listOfSubSubCategoryDetails = listOfSubSubCategoryDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_sub_sub_categories_details_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        SubSubCategoryDetails subSubCategoryDetails = listOfSubSubCategoryDetails.get(position);
        if (subSubCategoryDetails != null) {
            String name = subSubCategoryDetails.getName();
            String imageUrl = subSubCategoryDetails.getImageUrl();
            String id = String.valueOf(subSubCategoryDetails.getId());

            holder.tvSubSubCategoryName.setText(name);

            if (id.equalsIgnoreCase(selectedSubSubCategoryId)) {
                holder.checkBox.setChecked(true);
            } else {
                holder.checkBox.setChecked(false);
            }

            if (imageUrl != null) {
                if (!imageUrl.isEmpty()) {
                    Glide.with(context)
                            .load(imageUrl)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .placeholder(R.drawable.ic_user_image)
                            .error(R.drawable.ic_user_image)
                            .into(holder.ivPic);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return listOfSubSubCategoryDetails.size();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.tvSave:
                prepareSaveDetails();
                break;
            default:
                break;
        }
    }

    private void prepareSaveDetails() {
        Context context = UserData.getInstance().getContext();
        if (context != null) {
            SelectedSubSubCategoryCallBack selectedSubSubCategoryCallBack = (SelectedSubSubCategoryCallBack) context;
            selectedSubSubCategoryCallBack.selectedSubSubCategoryDetails(selectedSubSubCategoryId, selectedSubSubCategoryName);
        }

        CloseCallBack closeCallBack = (CloseCallBack) this.context;
        closeCallBack.close();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {

        private ImageView ivPic;
        private CheckBox checkBox;
        private TextView tvSubSubCategoryName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivPic = itemView.findViewById(R.id.ivPic);
            checkBox = itemView.findViewById(R.id.checkBox);
            tvSubSubCategoryName = itemView.findViewById(R.id.tvSubSubCategoryName);
            checkBox.setOnCheckedChangeListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                selectedSubSubCategoryId = String.valueOf(listOfSubSubCategoryDetails.get(getLayoutPosition()).getId());
                selectedSubSubCategoryName = listOfSubSubCategoryDetails.get(getLayoutPosition()).getName();
                if (selectedPosition == -1) {
                    selectedPosition = getLayoutPosition();
                } else {
                    if (selectedPosition != getLayoutPosition()) {
                        selectedPosition = getLayoutPosition();
                        android.os.Handler handler = new android.os.Handler();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                notifyDataSetChanged();
                            }
                        });
                    }
                }
            } else {
                if (selectedPosition == getLayoutPosition()) {
                    selectedSubSubCategoryId = "";
                    selectedSubSubCategoryName = "";
                    selectedPosition = -1;
                }
            }
        }
    }
}
