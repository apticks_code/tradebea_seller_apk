package com.tradebeaseller.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.tradebeaseller.R;
import com.tradebeaseller.activities.ProductGroupDetailsActivity;
import com.tradebeaseller.interfaces.CloseCallBack;
import com.tradebeaseller.interfaces.SelectedProductGroupDetailsCallBack;
import com.tradebeaseller.models.responseModels.groupDetailsResponse.GroupData;
import com.tradebeaseller.models.responseModels.groupDetailsResponse.GroupDetails;
import com.tradebeaseller.utils.UserData;

import java.util.LinkedList;

public class ProductGroupDetailsAdapter extends RecyclerView.Adapter<ProductGroupDetailsAdapter.ViewHolder> implements View.OnClickListener {

    private Context context;
    private ColorGenerator generator;
    private TextDrawable.IBuilder mDrawableBuilder;

    private LinkedList<GroupData> listOfGroupData;

    private String selectedProductGroupId = "";
    private String selectedProductGroupName = "";

    private int selectedPosition = -1;

    public ProductGroupDetailsAdapter(Context context, LinkedList<GroupData> listOfGroupData, String selectedProductGroupId, String selectedProductGroupName, TextView tvSave) {
        this.context = context;
        tvSave.setOnClickListener(this);
        generator = ColorGenerator.MATERIAL;
        this.listOfGroupData = listOfGroupData;
        mDrawableBuilder = TextDrawable.builder().round();
        this.selectedProductGroupId = selectedProductGroupId;
        this.selectedProductGroupName = selectedProductGroupName;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_group_option_details_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GroupData groupData = listOfGroupData.get(position);
        if (groupData != null) {
            GroupDetails groupDetails = groupData.getGroupDetails();
            String id = String.valueOf(groupDetails.getId());
            String name = groupDetails.getName();
            int randomColor = generator.getRandomColor();
            TextDrawable drawable = mDrawableBuilder.build(String.valueOf(name.charAt(0)), randomColor);

            holder.tvProductGroupName.setText(name);
            holder.ivPic.setImageDrawable(drawable);

            if (id.equalsIgnoreCase(selectedProductGroupId)) {
                holder.checkBox.setChecked(true);
            } else {
                holder.checkBox.setChecked(false);
            }
        }
    }

    @Override
    public int getItemCount() {
        return listOfGroupData.size();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.tvSave:
                prepareSaveDetails();
                break;
            default:
                break;
        }
    }

    private void prepareSaveDetails() {
        Context context = UserData.getInstance().getContext();
        if (context != null) {
            SelectedProductGroupDetailsCallBack selectedProductGroupDetailsCallBack = (SelectedProductGroupDetailsCallBack) context;
            selectedProductGroupDetailsCallBack.selectedProductGroupDetails(selectedProductGroupId, selectedProductGroupName);
        }

        CloseCallBack closeCallBack = (CloseCallBack) this.context;
        closeCallBack.close();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {

        private ImageView ivPic;
        private CheckBox checkBox;
        private TextView tvProductGroupName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivPic = itemView.findViewById(R.id.ivPic);
            checkBox = itemView.findViewById(R.id.checkBox);
            tvProductGroupName = itemView.findViewById(R.id.tvProductGroupName);
            checkBox.setOnCheckedChangeListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                selectedProductGroupId = String.valueOf(listOfGroupData.get(getLayoutPosition()).getGroupDetails().getId());
                selectedProductGroupName = listOfGroupData.get(getLayoutPosition()).getGroupDetails().getName();
                if (selectedPosition == -1) {
                    selectedPosition = getLayoutPosition();
                } else {
                    if (selectedPosition != getLayoutPosition()) {
                        selectedPosition = getLayoutPosition();
                        android.os.Handler handler = new android.os.Handler();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                notifyDataSetChanged();
                            }
                        });
                    }
                }
            } else {
                if (selectedPosition == getLayoutPosition()) {
                    selectedProductGroupId = "";
                    selectedProductGroupName = "";
                    selectedPosition = -1;
                }
            }
        }
    }
}
