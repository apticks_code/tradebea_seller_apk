package com.tradebeaseller.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.tradebeaseller.ApiCalls.SaveAddProductDetailsApiCall;
import com.tradebeaseller.R;
import com.tradebeaseller.adapters.AddProductImageDetailsAdapter;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.interfaces.SelectedBrandDetailsCallBack;
import com.tradebeaseller.interfaces.SelectedCategoryCallBack;
import com.tradebeaseller.interfaces.SelectedItemsDetailsCallBack;
import com.tradebeaseller.interfaces.SelectedMenuDetailsCallBack;
import com.tradebeaseller.interfaces.SelectedProductGroupDetailsCallBack;
import com.tradebeaseller.interfaces.SelectedProductOptionDetailsCallBack;
import com.tradebeaseller.interfaces.SelectedSubCategoryCallBack;
import com.tradebeaseller.interfaces.SelectedSubSubCategoryCallBack;
import com.tradebeaseller.models.requestModels.AddOptionDetails;
import com.tradebeaseller.models.requestModels.saveAddProductRequest.ProductImageDetails;
import com.tradebeaseller.models.requestModels.saveAddProductRequest.SaveAddProductRequest;
import com.tradebeaseller.models.requestModels.saveAddProductRequest.VariantDetails;
import com.tradebeaseller.utils.AppPermissions;
import com.tradebeaseller.utils.Constants;
import com.tradebeaseller.utils.PreferenceConnector;
import com.tradebeaseller.utils.UserData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

public class AddProductActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack, SelectedMenuDetailsCallBack, SelectedCategoryCallBack, SelectedSubCategoryCallBack, SelectedSubSubCategoryCallBack, SelectedBrandDetailsCallBack, SelectedProductOptionDetailsCallBack, SelectedProductGroupDetailsCallBack, SelectedItemsDetailsCallBack {

    private RecyclerView rvProductImageDetails;
    private ImageView ivBackArrow, ivAddProductPic;
    private BottomSheetDialog pickerBottomSheetDialog;
    private LinearLayout llGenerateVariantDetails, llAddOptionDetails;
    private AddProductImageDetailsAdapter addProductImageDetailsAdapter;
    private TextView tvGenerateVariant, tvAddOption, tvVariantInformationText, tvAddOptionText, tvSubmit;
    private EditText etShopName, etMenu, etCategory, etSubCategory, etSubSubCategory, etBrand, etDescription;

    private ArrayList<String> listOfPhotoPaths;
    private LinkedList<String> listOfSelectedPhotoPaths;
    private LinkedList<View> listOfProductOptionViews = new LinkedList<>();
    private LinkedList<View> listOfGenerateVariantViews = new LinkedList<>();
    private LinkedList<String> listOfTempGenerateVariantItems = new LinkedList<>();
    private LinkedList<AddOptionDetails> listOfAddOptionDetails = new LinkedList<>();
    private LinkedList<String> listOfTempGenerateVariantItemsIds = new LinkedList<>();


    private String token = "";
    private String selectedMenuId = "";
    private String selectedMenuName = "";

    private String selectedBrandId = "";
    private String selectedBrandName = "";

    private String selectedCategoryId = "";
    private String selectedCategoryName = "";

    private String selectedSubCategoryId = "";
    private String selectedSubCategoryName = "";

    private String selectedSubSubCategoryId = "";
    private String selectedSubSubCategoryName = "";


    private int addOptionCount = 0;
    private int MAX_ATTACHMENT_COUNT = 5;
    private int selectedItemPosition = -1;
    private int selectedGroupPosition = -1;
    private int selectedOptionPosition = -1;
    private int selectedDeletePosition = -1;
    private int selectedDeleteVariantPosition = -1;

    private int selectionType = 1;//1- radio ,2-checkbox
    private String previousMenuId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_add_product);
        initializeUi();
        initializeListeners();
    }

    private void initializeUi() {
        etMenu = findViewById(R.id.etMenu);
        etBrand = findViewById(R.id.etBrand);
        tvSubmit = findViewById(R.id.tvSubmit);
        etCategory = findViewById(R.id.etCategory);
        etShopName = findViewById(R.id.etShopName);
        tvAddOption = findViewById(R.id.tvAddOption);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        etDescription = findViewById(R.id.etDescription);
        etSubCategory = findViewById(R.id.etSubCategory);
        tvAddOptionText = findViewById(R.id.tvAddOptionText);
        ivAddProductPic = findViewById(R.id.ivAddProductPic);
        etSubSubCategory = findViewById(R.id.etSubSubCategory);
        tvGenerateVariant = findViewById(R.id.tvGenerateVariant);
        llAddOptionDetails = findViewById(R.id.llAddOptionDetails);
        rvProductImageDetails = findViewById(R.id.rvProductImageDetails);
        llGenerateVariantDetails = findViewById(R.id.llGenerateVariantDetails);
        tvVariantInformationText = findViewById(R.id.tvVariantInformationText);

        listOfAddOptionDetails = new LinkedList<>();
        listOfProductOptionViews = new LinkedList<>();
        listOfSelectedPhotoPaths = new LinkedList<>();
        token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
    }

    private void initializeListeners() {
        etMenu.setOnClickListener(this);
        etBrand.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);
        etCategory.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);
        tvAddOption.setOnClickListener(this);
        etSubCategory.setOnClickListener(this);
        ivAddProductPic.setOnClickListener(this);
        etSubSubCategory.setOnClickListener(this);
        tvGenerateVariant.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        UserData.getInstance().setContext(this);
        UserData.getInstance().setFragment(null);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            case R.id.ivAddProductPic:
                prepareAddProductPic();
                break;
            case R.id.etMenu:
                prepareMenuDetails();
                break;
            case R.id.etCategory:
                prepareCategoryDetails();
                break;
            case R.id.etSubCategory:
                prepareSubCategoryDetails();
                break;
            case R.id.etSubSubCategory:
                prepareSubSubCategoryDetails();
                break;
            case R.id.etBrand:
                prepareBrandDetails();
                break;
            case R.id.tvGenerateVariant:
                prepareGenerateVariantDetails();
                break;
            case R.id.tvAddOption:
                addOptionCount = addOptionCount + 1;
                prepareAddOptionDetails();
                break;
            case R.id.tvOptions:
                selectedOptionPosition = (int) view.getTag();
                selectionType = 1;
                prepareOptionDetails();
                break;
            case R.id.tvGroups:
                selectedGroupPosition = (int) view.getTag();
                selectionType = 1;
                prepareGroupDetails();
                break;
            case R.id.tvItems:
                selectedItemPosition = (int) view.getTag();
                selectionType = 2;
                prepareItemDetails();
                break;
            case R.id.ivDeleteOption:
                selectedDeletePosition = (int) view.getTag();
                prepareDeleteOption();
                break;
            case R.id.tvImagePicker:
                listOfPhotoPaths = new ArrayList<>();
                onPickPhoto();
                closeBottomSheetView();
                break;
            case R.id.tvCamera:
                listOfPhotoPaths = new ArrayList<>();
                captureImage();
                closeBottomSheetView();
                break;
            case R.id.tvCancel:
                closeBottomSheetView();
                break;
            case R.id.ivDeleteVariant:
                selectedDeleteVariantPosition = (int) view.getTag();
                prepareDeleteVariant();
                break;
            case R.id.tvSubmit:
                showProgressBar(this);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        prepareSubmitDetails();
                    }
                }, 1000);
                break;
            default:
                break;
        }
    }

    private void prepareSubmitDetails() {
        SaveAddProductRequest saveAddProductRequest = new SaveAddProductRequest();
        LinkedList<VariantDetails> listOfVariants = new LinkedList<>();
        LinkedList<ProductImageDetails> listOfProductImages = new LinkedList<>();
        try {
            String shopName = etShopName.getText().toString();
            String description = etDescription.getText().toString();
            if (listOfGenerateVariantViews != null) {
                if (listOfGenerateVariantViews.size() != 0) {
                    for (int index = 0; index < listOfGenerateVariantViews.size(); index++) {
                        VariantDetails variantDetails = new VariantDetails();
                        View view = listOfGenerateVariantViews.get(index);
                        TextView tvVariantName = view.findViewById(R.id.tvVariantName);
                        EditText etMrp = view.findViewById(R.id.etMrp);
                        EditText etDiscount = view.findViewById(R.id.etDiscount);

                        String variantId = tvVariantName.getTag().toString();
                        String mrp = etMrp.getText().toString();
                        String discount = etDiscount.getText().toString();

                        LinkedList<String> listOfOptionItemIds = new LinkedList<>(Arrays.asList(variantId.split("\\s*,\\s*")));
                        variantDetails.setMrp(mrp);
                        variantDetails.setDiscount(discount);
                        variantDetails.setListOfOptionItemIds(listOfOptionItemIds);
                        listOfVariants.add(variantDetails);
                    }
                }
            }

            if (listOfSelectedPhotoPaths != null) {
                if (listOfSelectedPhotoPaths.size() != 0) {
                    for (int index = 0; index < listOfSelectedPhotoPaths.size(); index++) {
                        ProductImageDetails productImageDetails = new ProductImageDetails();
                        String selectedPhotoPaths = listOfSelectedPhotoPaths.get(index);
                        Bitmap bm = BitmapFactory.decodeFile(selectedPhotoPaths);
                        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
                        bm.compress(Bitmap.CompressFormat.JPEG, 90, bOut);
                        String base64Image = Base64.encodeToString(bOut.toByteArray(), Base64.DEFAULT);
                        productImageDetails.setImage(base64Image);
                        productImageDetails.setExt("jpg");
                        listOfProductImages.add(productImageDetails);
                    }
                }
            }

            if (listOfProductImages.size() != 0) {
                if (listOfVariants.size() != 0) {
                    if (!shopName.isEmpty()) {
                        if (!selectedMenuId.isEmpty()) {
                            if (!selectedCategoryId.isEmpty()) {
                                if (!selectedBrandId.isEmpty()) {
                                    saveAddProductRequest.setName(shopName);
                                    saveAddProductRequest.setDesc(description);
                                    saveAddProductRequest.setMenuId(selectedMenuId);
                                    saveAddProductRequest.setCatId(selectedCategoryId);
                                    saveAddProductRequest.setSubCatId(selectedSubCategoryId);
                                    saveAddProductRequest.setSubSubCatId(selectedSubSubCategoryId);
                                    saveAddProductRequest.setBrandId(selectedBrandId);
                                    saveAddProductRequest.setListOfProductImages(listOfProductImages);
                                    saveAddProductRequest.setListOfVariantDetails(listOfVariants);
                                    SaveAddProductDetailsApiCall.serviceCallForSaveProductDetails(this, null, null, saveAddProductRequest, token);
                                } else {
                                    closeProgressbar();
                                    Toast.makeText(this, getString(R.string.please_select_brand), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                closeProgressbar();
                                Toast.makeText(this, getString(R.string.please_select_category), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            closeProgressbar();
                            Toast.makeText(this, getString(R.string.please_select_menu), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        closeProgressbar();
                        Toast.makeText(this, getString(R.string.please_enter_shop_name), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    closeProgressbar();
                    Toast.makeText(this, getString(R.string.please_generate_variants), Toast.LENGTH_SHORT).show();
                }
            } else {
                closeProgressbar();
                Toast.makeText(this, getString(R.string.please_select_product_image), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception exception) {
            closeProgressbar();
            exception.printStackTrace();
        }
    }

    private void prepareDeleteVariant() {
        if (selectedDeleteVariantPosition != -1) {
            View view = listOfGenerateVariantViews.get(selectedDeleteVariantPosition);
            listOfTempGenerateVariantItems.remove(selectedDeleteVariantPosition);
            listOfTempGenerateVariantItemsIds.remove(selectedDeleteVariantPosition);
            llGenerateVariantDetails.removeView(view);
        }
    }

    private void prepareAddProductPic() {
        if (AppPermissions.checkPermissionForAccessExternalStorage(this)) {
            if (AppPermissions.checkPermissionForCamera(this)) {
                if (listOfSelectedPhotoPaths.size() < 4) {
                    showBottomSheetView();
                } else {
                    Toast.makeText(this, "Max 4 images", Toast.LENGTH_SHORT).show();
                }
            } else {
                AppPermissions.requestPermissionForCamera(this);
            }
        } else {
            AppPermissions.requestPermissionForAccessExternalStorage(this);
        }
    }

    @SuppressLint("InflateParams")
    private void showBottomSheetView() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.layout_image_picker_sheet, null);

        TextView tvCamera = view.findViewById(R.id.tvCamera);
        TextView tvImagePicker = view.findViewById(R.id.tvImagePicker);
        TextView tvCancel = view.findViewById(R.id.tvCancel);

        tvCamera.setOnClickListener(this);
        tvImagePicker.setOnClickListener(this);
        tvCancel.setOnClickListener(this);

        pickerBottomSheetDialog = new BottomSheetDialog(this, R.style.BottomSheetDialog);
        pickerBottomSheetDialog.setContentView(view);
        pickerBottomSheetDialog.setCanceledOnTouchOutside(true);
        pickerBottomSheetDialog.show();
    }

    private void closeBottomSheetView() {
        if (pickerBottomSheetDialog != null) {
            pickerBottomSheetDialog.cancel();
        }
    }

    private void onPickPhoto() {
        int maxCount = MAX_ATTACHMENT_COUNT - listOfSelectedPhotoPaths.size();
        if (listOfSelectedPhotoPaths.size() == MAX_ATTACHMENT_COUNT) {
            Toast.makeText(this, "Cannot select more than " + MAX_ATTACHMENT_COUNT + " items", Toast.LENGTH_SHORT).show();
        } else {
            FilePickerBuilder.getInstance()
                    .setMaxCount(maxCount)
                    .setSelectedFiles(new ArrayList<>())
                    .setActivityTheme(R.style.FilePickerTheme)
                    .setActivityTitle(getString(R.string.select_image))
                    .enableVideoPicker(false)
                    .enableCameraSupport(true)
                    .showGifs(true)
                    .showFolderView(true)
                    .enableSelectAll(false)
                    .enableImagePicker(true)
                    .setCameraPlaceholder(R.drawable.image_placeholder)
                    .withOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                    .pickPhoto(this, Constants.PICK_GALLERY);
        }
    }

    private void captureImage() {
        ImagePicker.cameraOnly().start(this, Constants.REQUEST_CODE_CAPTURE_IMAGE);
    }

    private void prepareOptionDetails() {
        AddOptionDetails addOptionDetails = null;
        String selectedProductOptionId = "";
        String selectedProductOptionName = "";
        if (selectedOptionPosition != -1) {
            if (listOfAddOptionDetails.size() > selectedOptionPosition) {
                addOptionDetails = listOfAddOptionDetails.get(selectedOptionPosition);
                selectedProductOptionId = addOptionDetails.getOptionId();
                selectedProductOptionName = addOptionDetails.getOptionName();
            }
        }

        if (selectedCategoryId != null) {
            if (!selectedCategoryId.isEmpty()) {
                Intent productOptionDetailsIntent = new Intent(this, ProductOptionDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(getString(R.string.selected_category_id), selectedCategoryId);
                bundle.putString(getString(R.string.selected_product_option_id), selectedProductOptionId);
                bundle.putString(getString(R.string.selected_product_option_name), selectedProductOptionName);
                productOptionDetailsIntent.putExtras(bundle);
                startActivity(productOptionDetailsIntent);
            } else {
                Toast.makeText(this, getString(R.string.please_select_category), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.please_select_category), Toast.LENGTH_SHORT).show();
        }
    }

    private void prepareGroupDetails() {
        AddOptionDetails addOptionDetails = null;
        String selectedProductOptionId = "";
        String selectedProductOptionName = "";
        String selectedProductGroupId = "";
        String selectedProductGroupName = "";
        if (selectedGroupPosition != -1) {
            if (listOfAddOptionDetails.size() > selectedGroupPosition) {
                addOptionDetails = listOfAddOptionDetails.get(selectedGroupPosition);
                selectedProductOptionId = addOptionDetails.getOptionId();
                selectedProductOptionName = addOptionDetails.getOptionName();
                selectedProductGroupId = addOptionDetails.getGroupId();
                selectedProductGroupName = addOptionDetails.getGroupName();
            }
        }
        if (addOptionDetails != null) {
            if (selectedCategoryId != null) {
                if (!selectedCategoryId.isEmpty()) {
                    if (selectedProductOptionId != null) {
                        if (!selectedProductOptionId.isEmpty()) {
                            Intent productOptionDetailsIntent = new Intent(this, ProductGroupDetailsActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(getString(R.string.selected_category_id), selectedCategoryId);
                            bundle.putString(getString(R.string.selected_product_option_id), selectedProductOptionId);
                            bundle.putString(getString(R.string.selected_product_option_name), selectedProductOptionName);
                            bundle.putString(getString(R.string.selected_product_group_id), selectedProductGroupId);
                            bundle.putString(getString(R.string.selected_product_group_name), selectedProductGroupName);
                            productOptionDetailsIntent.putExtras(bundle);
                            startActivity(productOptionDetailsIntent);
                        } else {
                            Toast.makeText(this, getString(R.string.please_select_product_option), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, getString(R.string.please_select_product_option), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getString(R.string.please_select_category), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, getString(R.string.please_select_category), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.please_select_option), Toast.LENGTH_SHORT).show();
        }
    }

    private void prepareItemDetails() {
        AddOptionDetails addOptionDetails = null;
        String selectedProductOptionId = "";
        String selectedProductOptionName = "";
        String selectedProductGroupId = "";
        String selectedProductGroupName = "";
        String selectedProductItemIds = "";
        String selectedProductItemNames = "";
        if (selectedItemPosition != -1) {
            if (listOfAddOptionDetails.size() > selectedItemPosition) {
                addOptionDetails = listOfAddOptionDetails.get(selectedItemPosition);
                selectedProductOptionId = addOptionDetails.getOptionId();
                selectedProductOptionName = addOptionDetails.getOptionName();
                selectedProductGroupId = addOptionDetails.getGroupId();
                selectedProductGroupName = addOptionDetails.getGroupName();
                selectedProductItemIds = addOptionDetails.getItemIds();
                selectedProductItemNames = addOptionDetails.getItemNames();
            }
        }

        if (addOptionDetails != null) {
            if (selectedProductOptionId != null) {
                if (!selectedProductOptionId.isEmpty()) {
                    if (selectedProductGroupId != null) {
                        if (!selectedProductGroupId.isEmpty()) {
                            Intent productOptionDetailsIntent = new Intent(this, ProductItemsDetailsActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(getString(R.string.selected_category_id), selectedCategoryId);
                            bundle.putString(getString(R.string.selected_product_option_id), selectedProductOptionId);
                            bundle.putString(getString(R.string.selected_product_option_name), selectedProductOptionName);
                            bundle.putString(getString(R.string.selected_product_group_id), selectedProductGroupId);
                            bundle.putString(getString(R.string.selected_product_group_name), selectedProductGroupName);
                            bundle.putString(getString(R.string.selected_product_item_ids), selectedProductItemIds);
                            bundle.putString(getString(R.string.selected_product_item_names), selectedProductItemNames);
                            bundle.putInt("SelectionType", selectionType);
                            productOptionDetailsIntent.putExtras(bundle);
                            startActivity(productOptionDetailsIntent);
                        } else {
                            Toast.makeText(this, getString(R.string.please_select_group), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, getString(R.string.please_select_group), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getString(R.string.please_select_option), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, getString(R.string.please_select_option), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.please_select_option), Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("SetTextI18n")
    private void prepareDeleteOption() {
        if (selectedDeletePosition != -1) {
            if (listOfAddOptionDetails.size() > selectedDeletePosition) {
                Log.d("perform", selectedDeletePosition + "");
                listOfAddOptionDetails.remove(selectedDeletePosition);
                //llAddOptionDetails.removeAllViewsInLayout();

                listOfProductOptionViews = new LinkedList<>();
                addOptionCount = 0;
                if (listOfAddOptionDetails.size() != 0) {
                    for (int index = 0; index < listOfAddOptionDetails.size(); index++) {
                        addOptionCount = addOptionCount + 1;
                        AddOptionDetails addOptionDetails = listOfAddOptionDetails.get(index);
                        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                        assert inflater != null;
                        @SuppressLint("InflateParams") View addOptionView = inflater.inflate(R.layout.layout_add_option, null);

                        TextView tvItems = addOptionView.findViewById(R.id.tvItems);
                        TextView tvGroups = addOptionView.findViewById(R.id.tvGroups);
                        TextView tvOptions = addOptionView.findViewById(R.id.tvOptions);
                        TextView tvOptionSNO = addOptionView.findViewById(R.id.tvOptionSNO);
                        ImageView ivDeleteOption = addOptionView.findViewById(R.id.ivDeleteOption);

                        String itemNames = addOptionDetails.getItemNames();
                        String groupNames = addOptionDetails.getGroupName();
                        String optionNames = addOptionDetails.getOptionName();

                        tvItems.setText(itemNames);
                        tvGroups.setText(groupNames);
                        tvOptions.setText(optionNames);

                        tvItems.setOnClickListener(this);
                        tvGroups.setOnClickListener(this);
                        tvOptions.setOnClickListener(this);
                        ivDeleteOption.setOnClickListener(this);

                        int count = addOptionCount - 1;
                        tvItems.setTag(count);
                        tvGroups.setTag(count);
                        tvOptions.setTag(count);
                        ivDeleteOption.setTag(count);

                        tvOptionSNO.setText(getString(R.string.option) + " " + addOptionCount);
                        listOfProductOptionViews.add(addOptionView);
                        llAddOptionDetails.addView(addOptionView);
                    }
                } else {
                    tvAddOptionText.setVisibility(View.GONE);
                    llAddOptionDetails.setVisibility(View.GONE);
                    //llAddOptionDetails.removeAllViewsInLayout();
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private void prepareGenerateVariantDetails() {
        llGenerateVariantDetails.removeAllViewsInLayout();
        if (listOfAddOptionDetails != null) {
            if (listOfAddOptionDetails.size() != 0) {
                tvVariantInformationText.setVisibility(View.VISIBLE);
                listOfTempGenerateVariantItems = new LinkedList<>();
                listOfTempGenerateVariantItemsIds = new LinkedList<>();
                for (int index = 0; index < listOfAddOptionDetails.size(); index++) {
                    AddOptionDetails addOptionDetails = listOfAddOptionDetails.get(index);
                    String optionItemIds = addOptionDetails.getItemIds();
                    String optionItemNames = addOptionDetails.getItemNames();
                    LinkedList<String> listOfOptionItemIds = new LinkedList<>(Arrays.asList(optionItemIds.split("\\s*,\\s*")));
                    LinkedList<String> listOfOptionItemNames = new LinkedList<>(Arrays.asList(optionItemNames.split("\\s*,\\s*")));
                    LinkedList<String> listOfTempData = new LinkedList<>();
                    LinkedList<String> listOfTempDataIds = new LinkedList<>();
                    for (int optionItemsIndex = 0; optionItemsIndex < listOfOptionItemNames.size(); optionItemsIndex++) {
                        String optionId = listOfOptionItemIds.get(optionItemsIndex);
                        String optionName = listOfOptionItemNames.get(optionItemsIndex);
                        if (index != 0) {
                            for (int tempIndex = 0; tempIndex < listOfTempGenerateVariantItems.size(); tempIndex++) {
                                String temp = listOfTempGenerateVariantItems.get(tempIndex);
                                String tempId = listOfTempGenerateVariantItemsIds.get(tempIndex);
                                listOfTempData.add(temp + " | " + optionName);
                                listOfTempDataIds.add(tempId + "," + optionId);
                            }
                        } else {
                            listOfTempGenerateVariantItems.add(optionName);
                            listOfTempGenerateVariantItemsIds.add(optionId);
                        }
                    }
                    if (listOfTempData.size() != 0) {
                        listOfTempGenerateVariantItems = new LinkedList<>(listOfTempData);
                        listOfTempGenerateVariantItemsIds = new LinkedList<>(listOfTempDataIds);
                    }
                }
                listOfGenerateVariantViews = new LinkedList<>();
                for (int variantsIndex = 0; variantsIndex < listOfTempGenerateVariantItemsIds.size(); variantsIndex++) {
                    prepareAddGenerateVariants(listOfTempGenerateVariantItems.get(variantsIndex), listOfTempGenerateVariantItemsIds.get(variantsIndex), variantsIndex);
                }
            }
        }
    }

    private void prepareAddGenerateVariants(String variantInfo, String variantInfoId, int variantsIndex) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.layout_generate_variant, null);
        TextView tvVariantName = view.findViewById(R.id.tvVariantName);
        ImageView ivDeleteVariant = view.findViewById(R.id.ivDeleteVariant);
        EditText etMrp = view.findViewById(R.id.etMrp);
        EditText etDiscount = view.findViewById(R.id.etDiscount);

        tvVariantName.setText(variantInfo);
        tvVariantName.setTag(variantInfoId);
        ivDeleteVariant.setTag(variantsIndex);
        ivDeleteVariant.setOnClickListener(this);

        listOfGenerateVariantViews.add(view);
        llGenerateVariantDetails.addView(view);
    }

    @SuppressLint("SetTextI18n")
    private void prepareAddOptionDetails() {
        tvAddOptionText.setVisibility(View.VISIBLE);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.layout_add_option, null);

        TextView tvItems = view.findViewById(R.id.tvItems);
        TextView tvGroups = view.findViewById(R.id.tvGroups);
        TextView tvOptions = view.findViewById(R.id.tvOptions);
        TextView tvOptionSNO = view.findViewById(R.id.tvOptionSNO);
        ImageView ivDeleteOption = view.findViewById(R.id.ivDeleteOption);

        tvItems.setOnClickListener(this);
        tvGroups.setOnClickListener(this);
        tvOptions.setOnClickListener(this);
        ivDeleteOption.setOnClickListener(this);

        int count = addOptionCount - 1;
        tvItems.setTag(count);
        tvGroups.setTag(count);
        tvOptions.setTag(count);
        ivDeleteOption.setTag(count);

        tvOptionSNO.setText(getString(R.string.option) + " " + addOptionCount);
        AddOptionDetails addOptionDetails = new AddOptionDetails();
        listOfAddOptionDetails.add(addOptionDetails);
        listOfProductOptionViews.add(view);
        llAddOptionDetails.addView(view);
    }

    private void prepareMenuDetails() {
        Intent menuIntent = new Intent(this, MenuDetailsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(getString(R.string.selected_menu_id), selectedMenuId);
        bundle.putString(getString(R.string.selected_menu_name), selectedMenuName);
        menuIntent.putExtras(bundle);
        startActivity(menuIntent);
    }

    private void prepareCategoryDetails() {
        if (selectedMenuId != null) {
            if (!selectedMenuId.isEmpty()) {
                Intent categoryDetailsIntent = new Intent(this, CategoryDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(getString(R.string.selected_menu_id), selectedMenuId);
                bundle.putString(getString(R.string.selected_category_id), selectedCategoryId);
                bundle.putString(getString(R.string.selected_category_name), selectedCategoryName);
                categoryDetailsIntent.putExtras(bundle);
                startActivity(categoryDetailsIntent);
            } else {
                Toast.makeText(this, getString(R.string.please_select_menu), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.please_select_menu), Toast.LENGTH_SHORT).show();
        }
    }

    private void prepareSubCategoryDetails() {
        if (selectedCategoryId != null) {
            if (!selectedCategoryId.isEmpty()) {
                Intent subCategoryDetailsIntent = new Intent(this, SubCategoryDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(getString(R.string.selected_category_id), selectedCategoryId);
                bundle.putString(getString(R.string.selected_category_name), selectedCategoryName);
                bundle.putString(getString(R.string.selected_sub_category_id), selectedSubCategoryId);
                bundle.putString(getString(R.string.selected_sub_category_name), selectedSubCategoryName);
                subCategoryDetailsIntent.putExtras(bundle);
                startActivity(subCategoryDetailsIntent);
            } else {
                Toast.makeText(this, getString(R.string.please_select_category), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.please_select_category), Toast.LENGTH_SHORT).show();
        }
    }

    private void prepareSubSubCategoryDetails() {
        if (selectedSubCategoryId != null) {
            if (!selectedSubCategoryId.isEmpty()) {
                Intent subSubCategoryDetailsIntent = new Intent(this, SubSubCategoryDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(getString(R.string.selected_sub_category_id), selectedSubCategoryId);
                bundle.putString(getString(R.string.sub_sub_category_id), selectedSubSubCategoryId);
                bundle.putString(getString(R.string.sub_sub_category_name), selectedSubSubCategoryName);
                subSubCategoryDetailsIntent.putExtras(bundle);
                startActivity(subSubCategoryDetailsIntent);
            } else {
                Toast.makeText(this, getString(R.string.please_select_sub_category), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.please_select_sub_category), Toast.LENGTH_SHORT).show();
        }
    }

    private void prepareBrandDetails() {
        if (selectedCategoryId != null) {
            if (!selectedCategoryId.isEmpty()) {
                Intent brandDetailsIntent = new Intent(this, BrandDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(getString(R.string.selected_category_id), selectedCategoryId);
                bundle.putString(getString(R.string.selected_brand_id), selectedBrandId);
                bundle.putString(getString(R.string.selected_brand_name), selectedBrandName);
                brandDetailsIntent.putExtras(bundle);
                startActivity(brandDetailsIntent);
            } else {
                Toast.makeText(this, getString(R.string.please_select_category), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.please_select_category), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_FOR_ADD_SAVE_PRODUCT_DETAILS:
                if (jsonResponse != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(jsonResponse);
                        boolean status = jsonObject.getBoolean("status");
                        String message = jsonObject.getString("message");
                        if (status) {
                            PreferenceConnector.writeBoolean(this, getString(R.string.refresh_products), true);
                            finish();
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    @Override
    public void selectedMenuDetails(String selectedMenuId, String selectedMenuName) {
        this.selectedMenuId = selectedMenuId;
        this.selectedMenuName = selectedMenuName;

        etMenu.setText(selectedMenuName);

        selectedCategoryId = "";
        selectedCategoryName = "";

        selectedSubCategoryId = "";
        selectedSubCategoryName = "";

        selectedSubSubCategoryId = "";
        selectedSubSubCategoryName = "";

        selectedBrandId = "";
        selectedBrandName = "";

        etCategory.setText("");
        etSubCategory.setText("");
        etSubSubCategory.setText("");
        etBrand.setText("");
        if (llAddOptionDetails != null) {
            tvAddOptionText.setVisibility(View.GONE);
            llAddOptionDetails.removeAllViewsInLayout();
        }
        if (llGenerateVariantDetails != null) {
            llGenerateVariantDetails.removeAllViewsInLayout();
        }

        listOfAddOptionDetails = new LinkedList<>();
        listOfGenerateVariantViews = new LinkedList<>();
        listOfProductOptionViews = new LinkedList<>();
        listOfTempGenerateVariantItems = new LinkedList<>();
        listOfTempGenerateVariantItemsIds = new LinkedList<>();
    }

    @Override
    public void selectedCategoryDetails(String selectedCategoryId, String selectedCategoryName) {
        this.selectedCategoryId = selectedCategoryId;
        this.selectedCategoryName = selectedCategoryName;

        etCategory.setText(selectedCategoryName);
        etSubCategory.setText("");
        etSubSubCategory.setText("");
        etBrand.setText("");

        selectedSubCategoryId = "";
        selectedSubCategoryName = "";

        selectedSubSubCategoryId = "";
        selectedSubSubCategoryName = "";

        selectedBrandId = "";
        selectedBrandName = "";
    }

    @Override
    public void selectedSubCategoryDetails(String selectedSubCategoryId, String selectedSubCategoryName) {
        this.selectedSubCategoryId = selectedSubCategoryId;
        this.selectedSubCategoryName = selectedSubCategoryName;

        etSubCategory.setText(selectedSubCategoryName);
        etSubSubCategory.setText("");

        selectedSubSubCategoryId = "";
        selectedSubSubCategoryName = "";
    }

    @Override
    public void selectedSubSubCategoryDetails(String selectedSubSubCategoryId, String selectedSubSubCategoryName) {
        this.selectedSubSubCategoryId = selectedSubSubCategoryId;
        this.selectedSubSubCategoryName = selectedSubSubCategoryName;

        etSubSubCategory.setText(selectedSubSubCategoryName);
    }

    @Override
    public void selectedBrandDetails(String selectedBrandId, String selectedBrandName) {
        this.selectedBrandId = selectedBrandId;
        this.selectedBrandName = selectedBrandName;

        etBrand.setText(selectedBrandName);
    }

    @Override
    public void selectedProductOptionDetails(String selectedProductOptionId, String selectedProductOptionName) {

        View view = listOfProductOptionViews.get(selectedOptionPosition);
        TextView tvOptions = view.findViewById(R.id.tvOptions);
        tvOptions.setText(selectedProductOptionName);

        if (listOfAddOptionDetails != null) {
            if (listOfAddOptionDetails.size() != 0) {
                if (listOfAddOptionDetails.size() > selectedOptionPosition) {
                    AddOptionDetails addOptionDetails = listOfAddOptionDetails.get(selectedOptionPosition);
                    addOptionDetails.setOptionId(selectedProductOptionId);
                    addOptionDetails.setOptionName(selectedProductOptionName);
                } else {
                    AddOptionDetails addOptionDetails = new AddOptionDetails();
                    addOptionDetails.setOptionId(selectedProductOptionId);
                    addOptionDetails.setOptionName(selectedProductOptionName);
                    listOfAddOptionDetails.add(addOptionDetails);
                }
            } else {
                AddOptionDetails addOptionDetails = new AddOptionDetails();
                addOptionDetails.setOptionId(selectedProductOptionId);
                addOptionDetails.setOptionName(selectedProductOptionName);
                listOfAddOptionDetails.add(addOptionDetails);
            }
        } else {
            AddOptionDetails addOptionDetails = new AddOptionDetails();
            addOptionDetails.setOptionId(selectedProductOptionId);
            addOptionDetails.setOptionName(selectedProductOptionName);
            listOfAddOptionDetails.add(addOptionDetails);
        }
    }

    @Override
    public void selectedProductGroupDetails(String selectedProductGroupId, String selectedProductGroupName) {

        View view = listOfProductOptionViews.get(selectedGroupPosition);
        TextView tvGroups = view.findViewById(R.id.tvGroups);
        tvGroups.setText(selectedProductGroupName);

        if (listOfAddOptionDetails != null) {
            if (listOfAddOptionDetails.size() != 0) {
                if (listOfAddOptionDetails.size() > selectedGroupPosition) {
                    AddOptionDetails addOptionDetails = listOfAddOptionDetails.get(selectedGroupPosition);
                    addOptionDetails.setGroupId(selectedProductGroupId);
                    addOptionDetails.setGroupName(selectedProductGroupName);
                } else {
                    AddOptionDetails addOptionDetails = new AddOptionDetails();
                    addOptionDetails.setGroupId(selectedProductGroupId);
                    addOptionDetails.setGroupName(selectedProductGroupName);
                    listOfAddOptionDetails.add(addOptionDetails);
                }
            } else {
                AddOptionDetails addOptionDetails = new AddOptionDetails();
                addOptionDetails.setGroupId(selectedProductGroupId);
                addOptionDetails.setGroupName(selectedProductGroupName);
                listOfAddOptionDetails.add(addOptionDetails);
            }
        } else {
            AddOptionDetails addOptionDetails = new AddOptionDetails();
            addOptionDetails.setGroupId(selectedProductGroupId);
            addOptionDetails.setGroupName(selectedProductGroupName);
            listOfAddOptionDetails.add(addOptionDetails);
        }
    }

    @Override
    public void selectedItemsDetails(String selectedItemIds, String selectedItemNames) {
        View view = listOfProductOptionViews.get(selectedItemPosition);
        TextView tvItems = view.findViewById(R.id.tvItems);
        tvItems.setText(selectedItemNames);

        if (listOfAddOptionDetails != null) {
            if (listOfAddOptionDetails.size() != 0) {
                if (listOfAddOptionDetails.size() > selectedItemPosition) {
                    AddOptionDetails addOptionDetails = listOfAddOptionDetails.get(selectedItemPosition);
                    addOptionDetails.setItemIds(selectedItemIds);
                    addOptionDetails.setItemNames(selectedItemNames);
                } else {
                    AddOptionDetails addOptionDetails = new AddOptionDetails();
                    addOptionDetails.setItemIds(selectedItemIds);
                    addOptionDetails.setItemNames(selectedItemNames);
                    listOfAddOptionDetails.add(addOptionDetails);
                }
            } else {
                AddOptionDetails addOptionDetails = new AddOptionDetails();
                addOptionDetails.setItemIds(selectedItemIds);
                addOptionDetails.setItemNames(selectedItemNames);
                listOfAddOptionDetails.add(addOptionDetails);
            }
        } else {
            AddOptionDetails addOptionDetails = new AddOptionDetails();
            addOptionDetails.setItemIds(selectedItemIds);
            addOptionDetails.setItemNames(selectedItemNames);
            listOfAddOptionDetails.add(addOptionDetails);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (permissions.length != 0 && grantResults.length != 0) {
            switch (requestCode) {
                case Constants.REQUEST_CODE_FOR_CAMERA_PERMISSION:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        prepareAddProductPic();
                    } else {
                        prepareAddProductPic();
                    }
                    break;
                case Constants.REQUEST_CODE_FOR_EXTERNAL_STORAGE_PERMISSION:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        prepareAddProductPic();
                    } else {
                        prepareAddProductPic();
                    }
                    break;
                default:
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultIntent) {
        super.onActivityResult(requestCode, resultCode, resultIntent);
        switch (requestCode) {
            case Constants.PICK_GALLERY:
                listOfPhotoPaths = new ArrayList<>();
                if (resultIntent != null) {
                    if (resultCode == Activity.RESULT_OK) {
                        listOfPhotoPaths.addAll(resultIntent.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
                        listOfSelectedPhotoPaths.addAll(listOfPhotoPaths);
                        rvProductImageDetails.setVisibility(View.VISIBLE);
                        if (addProductImageDetailsAdapter == null) {
                            initializeProductImageDetailsAdapter();
                        } else {
                            addProductImageDetailsAdapter.updateImages(listOfSelectedPhotoPaths);
                        }
                    }
                }
                break;
            case Constants.REQUEST_CODE_CAPTURE_IMAGE:
                listOfPhotoPaths = new ArrayList<>();
                List<Image> listOfImages = ImagePicker.getImages(resultIntent);
                if (listOfImages != null) {
                    if (listOfImages.size() != 0) {
                        String selectedImageUrl = listOfImages.get(0).getPath();
                        listOfPhotoPaths.add(selectedImageUrl);
                        listOfSelectedPhotoPaths.add(selectedImageUrl);
                        rvProductImageDetails.setVisibility(View.VISIBLE);
                        if (addProductImageDetailsAdapter == null) {
                            initializeProductImageDetailsAdapter();
                        } else {
                            addProductImageDetailsAdapter.updateImages(listOfSelectedPhotoPaths);
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    private void initializeProductImageDetailsAdapter() {
        rvProductImageDetails.setVisibility(View.VISIBLE);
        addProductImageDetailsAdapter = new AddProductImageDetailsAdapter(this, listOfSelectedPhotoPaths);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 4);
        rvProductImageDetails.setLayoutManager(layoutManager);
        rvProductImageDetails.setItemAnimator(new DefaultItemAnimator());
        addProductImageDetailsAdapter.setClickListener(new AddProductImageDetailsAdapter.ItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                listOfSelectedPhotoPaths.remove(position);
                if (listOfSelectedPhotoPaths != null) {
                    if (listOfSelectedPhotoPaths.size() != 0) {
                        rvProductImageDetails.setVisibility(View.VISIBLE);
                        if (addProductImageDetailsAdapter != null) {
                            addProductImageDetailsAdapter.updateImages(listOfSelectedPhotoPaths);
                        }
                    } else {
                        rvProductImageDetails.setVisibility(View.GONE);
                    }
                } else {
                    rvProductImageDetails.setVisibility(View.GONE);
                }
            }
        });
        rvProductImageDetails.setAdapter(addProductImageDetailsAdapter);
    }
}
