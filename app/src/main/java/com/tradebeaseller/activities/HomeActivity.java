package com.tradebeaseller.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.tradebeaseller.ApiCalls.GetProfileDetailsApiCall;
import com.tradebeaseller.R;
import com.tradebeaseller.adapters.NavigationMenuAdapter;
import com.tradebeaseller.fragments.HomeFragment;
import com.tradebeaseller.fragments.OrdersFragment;
import com.tradebeaseller.fragments.PaymentsFragment;
import com.tradebeaseller.fragments.ProductsFragment;
import com.tradebeaseller.fragments.ReturnsFragment;
import com.tradebeaseller.fragments.SupportFragment;
import com.tradebeaseller.fragments.WalletFragment;
import com.tradebeaseller.interfaces.HomeNavigationCallBack;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.models.NavigationMenuItems;
import com.tradebeaseller.models.responseModels.profileResponse.GetProfileData;
import com.tradebeaseller.models.responseModels.profileResponse.ProfileResponse;
import com.tradebeaseller.models.responseModels.profileResponse.UserDetails;
import com.tradebeaseller.utils.Constants;
import com.tradebeaseller.utils.PreferenceConnector;

import java.util.LinkedList;


public class HomeActivity extends BaseActivity implements View.OnClickListener, NavigationMenuAdapter.ItemClickListener, HttpReqResCallBack, HomeNavigationCallBack {

    private TextView tvTitle;
    private Fragment fragment;
    private RecyclerView rvMenu;
    private DrawerLayout drawerLayout;
    private ImageView ivMenuNav, ivNotification;
    public ImageView ivFilter;

    private LinkedList<NavigationMenuItems> listOfNavigationMenuItems;

    private String token = "";
    private String userName = "";
    private String userUniqueId = "";
    private String userImageUrl = "";

    private Long mobileNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_home);
        initializeUi();
        initializeListeners();
        getUserProfile();
    }


    private void initializeUi() {
        rvMenu = findViewById(R.id.rvMenu);
        tvTitle = findViewById(R.id.tvTitle);
        ivFilter = findViewById(R.id.ivFilter);
        ivMenuNav = findViewById(R.id.ivMenuNav);
        drawerLayout = findViewById(R.id.drawerLayout);
        ivNotification = findViewById(R.id.ivNotification);
        token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
    }


    private void initializeListeners() {
        ivMenuNav.setOnClickListener(this);
        ivNotification.setOnClickListener(this);
    }

    private void prepareDetails() {
        listOfNavigationMenuItems = new LinkedList<>();
        listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_home, getResources().getString(R.string.home)));
        listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_orders, getResources().getString(R.string.orders)));
        listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_products, getResources().getString(R.string.products)));
        /*listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_reports, getResources().getString(R.string.reports)));*/
        listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_payment, getResources().getString(R.string.payments)));
        listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_return, getResources().getString(R.string.returns)));
        listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_my_wallet, getString(R.string.wallet)));
        listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_support, getResources().getString(R.string.support)));
        listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_logout, getResources().getString(R.string.logout)));
        initializeAdapter();
        onItemClick(null, 1);
    }

    private void initializeAdapter() {
        NavigationMenuAdapter navigationMenuAdapter = new NavigationMenuAdapter(HomeActivity.this, listOfNavigationMenuItems, userName, userImageUrl, userUniqueId, mobileNumber);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvMenu.setLayoutManager(layoutManager);
        rvMenu.setItemAnimator(new DefaultItemAnimator());
        navigationMenuAdapter.setClickListener(this);
        rvMenu.setAdapter(navigationMenuAdapter);
    }

    private void getUserProfile() {
        showProgressBar(this);
        GetProfileDetailsApiCall.serviceCallToGetProfileDetails(this, null, null, token);
    }


    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivMenuNav) {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            } else
                drawerLayout.openDrawer(GravityCompat.START);
        } else if (id == R.id.ivNotification) {
            goToNotifications();
        }
    }

    private void goToNotifications() {
        Intent notificationsIntent = new Intent(this, NotificationsActivity.class);
        startActivity(notificationsIntent);
    }

    @Override
    public void onItemClick(View view, int position) {
        if (position != 0) {
            ivFilter.setVisibility(View.GONE);
            NavigationMenuItems navigationMenuItems = listOfNavigationMenuItems.get(position - 1);
            String menuTitle = navigationMenuItems.getMenuItemName();
            if (menuTitle.equalsIgnoreCase(getString(R.string.home))) {
                tvTitle.setText(menuTitle);
                fragment = new HomeFragment();
            } else if (menuTitle.equalsIgnoreCase(getString(R.string.orders))) {
                tvTitle.setText(menuTitle);
                ivFilter.setVisibility(View.VISIBLE);
                fragment = new OrdersFragment();
                //((OrdersFragment) fragment).ivFilter = ivFilter;
            } else if (menuTitle.equalsIgnoreCase(getString(R.string.products))) {
                tvTitle.setText(menuTitle);
                fragment = new ProductsFragment();
            } /*else if (menuTitle.equalsIgnoreCase(getString(R.string.reports))) {
                tvTitle.setText(menuTitle);
                fragment = new ReportsFragment();
            }*/ else if (menuTitle.equalsIgnoreCase(getString(R.string.payments))) {
                tvTitle.setText(menuTitle);
                fragment = new PaymentsFragment();
            } else if (menuTitle.equalsIgnoreCase(getString(R.string.returns))) {
                tvTitle.setText(menuTitle);
                fragment = new ReturnsFragment();
            } else if (menuTitle.equalsIgnoreCase(getString(R.string.support))) {
                tvTitle.setText(menuTitle);
                fragment = new SupportFragment();
            } else if (menuTitle.equalsIgnoreCase(getString(R.string.wallet))) {
                tvTitle.setText(menuTitle);
                fragment = new WalletFragment();
            } else {
                showLogoutAlertDialog();
            }
            if (fragment != null) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.detailsFragment, fragment);
                fragmentTransaction.setTransitionStyle(R.style.Theme_Transparent);
                fragmentTransaction.commitAllowingStateLoss();
            }

            if (view != null) {
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                } else
                    drawerLayout.openDrawer(GravityCompat.START);
            }
        } else {
            goToEditProfile();
        }
    }

    private void goToEditProfile() {
        Intent editProfileIntent = new Intent(this, EditProfileActivity.class);
        startActivity(editProfileIntent);
    }

    private void showLogoutAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        alertDialogBuilder.setMessage(getString(R.string.are_you_sure_do_you_want_to_logout));
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                clearDataInPreferences();
                goToLogin();
                dialog.cancel();
            }
        });
        alertDialogBuilder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void goToLogin() {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(loginIntent);
    }

    private void clearDataInPreferences() {
        PreferenceConnector.clearData(this);
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_PROFILE_DETAILS:
                if (jsonResponse != null) {
                    try {
                        ProfileResponse profileResponse = new Gson().fromJson(jsonResponse, ProfileResponse.class);
                        if (profileResponse != null) {
                            boolean status = profileResponse.getStatus();
                            String message = profileResponse.getMessage();
                            if (status) {
                                GetProfileData getProfileData = profileResponse.getGetProfileData();
                                if (getProfileData != null) {
                                    boolean isAdmin = getProfileData.isAdmin();
                                    PreferenceConnector.writeBoolean(this, getString(R.string.is_admin), isAdmin);
                                    UserDetails userDetails = getProfileData.getUserDetails();
                                    if (userDetails != null) {
                                        userName = userDetails.getFirstName();
                                        userUniqueId = userDetails.getUniqueId();
                                        userImageUrl = userDetails.getProfileImage();
                                        mobileNumber = userDetails.getMobile();
                                    }
                                }
                            } else {
                                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
                prepareDetails();
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        exitMaterialAlert();
    }

    private void exitMaterialAlert() {
        androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(this, R.style.MyDialogTheme);
        alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));
        alertDialogBuilder.setMessage(getResources().getString(R.string.are_you_sure_do_you_want_to_exit));
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        androidx.appcompat.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void homeNavigationCallBack(String goTo) {
        if (goTo.equalsIgnoreCase(getString(R.string.orders))) {
            onItemClick(null, 2);
        }
    }
}
