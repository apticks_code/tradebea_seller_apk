package com.tradebeaseller.models.requestModels.orderDetailsRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderDetailsRequest {

    @SerializedName("start_date")
    @Expose
    public String startDate;
    @SerializedName("end_date")
    @Expose
    public String endDate;
    @SerializedName("last_days")
    @Expose
    public String lastDays;
    @SerializedName("last_years")
    @Expose
    public String lastYears;
    @SerializedName("status")
    @Expose
    public int status;
}
