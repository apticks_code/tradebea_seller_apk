package com.tradebeaseller.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tradebeaseller.ApiCalls.OrderAcceptApiCall;
import com.tradebeaseller.ApiCalls.OrderRejectApiCall;
import com.tradebeaseller.ApiCalls.SelectedOrderDetailsApiCall;
import com.tradebeaseller.R;
import com.tradebeaseller.adapters.SelectedOrderDetailsAdapter;
import com.tradebeaseller.interfaces.CloseCallBack;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.models.responseModels.selectedOrderDetailsResponse.CustomerAppStatus;
import com.tradebeaseller.models.responseModels.selectedOrderDetailsResponse.OrderDetails;
import com.tradebeaseller.models.responseModels.selectedOrderDetailsResponse.Rules;
import com.tradebeaseller.models.responseModels.selectedOrderDetailsResponse.SelectedOrderData;
import com.tradebeaseller.models.responseModels.selectedOrderDetailsResponse.SelectedOrderDetailsResponse;
import com.tradebeaseller.utils.Constants;
import com.tradebeaseller.utils.PreferenceConnector;
import com.tradebeaseller.utils.UserData;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class SelectedOrderDetailsActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack, CloseCallBack {

    private ImageView ivBackArrow;
    private AlertDialog alertDialog;
    private RecyclerView rvOrderDetails;
    private SelectedOrderData selectedOrderData;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout llOrderDetails, llPriceDetails, llAcceptRejectStatus, llOutForDelivery;
    private TextView tvOTP, tvAccept, tvReject, tvTotalItems, tvTotalPrice, tvPromoDiscount, tvDiscount, tvSubTotal, tvTax, tvGrandTotal, tvError, tvPreparationTime, tvOutForDelivery;

    private LinkedList<OrderDetails> listOfOrderDetails;

    private String token = "";
    private String orderId = "";
    private String preparationTime = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_selected_order_details);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();
        refreshContent();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.order_id)))
                orderId = bundle.getString(getString(R.string.order_id));
        }
    }

    private void initializeUi() {
        tvTax = findViewById(R.id.tvTax);
        tvOTP = findViewById(R.id.tvOTP);
        tvError = findViewById(R.id.tvError);
        tvAccept = findViewById(R.id.tvAccept);
        tvReject = findViewById(R.id.tvReject);
        tvDiscount = findViewById(R.id.tvDiscount);
        tvSubTotal = findViewById(R.id.tvSubTotal);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        tvTotalItems = findViewById(R.id.tvTotalItems);
        tvTotalPrice = findViewById(R.id.tvTotalPrice);
        tvGrandTotal = findViewById(R.id.tvGrandTotal);
        llPriceDetails = findViewById(R.id.llPriceDetails);
        llOrderDetails = findViewById(R.id.llOrderDetails);
        rvOrderDetails = findViewById(R.id.rvOrderDetails);
        tvPromoDiscount = findViewById(R.id.tvPromoDiscount);
        llOutForDelivery = findViewById(R.id.llOutForDelivery);
        tvOutForDelivery = findViewById(R.id.tvOutForDelivery);
        tvPreparationTime = findViewById(R.id.tvPreparationTime);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        llAcceptRejectStatus = findViewById(R.id.llAcceptRejectStatus);

        UserData.getInstance().setSelectedOrderDetailsContext(this);
    }

    private void initializeListeners() {
        tvAccept.setOnClickListener(this);
        tvReject.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);
        tvOutForDelivery.setOnClickListener(this);
    }

    private void prepareDetails() {
        showProgressBar(this);
        token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
        SelectedOrderDetailsApiCall.serviceCallForSelectedOrderDetails(this, null, null, orderId, token);
    }

    private void refreshContent() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                prepareDetails();
            }
        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivBackArrow) {
            onBackPressed();
        } else if (id == R.id.tvAccept) {
            prepareAcceptDetails();
        } else if (id == R.id.tvReject) {
            showRejectAlertDialog();
        } else if (id == R.id.tvOutForDelivery) {
            prepareOutForDelivery();
        }
    }

    private void prepareOutForDelivery() {
        Intent verifyDeliveryBoyOTPIntent = new Intent(this, VerifyDeliveryBoyOTPActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(getString(R.string.selected_order_id), orderId);
        verifyDeliveryBoyOTPIntent.putExtras(bundle);
        startActivity(verifyDeliveryBoyOTPIntent);
    }

    private void prepareAcceptDetails() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        alertDialogBuilder.setCancelable(false);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_preparation_time, null);
        alertDialogBuilder.setView(view);
        EditText etPreparationTime = view.findViewById(R.id.etPreparationTime);
        etPreparationTime.setText(preparationTime);
        TextView tvSubmit = view.findViewById(R.id.tvSubmit);
        tvSubmit.setOnClickListener(this);
        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preparationTime = etPreparationTime.getText().toString();
                if (!preparationTime.isEmpty()) {
                    showProgressBar(SelectedOrderDetailsActivity.this);
                    String token = PreferenceConnector.readString(SelectedOrderDetailsActivity.this, getString(R.string.user_token), "");
                    OrderAcceptApiCall.serviceCallForOrderAccept(SelectedOrderDetailsActivity.this, null, null, Integer.parseInt(orderId), token, preparationTime);
                } else {
                    Toast.makeText(SelectedOrderDetailsActivity.this, getString(R.string.please_enter_preparation_time), Toast.LENGTH_SHORT).show();
                }
                alertDialog.dismiss();
            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void showRejectAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        alertDialogBuilder.setTitle(getString(R.string.order_rejected_message));
        alertDialogBuilder.setCancelable(false);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.layout_order_reject_dialog, null);
        alertDialogBuilder.setView(view);
        EditText etRejectMessage = view.findViewById(R.id.etRejectMessage);
        alertDialogBuilder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                prepareRejectedDetails(orderId, etRejectMessage.getText().toString());
                dialog.cancel();
            }
        });
        alertDialogBuilder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void prepareRejectedDetails(String orderId, String rejectOrderMessage) {
        if (!rejectOrderMessage.isEmpty()) {
            showProgressBar(this);
            String token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
            OrderRejectApiCall.serviceCallForOrderReject(this, null, null, Integer.parseInt(orderId), token, rejectOrderMessage);
        } else {
            Toast.makeText(this, this.getString(R.string.please_enter_reject_message), Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_SELECTED_ORDER_DETAILS:
                if (jsonResponse != null) {
                    SelectedOrderDetailsResponse selectedOrderDetailsResponse = new Gson().fromJson(jsonResponse, SelectedOrderDetailsResponse.class);
                    if (selectedOrderDetailsResponse != null) {
                        boolean status = selectedOrderDetailsResponse.getStatus();
                        if (status) {
                            selectedOrderData = selectedOrderDetailsResponse.getSelectedOrderData();
                            if (selectedOrderData != null) {
                                String orderPickUpOTP = selectedOrderData.getOrderPickupOtp();
                                if (orderPickUpOTP != null) {
                                    if (!orderPickUpOTP.isEmpty()) {
                                        tvOTP.setText("Pickup OTP" + " " + ":" + " " + orderPickUpOTP);
                                    } else {
                                        tvOTP.setText("Pickup OTP" + " " + ":" + " " + "N/A");
                                    }
                                } else {
                                    tvOTP.setText("Pickup OTP" + " " + ":" + " " + "N/A");
                                }
                                listOfOrderDetails = selectedOrderData.getListOfOrderDetails();
                                if (selectedOrderData.getOrderStatus() != null) {
                                    Integer orderStatus = selectedOrderData.getOrderStatus();
                                    if (orderStatus == 4) {
                                        llAcceptRejectStatus.setVisibility(View.GONE);
                                        llOutForDelivery.setVisibility(View.GONE);
                                        tvPreparationTime.setVisibility(View.GONE);
                                    }
                                }
                                LinkedList<CustomerAppStatus> listOfOrderStatus = selectedOrderData.getListOfCustomerAppStatus();
                                preparationTime = selectedOrderData.getPreparationTime();

                                if (listOfOrderDetails != null) {
                                    if (listOfOrderDetails.size() != 0) {
                                        OrderDetails orderDetails = listOfOrderDetails.get(0);
                                        Rules rules = orderDetails.getRules();
                                        if (rules != null) {
                                            if (rules.getDefaultPreparationTime() != null) {
                                                preparationTime = rules.getDefaultPreparationTime().toString();
                                            }
                                        }
                                        listIsFull();
                                        preparePriceDetails();
                                        initializeAdapter();
                                    } else {
                                        listIsEmpty();
                                    }
                                } else {
                                    listIsEmpty();
                                }

                                if (listOfOrderStatus != null) {
                                    if (listOfOrderStatus.size() != 0) {
                                        for (int index = 0; index < listOfOrderStatus.size(); index++) {
                                            CustomerAppStatus orderStatus = listOfOrderStatus.get(index);
                                            String name = orderStatus.getName();
                                            String createdDate = orderStatus.getCreatedAt();
                                            if (name.equalsIgnoreCase("Accepted")) {
                                                if (createdDate != null) {
                                                    llOutForDelivery.setVisibility(View.VISIBLE);
                                                    llAcceptRejectStatus.setVisibility(View.GONE);

                                                    if (preparationTime != null) {
                                                        if (!preparationTime.isEmpty()) {

                                                            //createdDate = "2021-11-13 19:40:22";
                                                            SimpleDateFormat createdDateSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
                                                            try {
                                                                Date date = createdDateSimpleDateFormat.parse(createdDate);

                                                                //added one hour to createdDate
                                                                Date preparationTimeDate = createdDateSimpleDateFormat.parse(createdDate);
                                                                Calendar preparationTimeCalendar = Calendar.getInstance();
                                                                preparationTimeCalendar.setTime(preparationTimeDate);
                                                                if (preparationTime != null) {
                                                                    if (!preparationTime.isEmpty()) {
                                                                        preparationTimeCalendar.add(Calendar.HOUR, Integer.parseInt(preparationTime));
                                                                    }
                                                                }

                                                                //current time
                                                                Calendar calendar = Calendar.getInstance();

                                                                long diff = preparationTimeCalendar.getTimeInMillis() - calendar.getTimeInMillis();
                                                                long seconds = diff / 1000;
                                                                long minutes = seconds / 60;

                                                                if (calendar.getTimeInMillis() <= preparationTimeCalendar.getTimeInMillis()) {
                                                                    if (minutes > 0) {
                                                                        new CountDownTimer(TimeUnit.MINUTES.toMillis(Integer.parseInt(String.valueOf(minutes)))
                                                                                , 1000) { // adjust the milli seconds here
                                                                            public void onTick(long millisUntilFinished) {
                                                                                String time = String.format("%d hr,%d min, %d sec",
                                                                                        TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                                                                                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                                                                                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                                                                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
                                                                                tvPreparationTime.setText(getString(R.string.preparation_time) + " " + time);
                                                                            }

                                                                            public void onFinish() {
                                                                                tvPreparationTime.setText(getString(R.string.preparation_time) + " " + "Finished");
                                                                            }
                                                                        }.start();
                                                                    }
                                                                }
                                                            } catch (ParseException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    }

                                                } else {
                                                    llAcceptRejectStatus.setVisibility(View.VISIBLE);
                                                    llOutForDelivery.setVisibility(View.GONE);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_FOR_ORDER_ACCEPT_DETAILS:
                if (jsonResponse != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(jsonResponse);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            llAcceptRejectStatus.setVisibility(View.GONE);
                            llOutForDelivery.setVisibility(View.VISIBLE);
                            if (preparationTime != null) {
                                if (!preparationTime.isEmpty()) {
                                    int totalMinutes = Integer.parseInt(preparationTime) * 60;
                                    new CountDownTimer(TimeUnit.MINUTES.toMillis(totalMinutes)
                                            , 1000) { // adjust the milli seconds here

                                        public void onTick(long millisUntilFinished) {
                                            String time = String.format("%d hr,%d min, %d sec",
                                                    TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                                                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
                                            tvPreparationTime.setText(getString(R.string.preparation_time) + " " + time);
                                        }

                                        public void onFinish() {
                                            tvPreparationTime.setText(getString(R.string.preparation_time) + " " + "Finished");
                                        }
                                    }.start();
                                }
                            }
                            Toast.makeText(this, "Order Accepted", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_FOR_ORDER_REJECT_DETAILS:
                if (jsonResponse != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(jsonResponse);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            finish();
                            PreferenceConnector.writeBoolean(this, getString(R.string.order_status_changed), true);
                            Toast.makeText(this, "Order Rejected", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    private void preparePriceDetails() {
        int promoDiscount = 0;
        int totalPrice = 0;
        int subTotalPrice = 0;
        int discount = 0;
        int tax = 0;
        OrderDetails orderDetails = listOfOrderDetails.get(0);
        if (orderDetails.getTotal() != null)
            totalPrice = orderDetails.getTotal();
        if (orderDetails.getSubTotal() != null)
            subTotalPrice = orderDetails.getSubTotal();
        if (orderDetails.getDiscount() != null)
            discount = orderDetails.getDiscount();
        if (orderDetails.getPromoDiscount() != null)
            promoDiscount = orderDetails.getPromoDiscount();
        if (orderDetails.getTax() != null)
            tax = orderDetails.getTax();
        tvTotalItems.setText(getString(R.string.total_items) + "  " + " - " + "  " + listOfOrderDetails.size() + " " + "Items");

        tvPromoDiscount.setText("(-)" + getString(R.string.current_currency) + " " + promoDiscount + "/-");
        tvDiscount.setText("(-)" + getString(R.string.current_currency) + " " + discount + "/-");
        tvSubTotal.setText(getString(R.string.current_currency) + " " + subTotalPrice + "/-");
        tvTax.setText(getString(R.string.current_currency) + " " + tax + "/-");
        tvGrandTotal.setText(getString(R.string.current_currency) + " " + totalPrice + "/-");
        //tvTotalPrice.setText();
    }

    private void initializeAdapter() {
        SelectedOrderDetailsAdapter selectedOrderDetailsAdapter = new SelectedOrderDetailsAdapter(this, listOfOrderDetails);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvOrderDetails.setLayoutManager(layoutManager);
        rvOrderDetails.setItemAnimator(new DefaultItemAnimator());
        rvOrderDetails.setAdapter(selectedOrderDetailsAdapter);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        llOrderDetails.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        llOrderDetails.setVisibility(View.GONE);
    }

    @Override
    public void close() {
        prepareDetails();
    }
}
