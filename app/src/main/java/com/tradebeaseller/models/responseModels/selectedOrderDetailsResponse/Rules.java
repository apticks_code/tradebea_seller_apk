package com.tradebeaseller.models.responseModels.selectedOrderDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rules {
    @SerializedName("default_preparation_time_in_hrs")
    @Expose
    private Integer defaultPreparationTime;
    @SerializedName("cancel_before_in_hrs")
    @Expose
    private Integer cancelBeforeInHrs;
    @SerializedName("default_return_poilcy")
    @Expose
    private DefaultReturnPoilcy defaultReturnPoilcy;


    public Integer getDefaultPreparationTime() {
        return defaultPreparationTime;
    }

    public void setDefaultPreparationTime(Integer defaultPreparationTime) {
        this.defaultPreparationTime = defaultPreparationTime;
    }

    public Integer getCancelBeforeInHrs() {
        return cancelBeforeInHrs;
    }

    public void setCancelBeforeInHrs(Integer cancelBeforeInHrs) {
        this.cancelBeforeInHrs = cancelBeforeInHrs;
    }

    public DefaultReturnPoilcy getDefaultReturnPoilcy() {
        return defaultReturnPoilcy;
    }

    public void setDefaultReturnPoilcy(DefaultReturnPoilcy defaultReturnPoilcy) {
        this.defaultReturnPoilcy = defaultReturnPoilcy;
    }
}
