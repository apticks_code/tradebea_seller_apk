package com.tradebeaseller.models.responseModels.editProductResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OptionValues {

    @SerializedName("option")
    @Expose
    private Option option;
    @SerializedName("option_group")
    @Expose
    private OptionGroup optionGroup;
    @SerializedName("option_item")
    @Expose
    private OptionItem optionItem;


    public Option getOption() {
        return option;
    }

    public void setOption(Option option) {
        this.option = option;
    }

    public OptionGroup getOptionGroup() {
        return optionGroup;
    }

    public void setOptionGroup(OptionGroup optionGroup) {
        this.optionGroup = optionGroup;
    }

    public OptionItem getOptionItem() {
        return optionItem;
    }

    public void setOptionItem(OptionItem optionItem) {
        this.optionItem = optionItem;
    }
}
