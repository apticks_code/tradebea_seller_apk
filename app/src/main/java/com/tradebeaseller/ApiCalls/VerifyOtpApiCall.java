package com.tradebeaseller.ApiCalls;

import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeaseller.activities.OTPVerificationActivity;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.jsonBuilderParser.JsonBuilderParser;
import com.tradebeaseller.models.requestModels.generateOTPRequest.GenerateOTPRequest;
import com.tradebeaseller.models.requestModels.verifyOTPRequest.VerifyOTpRequest;
import com.tradebeaseller.utils.CommonMethods;
import com.tradebeaseller.utils.Constants;

import org.json.JSONObject;

public class VerifyOtpApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallForVerifyOTP(Context context, String mobileNumber, String enteredOtp) {
        String url = Constants.VERIFY_OTP_URL;
        VerifyOTpRequest verifyOTpRequest = new VerifyOTpRequest();
        verifyOTpRequest.mobile = mobileNumber;
        verifyOTpRequest.otp = enteredOtp;
        JSONObject postObject = JsonBuilderParser.jsonBuilder(verifyOTpRequest);

        AndroidNetworking.post(url)
                .addJSONObjectBody(postObject) // posting json
                .setPriority(Priority.HIGH)
                .addHeaders("APP_id", Constants.APP_ID)
                .addHeaders("Content-Type", "application/json")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_VERIFY_OTP);
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_VERIFY_OTP);
                    }
                });
    }
}
