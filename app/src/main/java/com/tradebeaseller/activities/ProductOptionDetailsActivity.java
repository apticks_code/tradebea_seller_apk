package com.tradebeaseller.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.tradebeaseller.ApiCalls.OptionsApiCall;
import com.tradebeaseller.R;
import com.tradebeaseller.adapters.ProductOptionDetailsAdapter;
import com.tradebeaseller.interfaces.CloseCallBack;
import com.tradebeaseller.interfaces.HttpReqResCallBack;
import com.tradebeaseller.models.responseModels.optionDetailsResponse.OptionDetailsResponse;
import com.tradebeaseller.models.responseModels.optionDetailsResponse.OptionsData;
import com.tradebeaseller.utils.Constants;

import java.util.LinkedList;

public class ProductOptionDetailsActivity extends BaseActivity implements HttpReqResCallBack, View.OnClickListener, CloseCallBack {

    private ImageView ivBackArrow;
    private TextView tvSave, tvError;
    private RecyclerView rvProductOptionsDetails;

    private LinkedList<OptionsData> listOfOptionsData;

    private String selectedCategoryId = "";
    private String selectedProductOptionId = "";
    private String selectedProductOptionName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_product_option_details);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.selected_category_id)))
                selectedCategoryId = bundle.getString(getString(R.string.selected_category_id), "");
            if (bundle.containsKey(getString(R.string.selected_product_option_id)))
                selectedProductOptionId = bundle.getString(getString(R.string.selected_product_option_id), "");
            if (bundle.containsKey(getString(R.string.selected_product_option_name)))
                selectedProductOptionName = bundle.getString(getString(R.string.selected_product_option_name), "");
        }
    }

    private void initializeUi() {
        tvSave = findViewById(R.id.tvSave);
        tvError = findViewById(R.id.tvError);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        rvProductOptionsDetails = findViewById(R.id.rvProductOptionsDetails);
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareDetails() {
        showProgressBar(this);
        OptionsApiCall.serviceCallToGetOptionsDetails(this, null, null, selectedCategoryId);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_OPTIONS_DETAILS:
                if (jsonResponse != null) {
                    OptionDetailsResponse optionDetailsResponse = new Gson().fromJson(jsonResponse, OptionDetailsResponse.class);
                    if (optionDetailsResponse != null) {
                        boolean status = optionDetailsResponse.getStatus();
                        if (status) {
                            listOfOptionsData = optionDetailsResponse.getListOfOptionsData();
                            if (listOfOptionsData != null) {
                                if (listOfOptionsData.size() != 0) {
                                    listIsFull();
                                    initializeAdapter();
                                } else {
                                    listIsEmpty();
                                }
                            } else {
                                listIsEmpty();
                            }
                        }
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    private void initializeAdapter() {
        if(listOfOptionsData.size()>0) {
            ProductOptionDetailsAdapter productOptionDetailsAdapter = new ProductOptionDetailsAdapter(this, listOfOptionsData, selectedProductOptionId, selectedProductOptionName, tvSave);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            rvProductOptionsDetails.setLayoutManager(layoutManager);
            rvProductOptionsDetails.setItemAnimator(new DefaultItemAnimator());
            rvProductOptionsDetails.setAdapter(productOptionDetailsAdapter);
        }
        else{
            Toast.makeText(this, "No options available for selected category", Toast.LENGTH_SHORT).show();
        }
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvProductOptionsDetails.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvProductOptionsDetails.setVisibility(View.GONE);
    }

    @Override
    public void close() {
        finish();
    }
}
