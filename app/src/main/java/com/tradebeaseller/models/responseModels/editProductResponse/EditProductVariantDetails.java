package com.tradebeaseller.models.responseModels.editProductResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class EditProductVariantDetails {

    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("variant_code")
    @Expose
    private Integer variantCode;
    @SerializedName("mrp")
    @Expose
    private Double mrp;
    @SerializedName("discount")
    @Expose
    private Double discount;
    @SerializedName("values")
    @Expose
    private LinkedList<OptionValues> listOfOptionValues;


    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVariantCode() {
        return variantCode;
    }

    public void setVariantCode(Integer variantCode) {
        this.variantCode = variantCode;
    }

    public Double getMrp() {
        return mrp;
    }

    public void setMrp(Double mrp) {
        this.mrp = mrp;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public LinkedList<OptionValues> getListOfOptionValues() {
        return listOfOptionValues;
    }

    public void setListOfOptionValues(LinkedList<OptionValues> listOfOptionValues) {
        this.listOfOptionValues = listOfOptionValues;
    }
}
