package com.tradebeaseller.models.requestModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddOptionDetails {

    @SerializedName("optionId")
    @Expose
    private String optionId = "";
    @SerializedName("optionName")
    @Expose
    private String optionName = "";
    @SerializedName("groupId")
    @Expose
    private String groupId = "";
    @SerializedName("groupName")
    @Expose
    private String groupName = "";
    @SerializedName("itemIds")
    @Expose
    private String itemIds = "";
    @SerializedName("itemNames")
    @Expose
    private String itemNames = "";


    public String getOptionId() {
        return optionId;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getItemIds() {
        return itemIds;
    }

    public void setItemIds(String itemIds) {
        this.itemIds = itemIds;
    }

    public String getItemNames() {
        return itemNames;
    }

    public void setItemNames(String itemNames) {
        this.itemNames = itemNames;
    }
}
