package com.tradebeaseller.interfaces;

public interface SelectedBrandDetailsCallBack {
    void selectedBrandDetails(String selectedBrandId,String selectedBrandName);
}
