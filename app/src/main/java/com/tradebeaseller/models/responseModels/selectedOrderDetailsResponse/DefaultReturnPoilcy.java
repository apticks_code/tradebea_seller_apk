package com.tradebeaseller.models.responseModels.selectedOrderDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class DefaultReturnPoilcy {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("valid_before_in_hrs")
    @Expose
    private Integer validBeforeInHrs;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("default_return_reasons")
    @Expose
    private LinkedList<DefaultReturnReason> listOfDefaultReturnReasons = null;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getValidBeforeInHrs() {
        return validBeforeInHrs;
    }

    public void setValidBeforeInHrs(Integer validBeforeInHrs) {
        this.validBeforeInHrs = validBeforeInHrs;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LinkedList<DefaultReturnReason> getListOfDefaultReturnReasons() {
        return listOfDefaultReturnReasons;
    }

    public void setListOfDefaultReturnReasons(LinkedList<DefaultReturnReason> listOfDefaultReturnReasons) {
        this.listOfDefaultReturnReasons = listOfDefaultReturnReasons;
    }
}
